(function($) {
	
	core.init();
	cookies.init();
	faqs.init();
	gallery.init();
	header.init();
	journal.init();
	map.init();
	notifications.init();
  recaptcha.init();
	related.init();
	team.init();
	twitter.init();
	
	console.log('Made by Boldr.');
	
}(jQuery));