var team = (function() {
  
  var opts = {
  	$trigger: $('.team-member-link'),
  	$close: $('.mfp-modal-button--close'),
  	active: 'team-member--active'
  };
  
  // Touch --
  
  var touch = function() {
  	
  	if (Modernizr.touchevents) {
  		
  		opts.$trigger.each(function(index, element) {
  			
  			var $this = $(this);
  			
  			$this.on({
  				'touchstart': function(event) {
  					$(this).parent().addClass(opts.active);
  				},
  				'touchend': function(event) {
  					$(this).parent().removeClass(opts.active);
  				}
  			});
  		});
  		
  	}
    
  };
  
  // Magnific popup --
  
  var mfp = function() {
    
    opts.$trigger.magnificPopup({
			type: 'inline',
  		mainClass: 'mfp-fade',
  		removalDelay: 300,
  		midClick: true,
			// autoFocusLast: true,
  	});
    
  };
  
  // Close --
  
  var close = function() {
  	
  	opts.$close.on('click', function(event) {
  	  $.magnificPopup.close();
  	  
  	  event.preventDefault();
    });
  	
  };
  
  // Init --
  
  var init = function() {
    touch();
    mfp();
    close();
  };

  return {
    init: init
  };
      
})();