var faqs = (function() {
  
  var opts = {
  	$trigger: $('.accordion-header'),
  	item: '.accordion-item',
  	active: 'accordion-item--active'
  };
  
  // Accordion --
  
  var accordion = function(event) {
  	
  	var $this = $(this);
  	
  	$this.attr('aria-expanded', function (index, attr) {
  	  return attr == 'true' ? 'false' : 'true';
  	}).closest(opts.item).toggleClass(opts.active);
  	
  	event.preventDefault();
  	
  };
  
  // Init --
  
  var init = function() {
  	opts.$trigger.on('click', accordion);
  };
  
  return {
  	init: init
  };
	
})();