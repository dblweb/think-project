var related = (function() {
  
  var opts = {
  	$slider: $('.related-slider')
  };
  
  // Slider --
  
  var slick = function() {
    
    opts.$slider.slick({
    	accessibility: false,
    	arrows: false,
    	dots: true,
    	dotsClass: 'slick-dots slick-dots--dark',
    	mobileFirst: true,
      swipeToSlide: true,
    	customPaging: function(slider, i) {
    		return $('<button type="button" class="slick-dot" />').text(i + 1);
    	},
    	responsive: [
    		{
    			breakpoint: 768,
    			settings: {
    				slidesToShow: 2
    			}
    		},
    		{
    			breakpoint: 1280,
    			settings: {
    				dots: false,
    				slidesToShow: 3
    			}
    		}
    	]
    });
    
  };
  
  // Init --
  
  var init = function() {
    
    if (opts.$slider.length) {
    	slick();
    }
    
  };

  return {
    init: init
  };
      
})();