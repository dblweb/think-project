var journal = (function() {
  
  var opts = {
  	container: '.journal',
  	$article: $('.article')
  };
  
  // Masonry --
  
  var macy = function() {
        
    var journal = Macy({
    	container: opts.container,
    	columns: 1,
    	// trueOrder: true,
    	mobileFirst: true,
    	breakAt: {
        1024: 3,
        768: 2
      }
    });
    
    load(journal);
    
  };
  
  // Load --
  
  var load = function(journal) {
  	    	
  	var $trigger = $('.load-more'),
  	    $journal = $trigger.parents('div').find(opts.container);
  	
    $trigger.on('click', function(event) {
    	
    	event.preventDefault();
    	
      $.ajax({
        dataType: 'html',
        url: $trigger.attr('href'),
        cache: true,
        beforeSend: function() {
        	$trigger.text('Loading');
        },
      }).done(function(data) {
      	
      	$journal.append($(data).find(opts.container).html());
      	
      	journal.runOnImageLoad(function () {
      	  journal.recalculate(true);
					// AOS.refreshHard();
      	}, true);
      	
      	if ($(data).find('.load-more').length) {
      		$trigger.text('Show more').attr('href', $(data).find('.load-more').attr('href'));
      	} else {
      		$trigger.parent().remove();
      	}
      	
      });
    });
    
  };
  
  // Time
  
  var time = function() {
  	
  	opts.$article.readingTime({
			lessThanAMinuteString: ' ',
      wordsPerMinute: 200,
      error: function(message) {
        opts.$article.find('.article-reading-time').remove();
      }
		});
  	
  };
  
  // Init --
  
  var init = function() {
    
    if ($(opts.container).length) {
    	macy();
    }
    
    if ($(opts.$article).length) {
      time();
    }
    
  };

  return {
    init: init
  };
      
})();