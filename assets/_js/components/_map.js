var map = (function() {
  
  var opts = {
  	$canvas: $('.map-canvas'),
  	api: 'AIzaSyCSZDTAimFZySybwFoa0IanVGZCWEPMnBY',
  	debug: false
  };
  
  
  // Build --
  
  var build = function() {
  	
  	opts.$canvas.each(function(index, element) {
  		
  		var $map = $(element),
  		    data = $map.data(),
  		    google = window.google,
  		    title = data.title,
  		    latlng = new google.maps.LatLng(data.lat, data.lng),
  		    zoom = data.zoom || 12,
  		    center = data.center || false,
  		    ui = data.ui || false,
  		    markersPath = data.markers || false,
  		    stylePath = data.style || false,
  		    markers = [],
  		    map, options, config, marker;
  		
  		options = {
  			backgroundColor: '#e0e7ec',
  			zoom: zoom,
  			minZoom: 10,
  			maxZoom: 16,
  			center: latlng,
  			mapTypeId: google.maps.MapTypeId.ROADMAP,
  			disableDefaultUI: ui,
  			// mapTypeControl: false,
  			// scaleControl: false,
  			// streetViewControl: false,
  			// rotateControl: false,
  			fullscreenControl: true,
  			zoomControl: true,
  			// disableDoubleClickZoom: true,
  			gestureHandling: 'cooperative',
  			// scrollwheel: false,
  			draggable: true
  		};
  		
  		
  		// Initialise map
  		// map = new google.maps.Map($map[index], mapOptions);
  		map = new google.maps.Map(element, options);
  		
  		
  		// Map style
  		if (stylePath) {
  			$.ajax({
  			  dataType: 'json',
  			  url: stylePath,
  			  cache: true,
  			}).done(function(data) {
  				
  				var mapType = new google.maps.StyledMapType(data);
  				
  				map.mapTypes.set('mapstyle', mapType);
  				map.setMapTypeId('mapstyle');
  				  				
  			}).fail(function() {
  				console.log('Map style error');
  			});
  		}
  		
  		
  		// Map markers
  		var icon = 'M12 2C8.1 2 5 5.1 5 9c0 5.2 7 13 7 13s7-7.8 7-13c0-3.9-3.1-7-7-7zm0 9.5c-1.4 0-2.5-1.1-2.5-2.5s1.1-2.5 2.5-2.5 2.5 1.1 2.5 2.5-1.1 2.5-2.5 2.5z',
  		    iconColor = '#536178';
  		
  		if (markersPath) {
  			
  			// Multiple markers
  			
  			$.ajax({
  			  dataType: 'json',
  			  url: markersPath,
  			  cache: true,
  			}).done(function(data) {
  				
  				// Construct bounds
  				bounds = new google.maps.LatLngBounds();
  				  				
  				// Multiple markers
  				$.each(data, function(index, value) {
  				  
  				  var location = new google.maps.LatLng(value.location.lat, value.location.lng);
  				  
  				  var config = {
  				    latlng: location,
  				    title: value.title,
  				    icon: icon,
  				    color: value.icon.color
  				  };
  				      
  				  // Create marker
  				  create(map, config);
  				  
  				  // Push marker into markers array
  				  markers.push(marker);
  				  
  				  // Extend bounds
  				  bounds.extend(location);
  				});
  				
  				// Fit bounds to map viewport
  				map.fitBounds(bounds);
  				
  				// Debug bounds
  				if (opts.debug) {
  					debug(map, bounds);
  				}
  				
  			}).fail(function() {
  				console.log('Map markers error');
  			});
  		} else {
  			
  			// Single marker
  			
  			var config = {
  			  latlng: latlng,
  			  title: title,
  			  icon: icon,
  			  color: iconColor
  			};
  			
  			// Create marker
  			create(map, config);
  		}
  		
  		
  		// Center map on resize
  		if (center) {

  			google.maps.event.addDomListener(window, 'resize', function() {
  			  
  			  if (markers.length > 1) {
  			  	// map.setCenter(bounds.getCenter());
  			  	map.fitBounds(bounds);
  			  } else {
  			  	 // map.setCenter(map.getCenter());
  			  	 map.setCenter(latlng);
  			  }
  			  
  			});
  		}
  	});
  	
  	// Return
  	return console.log('Map loaded');
  	
  };
  
  
  // Create --
  
  var create = function(map, config) {
  	
  	marker = new google.maps.Marker({
  	  // animation: google.maps.Animation.DROP,
  	  position: config.latlng,
  	  map: map,
  	  title: config.title,
  	  icon: {
  	  	path: config.icon,
  	  	fillColor: config.color,
  	  	fillOpacity: 1,
  	  	strokeWeight: 0,
  	  	scale: 2,
  	  	anchor: new google.maps.Point(12, 24)
  	  }
  	  
  	});
  	
  	return marker;
  	
  };
  
  
  // Debug --
    
  var debug = function(map, bounds) {
  	
  	var coordinates = [],
  	    ne = bounds.getNorthEast(),
  	    sw = bounds.getSouthWest(),
  	    nw = new google.maps.LatLng(ne.lat(), sw.lng()),
  	    se = new google.maps.LatLng(sw.lat(), ne.lng());
    
    coordinates.push(ne, se, sw, nw);
    
    var polygon = new google.maps.Polygon({
      paths: coordinates,
      strokeColor: '#e6525a',
      strokeOpacity: 1,
      strokeWeight: 3,
      fillColor: '#e6525a',
      fillOpacity: 0.3
    });
    
    polygon.setMap(map);
    
    console.log('Map debug enabled');
  		      
  };
  
  
  // Load --
  
  var load = function() {
    
    if (!window.google) {
    	$.ajax({
    	  dataType: 'script',
    	  url: 'https://maps.googleapis.com/maps/api/js?key=' + opts.api,
    	  cache: true
    	}).done(function() {
    		console.log('Map API loaded');
    		build();
    	}).fail(function() {
    		console.log('Map API error');
    	});
    } else {
    	build();
    }
    
  };
  
  
  // Init --
  
  var init = function() {
    
    if (opts.$canvas.length) {
    	load();
    }
  	
  };
  
  return {
  	init: init
  };
	
})();