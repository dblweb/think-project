var recaptcha = (function() {
	
	var opts = {
		$response: $('#g-recaptcha-response'),
    $form: $('.form').data('form'),
		api: '6LfRgpIUAAAAAGFH9nQgyrN16fUWtkuN4wEepg7s' // Production
    // api: '6LdAg5IUAAAAACbEifls8rOHvD8ZyUunoXqGigpN' // Development
	};
	
	// Recaptcha
	
	var recaptcha = function() {
    
    $action = opts.$form.charAt(0).toUpperCase() + opts.$form.slice(1);
    
    grecaptcha.ready(function () {
    	grecaptcha.execute(opts.api, { action: $action }).then(function (token) {
    		opts.$response.val('').val(token);
    	});
    });
	}
  
  // Load --
  
  var load = function() {
    
    $.ajax({
      dataType: 'script',
      url: 'https://www.google.com/recaptcha/api.js?render=' + opts.api,
      cache: true
    }).done(function() {
    	console.log('Recaptcha API loaded');
    	recaptcha();
    }).fail(function() {
    	console.log('Recaptcha API error');
    });
    
  };
	
	// Init --
	
	var init = function() {
    
		if (opts.$response.length) {
			load();
		}
    
	}
	
	return {
		init: init
	};
	
})();