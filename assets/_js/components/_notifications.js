var notifications = (function() {
	
	var opts = {
		modal: '#notification-modal',
		$button: $('.mfp-modal-button')
	};
	
	// Check cookie --
	
	var check = function() {
				
		if (Cookies.get('notification') == null || Cookies.get('notification') == 'active') {
			
			mfp();

			Cookies.set('notification', 'active', {
	      expires: 30,
				// secure: true
	    });
			
		}
		
	};
	
	// Set cookie --
	
	var set = function() {
				
		Cookies.set('notification', 'inactive', {
		  expires: 30,
			// secure: true
		});
		
		Cookies.set('notifications', 'disabled', {
		  expires: 30,
			// secure: true
		});
		
	};
	
	// Magnific popup --
	
	var mfp = function() {
		
		setTimeout(function(){
			$.magnificPopup.open({
			  type: 'inline',
			  mainClass: 'mfp-fade',
			  removalDelay: 300,
				// fixedContentPos: true,
				// fixedBgPos: true,
				// overflowY: true,
				// alignTop: true,
			  items: {
			  	src: opts.modal 
			  },
				callbacks: {
			    beforeClose: function() {
			      set();
			    }
				},
			});
		}, 3000);
	  
	};
	
	// Close --
	
	var close = function() {
		
		if (opts.$button.length) {
			opts.$button.on('click', function(event) {
				
				var $href = $(this).attr('href');
				
			  $.magnificPopup.close({
					callbacks: {
						beforeClose: function($href) {
							set();
							// window.location.href = $href;
							// window.location.replace($href);							
						},
					},
				});
				
				// event.preventDefault();
			});
		}
		
	};
	
  // Init --
  
  var init = function() {
		
		var $modal = $(opts.modal);
		
		if ($modal.length) {
			check();
			// close();
		}
  };

  return {
    init: init
  };
      
})();
