var gallery = (function() {
  
  var opts = {
  	container: '.gallery',
  	$trigger: $('.gallery-img-link'),
  	active: 'gallery-img--active',
  	width: Modernizr.mq('(max-width: 575px)')
  };
  
  // Touch --
  
  var touch = function() {
  	
  	if (Modernizr.touchevents && !opts.width) {
  		
  		opts.$trigger.each(function(index, element) {
  			
  			var $this = $(this);
  			
  			$this.on({
  				'touchstart': function(event) {
  					$(this).parent().addClass(opts.active);
  				},
  				'touchend': function(event) {
  					$(this).parent().removeClass(opts.active);
  				}
  			});
  		});
  		
  	}
    
  };
  
  // Masonry --
  
  var macy = function() {
  
    var gallery = Macy({
    	container: opts.container,
    	columns: 1,
    	mobileFirst: true,
    	breakAt: {
        1024: 4,
        768: 3,
        576: 2
      }
    });
    
    load(gallery);
    
  };
  
  // Load --
  
  var load = function(gallery) {
  	    	
  	var $trigger = $('.load-more'),
  	    $gallery = $trigger.parents('div').find(opts.container);
  	
    $trigger.on('click', function(event) {
    	
    	event.preventDefault();
    	
      $.ajax({
        dataType: 'html',
        url: $trigger.attr('href'),
        cache: true,
        beforeSend: function() {
        	$trigger.text('Loading');
        },
      }).done(function(data) {
      	
      	$gallery.append($(data).find(opts.container).html());
      	
      	gallery.runOnImageLoad(function () {
      	  gallery.recalculate(true);
					// AOS.refreshHard();
      	}, true);
      	
      	if ($(data).find('.load-more').length) {
      		$trigger.text('Show more').attr('href', $(data).find('.load-more').attr('href'));
      	} else {
      		$trigger.parent().remove();
      	}
      	
      });
    });
    
  };
  
  // Magnific popup --
  
  var mfp = function() {
  	  	
  	$(opts.container).magnificPopup({
  		type: 'image',
  		delegate: '.gallery-img-link',
  		mainClass: 'mfp-fade',
  		removalDelay: 300,
			midClick: true,
  		image: {
  			cursor: null
  		},
  		tLoading: '',
  	  disableOn: function() {
  	    if (opts.width) {
  	      return false;
  	    }
  	    return true;
  	  }
  	});
  	
  	if (opts.width) {
  	  opts.$trigger.on('click', function(event){
  	    event.preventDefault();
  	  });
  	}
  	
  };
  
  // Init --
  
  var init = function() {
    
    if ($(opts.container).length) {
    	touch();
    	macy();
    	mfp();
    }
    
  };

  return {
    init: init
  };
      
})();