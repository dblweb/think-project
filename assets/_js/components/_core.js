var core = (function() {
	
	// AOS
	
	var aos = function() {
		AOS.init({
			duration: 500,
			// disable: 'phone',
			disable: function() {
				return Modernizr.mq('(max-width: 575px)');
			},
			offset: 80,
			once: true
		});
	};
  
  // Dismiss
  
  var dismiss = function() {
  	
  	$('[data-dismiss]').on('click', function() {
      var $this = $(this),
          element = $this.data('dismiss');
      
      $this.parent().remove();
    });
  	
  };
  
  // Anti-spam email --
  
  var email = function() {
  
    $('.email').each(function() {
			
    	var $this = $(this);
    	    text = $this.text(),
					address = text.replace(' [at] ', '@');
    	    // address = text.replace('[at]', '@').replace(/\[dot\]/g, '.');
    	    
    	$this.attr('href', 'mailto:' + address).text(address);    	
    });
  
  };
	
	// External links --
	
	var links = function() {
    
    // External links
		
		$('a').filter('[href^="http"], [href^="//"]').not('[href*="' + window.location.host + '"]').attr({
			title: 'Opens in a new window',
			target: '_blank',
		  rel: 'noopener noreferrer'
		});
    
    // Download links
    
    // $('a').not('[href*="#"]').filter(function() {
    $('a').filter(function() {
      return $(this).attr('href').match(/\.(pdf|docx*|pptx*|xlsx*)$/i);
    }).attr({
    	title: 'Opens in a new window',
    	target: '_blank'
    });
		
	};
  
  var analytics = function() {
    
    // Check for Google Analytics
    
    // if (typeof(ga) !== 'undefined') {
    if (window.gtag) {
      
      console.log('Analytics loaded');
      
      // External links
      
      $('a').filter('[href^="http"], [href^="//"]').not('[href*="' + window.location.host + '"]').on('click', function(event) {
        
        var url = $(this).attr('href');
        
        // NOTE: Use older syntax (ga) for analytics.js
        /*
        ga('send', 'event', {
          eventCategory: 'Download',
          eventAction: 'click',
          eventLabel: url,
          transport: 'beacon',
          // nonInteraction: false,
          hitCallback: function() {
            console.log('Track: Event submitted');
          }
        });
        */
        gtag('event', 'click', {
          'event_category': 'External',
          'event_label': url,
          // 'non_interaction': true
        });
        
        // NOTE: Uncomment only for debugging (Marc)
        // event.preventDefault();
        // console.log('Track: External, click, ' + url);
      });
      
      
      // Email links
      
      $('a[href^="mailto:"]').on('click', function(event) {
        
        var url = $(this).attr('href');
        
        // NOTE: Use older syntax (ga) for analytics.js
        /*
        ga('send', 'event', {
          eventCategory: 'Email',
          eventAction: 'click',
          eventLabel: url,
          transport: 'beacon',
          // nonInteraction: false
        });
        */
        gtag('event', 'click', {
          'event_category': 'Email',
          'event_label': url,
          // 'non_interaction': true
        });
        
        // NOTE: Uncomment only for debugging (Marc)
        // event.preventDefault();
        // console.log('Track: Email, click, ' + url);
      });
      
      
      // Telephone links
      
      $('a[href^="tel:"]').on('click', function(event) {
        
        var url = $(this).attr('href');
        
        // NOTE: Use older syntax (ga) for analytics.js
        /*
        ga('event', 'event', {
          'event_category': 'Telephone',
          'event_label': url,
          // 'non_interaction': true,
          hitCallback: function() {
            console.log('Track: Event submitted');
            alert('Track: Event submitted');
          }
        });
        */
        gtag('event', 'click', {
          'event_category': 'Telephone',
          'event_label': url,
          // 'non_interaction': true
        });
        
        // NOTE: Uncomment only for debugging (Marc)
        // event.preventDefault();
        // console.log('Track: Telephone, click, ' + url);
      });
      
      
      // Download links
      
      // $('a').not('[href*="#"]').filter(function() {
      $('a').filter(function() {
        return $(this).attr('href').match(/\.(pdf|docx*|pptx*|xlsx*)$/i);
      }).on('click', function(event) {
        
        var url = $(this).attr('href');
        
        // NOTE: Use older syntax (ga) for analytics.js
        /*
        ga('send', 'event', {
          eventCategory: 'Download',
          eventAction: 'click',
          eventLabel: url,
          transport: 'beacon',
          // nonInteraction: false
        });
        */
        gtag('event', 'click', {
          'event_category': 'Download',
          'event_label': url,
          // 'non_interaction': true
        });
        
        // NOTE: Uncomment only for debugging (Marc)
        // event.preventDefault();
        // console.log('Track: Download, click, ' + url);
      });
      
      
      // Custom
      
      $('[data-track]').on('click', function(event) {
        var $this = $(this),
            element = $this.data('dismiss');
            
        var $this = $(this),
            url = ($this.attr('href') !== undefined) ? $this.attr('href') : '',
            category = ($this.data('track-category') !== undefined) ? $this.data('track-category') : 'Custom', // NOTE: Could also use typeof $this.data('category') !== 'undefined' or typeof $this.attr('data-category') !== 'undefined'
            action = ($this.data('track-action') !== undefined) ? $this.data('track-action') : 'Click',
            label = ($this.data('track-label') !== undefined) ? $this.data('track-label') : url;
        
        // NOTE: Use older syntax (ga) for analytics.js
        /*
        ga('send', 'event', {
          eventCategory: category,
          eventAction: action,
          eventLabel: label,
          transport: 'beacon',
          // nonInteraction: false
        });
        */
        gtag('event', action, {
          'event_category': category,
          'event_label': label,
          // 'non_interaction': true
        });
        
        // NOTE: Uncomment only for debugging (Marc)
        // event.preventDefault();
        // console.log('Track: ' + category + ', ' + action + ', ' + label);
        
      });
          
    } else {
      console.log('Google Analytics not loaded or version is out of date');
    }
    
  };
  
  // Init --
  
  var init = function() {
    svg4everybody();
		aos();
    dismiss();
    email();
		links();
    analytics();
  };

  return {
    init: init
  };
      
})();