var header = (function() {
	
	var opts = {
		$trigger: $('.nav-toggle, .overlay-close'),
		$overlay: $('.nav-overlay')
	};
  
  // Headroom --
  
  var headroom = function() {
  
    $('.header').headroom({
    	offset: 160,
      tolerance: {
        up: 24,
        down: 0
      }
    });
  
  };
	
	// Nav --
	
	var nav = function() {
		
		opts.$trigger.on('click', function(event) {
			opts.$overlay.toggleClass('overlay--active');
			$('html, body').toggleClass('overlay-active');
			
			event.preventDefault();
		});
			
	};
	
	var close = function() {
		
		opts.$overlay.removeClass('overlay--active');
		$('html, body').removeClass('overlay-active');
		
	};
  
  // Book a visit--
  
  var visit = function() {
    
    var $trigger = $('.book-a-visit'),
				section = '#book-a-visit',
				$form = $('#visit');
		
		if (!$(section).length) {
			$trigger.on('click', function(event) {
				
				close();
				
				window.location.href = '/' + section;
				
				event.preventDefault();
			});
		} else {
			$trigger.on('click', function(event) {
				
				close();
				
				window.location.href = section;
				
				$form.find(':input:visible:enabled:first').focus();
								
				event.preventDefault();
			});
		}
    
  };
  
  // Init --
  
  var init = function() {
    headroom();
		nav();
    visit();
  };

  return {
    init: init
  };
      
})();