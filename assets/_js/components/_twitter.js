var twitter = (function() {
  
  var opts = {
  	$slider: $('.twitter-slider')
  };
  
  // Slider --
  
  var slick = function() {
    
    opts.$slider.slick({
    	accessibility: false,
    	arrows: false,
    	dots: true,
    	mobileFirst: true,
      swipeToSlide: true,
    	customPaging: function(slider, i) {
    		return $('<button type="button" class="slick-dot" />').text(i + 1);
    	},
    	responsive: [
    		{
    			breakpoint: 768,
    			settings: {
    				slidesToShow: 2
    			}
    		},
    		{
    			breakpoint: 1280,
    			settings: {
    				slidesToShow: 3
    			}
    		},
    		{
    			breakpoint: 1440,
    			settings: {
    				dots: false,
    				slidesToShow: 4
    			}
    		}
    	]
    });
    
  };
  
  // Init --
  
  var init = function() {
    if (opts.$slider.length) {
    	slick();
    }
  };

  return {
    init: init
  };
      
})();