var cookies = (function() {
  
  var opts = {
  	$bar: $('.cookie-bar'),
  	$trigger: $('.cookie-bar-button'),
  	active: 'cookie-bar--active'
  };
  
  // Check cookie --
  
  var check = function() {
  	
  	if (Cookies.get('cookie-bar') == null || Cookies.get('cookie-bar') == 'active') {
      
			open();
			
      Cookies.set('cookie-bar', 'active', {
        expires: 365,
				// secure: true
      });
  	}
  	
  };
  
  // Set cookie --
  
  var set = function() {
  	
  	Cookies.set('cookie-bar', 'inactive', {
  	  expires: 365,
			// secure: true
  	});
  	
  	Cookies.set('cookies', 'enabled', {
  	  expires: 365,
			// secure: true
  	});
  	
  };
	
	// Open --
	
	var open = function() {
		
		// opts.bar.show();
		// opts.bar.delay(1000).slideDown();
		opts.$bar.addClass(opts.active);
		
	};
	
	// Close --
	
	var close = function() {
		
		// opts.$trigger.on("click", set);
		
		opts.$trigger.on('click', function(event) {
			
			// opts.$bar.hide();
			// opts.$bar.slideUp();
			opts.$bar.removeClass(opts.active);

			set();
			
			event.preventDefault();
		});
		
	};
  
  // Init --
  
  var init = function() {
  	check();
		close();
  };
  
  return {
  	init: init
  };
	
})();