/*!
# Chestnuts v1.1
# 2019-01-17
#
# Made by Boldr. <hello [at] weareboldr.co.uk>
*/

// @codekit-append quiet "plugins/aos.min.js";
// @codekit-append quiet "plugins/cookie.min.js";
// @codekit-append quiet "plugins/headroom.min.js";
// @codekit-append quiet "plugins/macy.min.js";
// @codekit-append quiet "plugins/magnific-popup.min.js";
// @codekit-append quiet "plugins/reading-time.min.js"
// @codekit-append quiet "plugins/slick.min.js";
// @codekit-append quiet "plugins/svg4everybody.min.js";

// @codekit-append "components/_core.js";
// @codekit-append "components/_cookies.js";
// @codekit-append "components/_faqs.js";
// @codekit-append "components/_gallery.js";
// @codekit-append "components/_header.js";
// @codekit-append "components/_journal.js";
// @codekit-append "components/_map.js";
// @codekit-append "components/_notifications.js";
// @codekit-append "components/_recaptcha.js";
// @codekit-append "components/_related.js";
// @codekit-append "components/_team.js";
// @codekit-append "components/_twitter.js";

// @codekit-append "_init.js";