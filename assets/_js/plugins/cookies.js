(function($) {
    $.fn.cookies = function(options) {
    
        var opts = $.extend({
            persistent: true,
            action: 'cookie-action', // class of link to accept cookies
            policy: 'cookie-policy', // class of link for cookie policy
            complete: null
        }, options);

        return this.each(function() {
            
            var $container = $(this),
                $action = $container.find('.'+ opts.action);
            
            if (opts.persistent) {
                if ($.cookie('cookies') == null || $.cookie('cookies') == 'show') {
                    $container.show();
                    $.cookie('cookies', 'show', {
                        expires: 365,
                        path: '/'
                    });
                    $action.click(hide);
                }
            } else {
                if ($.cookie('cookies') == null || $.cookie('cookies') == 'show') {
                    $container.show();
                    $.cookie('cookies', 'show', {
                        expires: 365,
                        path: '/'
                    });
                    $action.click(hide);
                    $('a').click(function() {
                        if (!$(this).hasClass(opts.policy)) $.cookie('cookies', 'hide', {
                            expires: 365,
                            path: '/'
                        });
                    });
                }
            }
            
            function hide() {
                $container.hide();
                $.cookie('cookies', 'hide', {
                    expires: 365,
                    path: '/'
                });
            }
            
            if ($.isFunction(opts.complete)) {
                opts.complete.call(this);
            }
            
        });

    }
}(jQuery));