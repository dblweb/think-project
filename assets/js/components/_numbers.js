// --------------------------------- //
// -- check if element is in view -- //
// --------------------------------- //

function isScrolledIntoView(ele) {
    var $ele = $(ele);
    var $window = $(window);

    var viewTop = $window.scrollTop();
    var viewBottom = viewTop + window.innerHeight;

    var eleTop = $ele.offset().top;

    var eleBottom = eleTop + $ele.height();

    // returns true or false
    return eleBottom <= viewBottom && eleTop >= viewTop;
}

    // count number
    $('.animateVal').each(function(i) {
        $(this).text('0');
    });
    $(window).on('load resize scroll', function(e) {
        $('.animateVal').each(function(i) {
            ele = $(this);
            val = $(this).attr('data-val');
            if (isScrolledIntoView(ele)) {
                animateVal(ele, val);
            }
        });
    });



