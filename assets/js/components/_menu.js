// menu - close other labels
$('button.menu-item').on('click', function(e) {
    e.preventDefault();
    var val = $(this).attr('data-menu');

    if (!$('#' + val).hasClass('menu--active')) {
        $('button.menu-item, .drop-down').removeClass('menu--active');
        $('#' + val + ', header, [data-menu="' + val + '"]').addClass('menu--active');
        $('body').addClass('menu--active');
    } else {
        $('#' + val + ', header, .drop-down, [data-menu="' + val + '"]').removeClass('menu--active');
        $('body').removeClass('menu--active');
    }
});

$('button.btn--sm-menu').on('click', function(e) {
    if ($(this).hasClass('menu--active')) {
        $(this).removeClass('menu--active');
    } else {
        $(this).addClass('menu--active');

    }
});
