// output all cookies //

//getCookies();

// ----------------------------- //
// open cookie consent popup box //
// ----------------------------- //
function cookiePopup() {
    $.magnificPopup.open({
        showCloseBtn: false,
        enableEscapeKey: false,
        closeOnBgClick: false,
        items: {
            src: '#cookieConsent',
            type: 'inline'
        }
    });
}
// ----------------------------- //
// open cookie consent popup box //
// ----------------------------- //




// ------------------------ //
// if cookie is not present //
// ------------------------ //
$(window).on('load', function() {
    if (typeof $.cookie('cookieConsent') == 'undefined') { 
        cookiePopup(); 
    }
});
// ------------------------ //
// if cookie is not present //
// ------------------------ //




// ------------------------- //
// clcik function for footer //
// ------------------------- //
$('.cookie-setting').on('click', function(e) {
    e.preventDefault();
    // take user to top of the page
    $(window).scrollTop(0);
    cookiePopup();
});
// ------------------------- //
// click function for footer //
// ------------------------- //



// ------------------------------- //
// click function buttons in popup //
// ------------------------------- //
$('#cookieConsent button').on('click', function(e) {
    e.preventDefault();
    var value = $(this).attr('data-cookie');
    
    if (value == 'declined') {
        // clear all cookies
        removeCookies(cookieRemove_Options);
    } 
    // set cookie
    $.cookie('cookieConsent', value, cookieSet_Options);
    // close popup
    $.magnificPopup.close();
    // reload page
    location.reload();
});
// ------------------------------- //
// click function buttons in popup //
// ------------------------------- //


