$('select.js--blog-filter').on('change', function(e) {
    // current url
    
    url = {}
    url.full = window.location.href;
    url.base = window.location.protocol + '//' + window.location.hostname + '/';
    // remove paging from url base
    url.path = window.location.pathname;
    url.pathArr = url.path.split('/').filter(x => x);

    url.pathArr.forEach(function(element) {
        if ($.isNumeric(element)) { url.pathArr.pop(); }
    });


    url.path = url.pathArr.join('/') + '/';

    

    


    console.log(url.base + url.path);
    if (typeof url.full.split('?')[1] !== 'undefined') {
        url.slug = url.full.split('?')[1];
        url.slugArr = url.slug.split('&').filter(x => x);
    } else {
        url.slug = '';
        url.slugArr = [];
    }
    
    // get path from select option
    catSlug = $(this).find('option:selected').attr('data-slug');
    catVal = this.value;
    if (catVal !== 'all') {
    newSlug = '?' + catSlug + '=' + catVal;
    } else {
        newSlug = '';
    }



    window.location.href = url.base + url.path + newSlug;     
});