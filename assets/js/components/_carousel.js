$(document).ready(function() {
    // sign-post carousel
    if ($(window).width() >= 1028) {
        var showCarouselItems = 4;
    } else if ($(window).width() < 1028 && $(window).width() > 767) {
        var showCarouselItems = 2;
    } else {
        var showCarouselItems = 1;
    }

    $('.carousel--wrapper').flickity({
        groupCells: showCarouselItems
    });
});
