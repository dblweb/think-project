$('select.js--collection-filter').on('change', function(e) {
    // get path from select option
    catPath = this.value;

    // selected val form option element
    filter = {};
    filter.path = catPath;
    filter.arr = filter.path.split('/').filter(Boolean);
    filter.key = filter.arr[0];
    filter.val = filter.arr[1];

    // current url
    url = {}
    url.full = $(location).attr('href');
    url.base = url.full.split('?')[0];
    if (typeof url.full.split('?')[1] !== 'undefined') {
        url.slug = url.full.split('?')[1];
        url.slugArr = url.slug.split('&').filter(x => x);
    } else {
        url.slug = '';
        url.slugArr = [];
    }
    
    // obj to create new slug
    slug = {};
    for (i = 0; i < url.slugArr.length; i++) {
        key = url.slugArr[i].split('=')[0];
        val = url.slugArr[i].split('=')[1];
        slug[key] = val; 
    }

    if (filter.path === 'all') {
        relCategory = $(this).attr('data-category');
        delete slug[relCategory];
    } else {
        slug[filter.key] = filter.val;
    }
    // deletes pagination from url
    delete slug['page'];

    slugKeys = Object.keys(slug);
    if (slugKeys.length === 0) {
        newSlug = '';
    } else {


        newSlug = [];
        for (i = 0; i < slugKeys.length; i++) {
            newSlug.push(slugKeys[i] + '=' + slug[slugKeys[i]]);
        }

        //delete page[relCategory];
        newSlug = '?' + newSlug.sort().join('&');

    }

    
    //console.log(newSlug);  
    window.location.href = url.base + newSlug;     
});