// ------------------------- //
// -- for animating stats -- //
// ------------------------- //

function animateVal(ele, val) {
    ele.each(function() {
        $(this)
            .prop('Counter', 0)
            .animate(
                {
                    Counter: val
                },
                {
                    duration: 3000,
                    easing: 'swing',
                    step: function(now) {
                        $(this).text(Math.ceil(now));
                    }
                }
            );
        $(this).removeClass('animateVal');
    });
}

// ----------------- //
// -- get cookies -- //
// ----------------- //

function getCookies() {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++){    
        arr = cookies[i].split("=");
        key = arr[0].trim();
        if (key !== 'cookieConsent') {
            console.log(key);
        }
    }
}

// -------------------- //
// -- remove cookies -- //
// -------------------- //

function removeCookies(options) {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++){    
        arr = cookies[i].split("=");
        key = arr[0].trim();
        if (key !== 'cookieConsent') {
            if (typeof options !== 'undefined') {
                $.removeCookie(key, options);
            } else {
                $.removeCookie(key, { path: '/' });
            }
        }
    }
}

// cookie switch different domains //
// -------------- //
// cookie options //
// -------------- //
switch(window.location.hostname) {
    case 'group.thinkproject.com':
        cookieSet_Options = {
            expires: 365,
            path: '/',
            domain: '.thinkproject.com',
        },
        cookieRemove_Options = {
            path: '/',
            domain: '.thinkproject.com',
        }
        break;
    
    case 'thinkproject.thirdfloordesign.net':
        cookieSet_Options = {
            expires: 365,
            path: '/',
            domain: '.thirdfloordesign.net',
        },
        cookieRemove_Options = {
            path: '/',
            domain: '.thirdfloordesign.net',
        }
        break;

    default:
        cookieSet_Options = {
            expires: 365,
            path: '/',
        },
        cookieRemove_Options = {
            path: '/',
        }
}

