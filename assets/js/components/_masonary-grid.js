    if ($('#news-events--container').length) {
        Macy.init({
            container: '#news-events--container',
            trueOrder: true,
            margin: {
                x: 30,
                y: 30
            },
            columns: 3,
            breakAt: {
                1299: 2,
                1027: 1
            }
        });
    }
