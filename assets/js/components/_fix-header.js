$(window).on('load scroll', function() {
    var header = $('header');
    var viewportOffset = window.pageYOffset;


    if( viewportOffset > 0 ) {
        header.addClass('header--fix');
    } else {
        header.removeClass('header--fix');
    }

});