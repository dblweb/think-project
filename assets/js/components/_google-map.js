if ( $( "#map" ).length ) {
    // zoom value
    zoomVal = 4.7;
    if ($("#map").attr('data-zoom')) {
        zoomVal = $( "#map" ).attr('data-zoom');
    }
    // center position
    mapPos = {lat: 47.000, lng: 7.500};
    if ($("#map").attr('data-lat') && $("#map").attr('data-lng')) {
       mapPos['lat'] = parseInt($("#map").attr('data-lat'));
       mapPos['lng'] = parseInt($("#map").attr('data-lng'));
    }

    console.log(mapPos);

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
        center: mapPos,
        zoom: parseInt(zoomVal),
        disableDefaultUI: true,
        styles: [
            // roads hidden
            {
                "featureType": "road",
                "stylers": [
                    {
                    "visibility": "off"
                    }
                ]
            },
            // points of interest
            {
                "featureType": "poi",
                "stylers": [
                    {
                    "visibility": "off"
                    }
                ]
            },
            // road labels
            {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [
                    {
                    "visibility": "off"
                    }
                ]
            },
            // land color 
            {
                "featureType": "landscape.natural",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                    "weight": "7.89"
                    },
                    {
                    "color": "#ffffff"
                    }
                ]
            },
            // country stroke divider
            {
                "featureType": "administrative.country",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                    "color": "#003B4D",
                    }
                ]
            },
            // water
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                    "color": "#8c9da5"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                    "color": "#444444"
                    }
                ]
            },
        ],
        });

        locations = [];
        // get locations longlat
        $('.location-grid li').each(function( index ) {
            if ( $(this).data().hasOwnProperty('lng') && $(this).data().hasOwnProperty('lat') ) {
                // get coordinates and force to numbers
                lat = $(this).attr('data-lat');
                lng = $(this).attr('data-lng');
                coordinate = {};
                coordinate['lat'] = Number(lat);
                coordinate['lng'] = Number(lng);
                // push to location array
                locations.push(coordinate);
            } 
        });

        for (i = 0; i < locations.length; i++) {
            var marker = new google.maps.Marker({
                position: locations[i],
                map: map,
                icon: {
                    url: window.location.origin + '/assets/img/thinkproject-map-pin.svg',
                    scaledSize: new google.maps.Size(40, 40),
                }
            });
        }
    }

}

