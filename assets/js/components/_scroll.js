// scroll to next section
$('.scroll--nxt').on('click', function(e) {
    e.preventDefault();

    var relSection = $(this).closest('section');
    var nextSection = relSection.next();
    var offset = nextSection.offset().top;

    //console.log(offset);

    $('html, body').animate({ scrollTop: offset }, 1000);
});
