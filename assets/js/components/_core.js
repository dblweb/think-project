// Anti-spam email --

$('.email').each(function() {
	
	var $this = $(this);
	    text = $this.text(),
			address = text.replace('[at]', '@');
	    // address = text.replace('[at]', '@').replace(/\[dot\]/g, '.');
	    
	$this.attr('href', 'mailto:' + address).text(address);    	
});

// External links

$('a[href]').filter('[href^="http"], [href^="//"]').not('[href*="' + window.location.host + '"]').attr({
	title: 'Opens in a new window',
	target: '_blank',
  rel: 'noopener noreferrer'
});

// Download links

// $('a').not('[href*="#"]').filter(function() {
$('a[href]').filter(function() {
  return $(this).attr('href').match(/\.(pdf|docx*|pptx*|xlsx*)$/i);
}).attr({
	title: 'Opens in a new window',
	target: '_blank'
});

// SVG sprite fallback
svg4everybody();