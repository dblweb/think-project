$('.modal--img').magnificPopup({
    type: 'image',
    // other options
    gallery: {
        enabled: true,
        //arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
        arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir% img-modal--arrow img-modal--arrow_%dir%"></button>',
       
    },
    image: {
        titleSrc: 'title',
    },
    closeMarkup: '<button title="%title%" type="button" class="mfp-close img-modal--close"><div></div><div></div></button>',
});