<?php 
    $page = [
        'title' => 'Project',
        'template' => 'project-reports-layout',
    ];
?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>



<section class="section-pad-sm">
    <div class="container paragraph-margin-lg">
        <div class="row">
            <div class="col-12">
                <ul class="breadcrumbs h3">
                    <li><a href="">Projects</a></li>
                    <li>Vitality World Thermal bath Laa</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="img-preview-grid v-padding--sm">
    <div class="container eq-margin--md">
        <div class="row">
            <div class="img-preview-grid--img-lg col-lg-6">
                <div class="img-preview-grid--item img-res">
                    <img src="https://via.placeholder.com/610x370.png/09f/fff">
                </div>
            </div>
            <div class="img-preview-grid--img-sm col-lg-6">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="img-preview-grid--item img-res">
                            <img src="https://via.placeholder.com/290x170.png/09f/fff">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="img-preview-grid--item img-res">
                            <img src="https://via.placeholder.com/290x170.png/09f/fff">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="img-preview-grid--item img-res">
                            <img src="https://via.placeholder.com/290x170.png/09f/fff">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
    </div>
</section>

<section class="v-padding--md">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 eq-margin--sm">
                <div>
                    <div class="h3">Laa an der thaya - Austria</div>
                    <div class="h2">Vitality World Thermal bath Laa</div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <hr>
                        <table class="before-heading--sm">
                            <tbody>
                                <tr>
                                    <td>Client:</td>
                                    <td>Vamed</td>
                                </tr>
                                <tr>
                                    <td>Industry:</td>
                                    <td>Hotel &amp; Tourism</td>
                                </tr>
                                <tr>
                                    <td>Project Type:</td>
                                    <td>Building Construction</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <h4 class="h2 h2--sm">Facts &amp; Figures</h4>
                        <ul class="list--sm ">
                            <li>Reconstruction and expansion</li>
                            <li>The core area of the spa is the unique Silent Spa</li>
                            <li>Approx. 3,600 m2 new usable area</li>
                        </ul>
                        </div>
                        <div class="col-lg-6">
                        <h4 class="h2 h2--sm">thinkproject supported:</h4>
                        <ul class="list--sm">
                            <li>Document storage and distribution</li>
                            <li>Drawing management</li>
                            <li>Claim management</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="cms-textarea--content">
                    <p>In Northern Weinviertel the latest extension of Thermal bath Laa, was opened. Starting December 2016, construction took 17 months including full maintenance operations.</p>
                    <p>The expansion includes the unique ‘Silent Spa’, containing a floatable tower, a cascade fountain with waterfall and three new exclusive relaxation oases. A lounge with bar, restaurant and terrace was also built, as well as several sauna facilities. Development also included adventure zones, plus a lounging area in the countryside with an outdoor pool. To ensure that the costs and schedule were not exceeded, VAMED worked with think project! for the duration.</p>
                    <p>The think project! Cross-Enterprise Collaboration Platform was used for document storage and distribution, as well as contract and drawing management throughout the project.</p>
                    <h3>Efficient Claim Management</h3>
                    <p>Structured project management is important to VAMED. Any claims by suppliers during the project had to be reported, checked and processed. For larger construction projects, such as the Thermal bath Laa, this task can become time-consuming and admin-heavy.</p>
                    <p>think project! was commissioned to create an efficient and process-driven claim management system.</p>
                    <p>The platform made it possible to record claims promptly and in a structured manner. Claims were automatically forwarded to the responsible parties with appropriate statuses provided to release them for order placement.</p>
                    <p>All processes were documented in a comprehensive manner, which lead to any deviations in the service descriptions being immediately visible. These could then be quickly checked, and unjustified claims were rejected with explanation.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="sign-post--img-col section-pad-md bg-white">
    <div class="container paragraph-margin-lg">
        <hr>
        <div class="row">
            <h3 class="h1 ta-c col-12">See other projects:</h3>
        </div>
        <div class="row">
            <a class="col-lg-4 paragraph-margin-sm">
                <div class="img-res">
                    <img src="https://via.placeholder.com/200x100.png/09f/fff" alt="">
                </div>
                <span href="#" class="link link--right">
                    <span>Bruckner Tower</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </span>
            </a>
            <a class="col-lg-4 paragraph-margin-sm">
                <div class="img-res">
                    <img src="https://via.placeholder.com/200x100.png/09f/fff" alt="">
                </div>
                <span href="#" class="link link--right">
                    <span>Bruckner Tower</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </span>
            </a>
            <a class="col-lg-4 paragraph-margin-sm">
                <div class="img-res">
                    <img src="https://via.placeholder.com/200x100.png/09f/fff" alt="">
                </div>
                <span href="#" class="link link--right">
                    <span>Bruckner Tower</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </span>
            </a>
        </div>
    </div>
</section>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/footer.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>