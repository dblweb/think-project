<!-- cookie consent -->    
<div id="cookieConsent" class="container bg-white section-pad-sm paragraph-margin-sm mfp-hide">
 <h4>Cookie Policy</h4>
  <hr />
  <p class="paragraph-lg">Our website uses cookies and other technologies to make your website experience better and to help us understand how you use our website. Please click “accept” to continue or “deny” to deactivate non-functional Cookies. The full website experience cannot be ensured, when Cookies are deactivated due to technical reasons. Learn more about Cookies in our data protection statement.</p>
  <div class="cookieConsent--cta ta-c">
   <button class="btn btn--skew" data-cookie="decline"><span>Decline</span></button>
   <button class="btn btn--skew btn--green" data-cookie="accept"><span>Accept</span></button>
  </div>
</div>
<!-- cookie consent -->
 
 <!-- start scripts -->
 <script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha384-nrOSfDHtoPMzJHjVTdCopGqIqeYETSXhZDFyniQ8ZHcVy08QesyHcnOUpMpqnmWq" crossorigin="anonymous"></script>
 <script>
  window.jQuery || document.write(
   '<script src="/assets/js/libs/jquery-3.1.0.min.js"><\/script>'
  );
 </script>
 <script src="/assets/build/combined.js"></script>
 <!-- end of scripts -->
</body>