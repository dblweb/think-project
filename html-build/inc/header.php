<body data-template="<?php echo $page[template]; ?>">
<header>
    <div class="container">
        <div class="row">

        <div class="col-4 logo--container">
            <a href="" class="logo">
                <svg class="logo" viewBox="0 0 180 40">
                    <use xlink:href="/assets/svg/svg-logo.svg#logo"></use>
                </svg>
            </a>
        </div>
        <div class="col-8  menu--container">
            <!--<input type="checkbox" id="checkbox--language" class="menu--checkbox hidden">
            <input type="checkbox" id="checkbox--search" class="menu--checkbox hidden">
            <input type="checkbox" id="checkbox--menu" class="menu--checkbox hidden">-->

            <button class="btn--language menu-item menu--label d-none d-lg-inline-block d-md-inline-block" data-menu="language">
                EN
                <svg class="icon--drop" viewBox="0 0 12 12">
                    <use xlink:href="/assets/svg/svg-defs.svg#icon--drop"></use>
                </svg>
            </button>
            
            <!--<label class="menu-item menu--label d-none d-lg-inline-block d-md-inline-block" for="checkbox--language">EN
                <svg class="icon--search" viewBox="0 0 12 12">
                    <use xlink:href="/assets/svg/svg-defs.svg#icon--drop"></use>
                </svg>
            </label>-->
            
            <a href="http://welcome.thinkproject.com/en/portal/" class="menu-item btn btn--skew"><span>Login</span></a>

            <button class="btn--search menu-item menu--label" data-menu="search">
                <svg class="icon--search" viewBox="0 0 24 24">
                    <use xlink:href="/assets/svg/svg-defs.svg#icon--search"></use>
                </svg>
            </button>

            <!--<label class="menu-item menu--label" for="checkbox--search">
                <svg class="icon--search" viewBox="0 0 24 24">
                    <use xlink:href="/assets/svg/svg-defs.svg#icon--search"></use>
                </svg>
            </label>-->

            <button class="btn--menu menu-item menu--label" data-menu="menu">
                <div class="menu--hamburger">
                    <span></span>
                    <span></span>
                </div>
            </button>

            <!--<label class="menu-item menu--label" for="checkbox--menu">
                <div class="menu--hamburger">
                    <span></span>
                    <span></span>
                </div>
            </label>-->

            <!-- drop down menus -->
            <div class="drop-down bg-white" id="language">
                <div class="container">
                    <hr>
                    langauge content
                </div>
            </div>
            <div class="drop-down bg-white" id="search">
                <div class="container ta-c">
                    <hr>
                    <div class="form-element--wrapper form-element--icon">
                        <input class="form-element" type="text" placeholder="Search" />
                        <svg class="icon--search" viewBox="0 0 24 24">
                            <use
                                xlink:href="/assets/svg/svg-defs.svg#icon--search"
                            ></use>
                        </svg>
                    </div> 
                </div>
            </div>
            <div class="drop-down bg-white" id="menu">
                <div class="container">
                    <hr>
                    <div class="row ta-l">

                        <div class="menu-col col-lg-3 col-md-4 paragraph-margin-md">
                            <ul class="menu-list">
                                <li>
                                    <a href="#">Solutions</a>
                                    <button class="btn--sm-menu d-md-none d-lg-none">
                                        <svg class="icon--drop" viewBox="0 0 12 12">
                                            <use xlink:href="/assets/svg/svg-defs.svg#icon--drop"></use>
                                        </svg>
                                    </button>
                                    <ul>
                                       <li>
                                          <a href="#">Project Collaboration</a>
                                          <ul>
                                             <li>
                                                <a href="#">Document
                                                Management</a>
                                             </li>
                                             <li>
                                                <a href="#">Quality
                                                Management</a>
                                             </li>
                                             <li>
                                                <a href="#">Task Management</a>
                                             </li>
                                             <li>
                                                <a href="#">Tender
                                                Management</a>
                                             </li>
                                          </ul>
                                        </li>
                                        <li><a href="#">BIM</a></li>
                                        <li><a href="#">Contracts</a></li>
                                        <li><a href="#">Services</a></li>
                                        <li><a href="#">Insights &amp; Integration</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        <div class="menu-col col-lg-3 col-md-4 paragraph-margin-md">
                            <ul class="menu-list">
                                <li><a href="#">About</a></li>
                            </ul>
                            <ul class="menu-list">
                                <li><a href="#">Sucess Stories</a></li>
                            </ul>
                            <ul class="menu-list">
                                <li><a href="#">News and Events</a></li>
                            </ul>
                            <ul class="menu-list">
                                <li><a href="#">Downloads</a></li>
                            </ul>
                            <ul class="menu-list">
                                <li><a href="#">Data Protection</a></li>
                            </ul>
                        </div>

                        <div class="menu-col col-lg-3 col-md-4 paragraph-margin-md">
                            <ul class="menu-list">
                                <li><a href="#">Contact</a></li>
                            </ul>
                            <ul class="menu-list">
                                <li><a href="#">Locations</a></li>
                            </ul>
                            <ul class="menu-list">
                                <li><a href="#">Careers</a></li>
                            </ul>
                            <ul class="menu-list">
                                <li>
                                    <a href="#">Customer Service</a>
                                    <button class="btn--sm-menu d-md-none d-lg-none">
                                        <svg class="icon--drop" viewBox="0 0 12 12">
                                            <use xlink:href="/assets/svg/svg-defs.svg#icon--drop"></use>
                                        </svg>
                                    </button>
                                    <ul>
                                        <li><a href="http://welcome.thinkproject.com/en/portal/">Login</a></li>
                                        <li><a href="#">Support</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        <div class="menu-col col-lg-3 paragraph-margin-sm d-none d-lg-block">
                            <div class="menu-address">
                                <p>
                                Think Project - Munich Office,<br class="hidden-sm">
                                Zamdorfer Strasse 100, <br class="hidden-sm">
                                81677 Munich, Germany
                                </p>
                            </div>
                            <div class="menu-contact">
                                <p>
                                Phone <a href="#">+49 89 930839 300</a><br>
                                <a href="#">info@thinkproject.com</a><br>
                                <a href="/">www.thinkproject.com</a>
                                </p>
                            </div>
                            <ul class="social-list text-color--pink">
                                <li>
                                    <a href="#">
                                    <svg class="icon--twitter" viewBox="0 0 30 30">
                                        <use xlink:href="/assets/svg/svg-defs.svg#icon--twitter"></use>
                                    </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                    <svg class="icon--linkedin" viewBox="0 0 30 30">
                                        <use xlink:href="/assets/svg/svg-defs.svg#icon--linkedin"></use>
                                    </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="menu--lang-sm d-md-none d-lg-none container">
                            <div class="d-md-none d-lg-none row">
                                <a href="#" class="col-2 ta-c">EN</a>
                                <a href="#" class="col-2 ta-c">DE</a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

        </div>
        </div>
    </div>
</header>