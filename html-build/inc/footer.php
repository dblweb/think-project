<section class="bg-grey section-pad-md">
  <div class="container">
    <div class="row paragraph-margin-sm">
      <div class="col-12 ta-c paragraph-margin-sm">
        <h2 class="h1">Contact us / Sign up:</h2>
        <p>Stay up-to-date with our news and events and receive insights into industry trends by filling in the form below to subscribe to our newsletter. Once completed, you will receive a confirmation email to activate your newsletter subscription. You can unsubscribe at any time.</p>
      </div>
      <form class="col-12">
        

          <div class="row">
            <div class="col-lg-4">
              <input class="form-element" type="text" placeholder="First name">
            </div>
            <div class="col-lg-4">
              <input class="form-element" type="text" placeholder="Last name">
            </div>
            <div class="col-lg-4">
              <input class="form-element" type="text" placeholder="Country">
            </div>
          </div>

          <div class="row">
            <div class="col-lg-4">
              <input class="form-element" type="text" placeholder="Email address">
            </div>
            <div class="col-lg-4">
              <input class="form-element" type="text" placeholder="Phone Number">
            </div>
            <div class="col-lg-4">
              <input class="form-element" type="text" placeholder="Company">
            </div>
          </div>

          <div class="row">
            <div class="col-12">
              <textarea class="form-element" rows="1" placeholder="Message"></textarea>
            </div>
          </div>

          <div class="row align-items-center">
            <div class="col-lg-5">   
              <div class="form--checkbox">
                <input type="checkbox" id="checkbox1" class="hidden">
                <label for="checkbox1">
                <span></span>
                  I have read the data protection statement and accept it.
                </label>
              </div>
            </div>
            <div class="col-lg-5">
              <div class="form--checkbox">
                <input type="checkbox" id="checkbox2" class="hidden">
                <label for="checkbox2">
                <span></span>
                I would like to receive more information about think project!
                </label>
              </div>  
            </div>
            <div class="col-lg-2 ta-r"><button class="menu-item btn btn--skew btn--pink"><span>Submit</span></button></div>
          </div>        
      </form>
  </div> 
</div>
  </div>
</section>

<footer class="bg-dgrey section-pad-sm">
  <div class="container paragraph-margin-md">
    <div class="row">

      <div class="footer--menu-col col-xl-2 col-md-3 d-none d-lg-block paragraph-margin-sm">
        <ul class="menu-list">
          <li>
            <a href="#">Solutions</a>
            <ul>
              <li>
                <a href="#">Project Collaboration</a>
                <ul>
                  <li><a href="#">Document Management</a></li>
                  <li><a href="#">Quality Management</a></li>
                  <li><a href="#">Task Management</a></li>
                  <li><a href="#">Tender Management</a></li>
                </ul>
              </li>
              <li><a href="#">BIM</a></li>
              <li><a href="#">Contracts</a></li>
              <li><a href="#">Services</a></li>
              <li><a href="#">Insights &amp; Intergration</a></li>
            </ul>
          </li>
        </ul>
      </div>
      
      <div class="footer--menu-col col-xl-2 col-md-3 d-none d-lg-block paragraph-margin-sm">
        <ul class="menu-list">
          <li><a href="#">About</a></li>
        </ul> 
        <ul class="menu-list">
          <li><a href="#">Success Stories</a></li>
        </ul>
        <ul class="menu-list">
          <li><a href="#">News &amp; Events</a></li>
        </ul>
        <ul class="menu-list">
          <li><a href="#">Downloads</a></li>
        </ul>
        <ul class="menu-list">
          <li><a href="#">Data Protection</a></li>
        </ul>
      </div>
      
      <div class="footer--menu-col col-xl-2 col-md-3 d-none d-lg-block paragraph-margin-sm">
        <ul class="menu-list">
          <li><a href="#">Contact</a></li>
        </ul>
        <ul class="menu-list">
          <li><a href="#">Locations</a></li>
        </ul>
        <ul class="menu-list">
          <li><a href="#">Products</a></li>
        </ul>
        <ul class="menu-list">
          <li><a href="#">Careers</a></li>
        </ul>
        
      </div>

      <div class="footer--menu-col col-xl-2 col-md-3 d-none d-lg-block paragraph-margin-sm">
        <ul class="menu-list">
          <li>
            <a href="#">Customer Service</a>
            <ul>
              <li><a href="http://welcome.thinkproject.com/en/portal/">Login</a></li>
              <li><a href="#">Support</a></li>
            </ul>
          </li>
        </ul>
      </div>

      <div class="footer--contact-col col-xl-3 col-md-12 d-none d-lg-block paragraph-margin-sm">
        <div class="menu-address paragraph-margin-sm">
            <p>
            Think Project - Munich Office, <br class="d-lg-block d-md-none d-sm-none">
            Zamdorfer Strasse 100, <br class="d-lg-block d-md-none d-sm-none">
            81677 Munich, <br class="d-lg-block d-md-none d-sm-none">
            Germany
            </p>                  
        </div>
        <div class="menu-contact paragraph-margin-sm">
          <p>
            Phone <a href="#">+49 89 930839 300</a><br>
            <a href="#">info@thinkproject.com</a><br>
            <a href="/">www.thinkproject.com</a>
          </p>
        </div>
      </div>
  
      <div class="footer--social-col col-6 col-xl-1 col-md-4 paragraph-margin-sm">
        <ul class="social-list text-color--pink">
          <li>
            <a href="#">
              <svg class="icon--twitter" viewBox="0 0 30 30">
                <use xlink:href="/assets/svg/svg-defs.svg#icon--twitter"></use>
              </svg>
            </a>
          </li>
          <li>
            <a href="#">
              <svg class="icon--linkedin" viewBox="0 0 30 30">
                <use xlink:href="/assets/svg/svg-defs.svg#icon--linkedin"></use>
              </svg>
            </a>
          </li>
        </ul>
      </div>

      <div class="footer--links-col col-6 col-lg-12">
        <ul class="list--inline">
          <li><a href="#">Cookie Settings</a></li>
          <li><a href="#">Cookie Policy</a></li>
          <li><a href="#">Data Protection Statement</a></li>
          <li><a href="#">Imprint</a></li>
        </ul>
      </div>
      
      <div class="footer-copyright-col col-12">
        <hr>
        <strong class="h4">Copyright <?php echo date('Y'); ?> / Think Project</strong>
      </div>
   </div>
 </div>
</footer>


