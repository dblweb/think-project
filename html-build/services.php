<?php 
    $page = [
        'title' => 'Services',
        'template' => 'services-layout',
    ];
?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>

<section class="hero hero--img-text section-pad-md bg-pink">
    <div class="container">
        <div class="row align-items-top">
            <div class="hero--img-text__col col-lg-6 paragraph-margin-md">
                <h1 class="h3">Services</h1>
                <h2 class="h1">Services and support, tailored to the way you work</h2>
                <p>Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, et at consectetuer adipiscing elit, dolor sit rem ipsum dolor sit amet Lorem ipsum dolor sit amet, Lorem ipsum dolor, consectet.</p>
                <a href="#" class="link link--down scroll--nxt">
                    <span>Find out more</span>
                    <svg class="icon--arrow-d icon--svg" viewBox="0 0 20 86">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-d"></use>
                    </svg>
                </a>
            </div>
            <div class="hero--img-text__col col-lg-5 offset-lg-1">
                <div class="img-res">
                    <img src="https://via.placeholder.com/800x600.png/09f/fff">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="hero-secondary hero-secondary--col  section-pad-md bg-pink">
    <div class="container">
        <div class="row ta-c">
            <a href="" class="hero-secondary--col__item col-lg-6 paragraph-margin-sm">
                <img src="https://via.placeholder.com/180x180.png/09f/fff">
                <h4 class="h1">Consultancy</h4>
                <p>think project! has gained experience from thousands of construction projects and can therefore offer qualified consulting and services including identification of requirements, roll-out planning, software implementation, on-demand and ongoing support, maintenance and user training.</p>
                <span href="#" class="link link--right">
                    <span>Find out more</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </span>
            </a>
            <a href="" class="hero-secondary--col__item col-lg-6 paragraph-margin-sm">
                <img src="https://via.placeholder.com/180x180.png/09f/fff">
                <h4 class="h1">Project Service</h4>
                <p>Collaboration and information management involves a range of organizational tasks that can be outsourced in case dedicated staff are not available for the project. think project! – in cooperation with the associated company planConnect – offers highly efficient project services on demand.</p>
                <span href="#" class="link link--right">
                    <span>Find out more</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </span>
            </a>
        </div>
    </div>
</section>

<section class="block--img-text__invert section-pad-md bg-white">
    <div class="container">
        <div class="row align-items-center">
            <div class="block--img-text__text  col-lg-6 paragraph-margin-md">
                <h2 class="h1">Looking for individual solutions?</h2>
                <p>Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, et at consectetuer adipiscing elit, dolor sit rem ipsum dolor sit amet Lorem ipsum dolor sit amet, Lorem ipsum dolor, consectet.</p>
                <a href="#" class="link link--right">
                    <span>Products</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </a>
            </div>
            <div class="block--img-text__img  col-lg-6 paragraph-margin-md ">
                <div class="img-res">
                    <img src="https://via.placeholder.com/600x400.png/09f/fff">
                </div>
            </div>
        </div>
    </div>
</section>


<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/footer.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>