<?php 
    $page = [
        'title' => 'About',
        'template' => 'about-layout',
    ];
?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>

<section class="hero hero--img-text section-pad-md bg-blue">
    <div class="container">
        <div class="row align-items-top">
            <div class="hero--img-text__col col-lg-6 paragraph-margin-md">
                <h1 class="h3">About</h1>
                <h2 class="h1">Supporting the industry and the people in it</h2>
                <p>Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, et at consectetuer adipiscing elit, dolor sit rem ipsum dolor sit amet Lorem ipsum dolor sit amet, Lorem ipsum dolor, consectet.</p>
                <a href="#" class="link link--down scroll--nxt">
                    <span>Find out more</span>
                    <svg class="icon--arrow-d icon--svg" viewBox="0 0 20 86">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-d"></use>
                    </svg>
                </a>
            </div>
            <div class="hero--img-text__col col-lg-5 offset-lg-1">
                <div class="img-res">
                    <img src="https://via.placeholder.com/800x600.png/09f/fff">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="block--text-col section-pad-md bg-grey">
    <div class="container">
        <div class="row align-items-top">
            <div class="block--text-col__title  col-lg-4 paragraph-margin-md">
                <h2 class="h1">Enabling better industry results</h2>
            </div>
            <div class="block--text-col__text col-lg-8 paragraph-margin-md newspaper-col-2">
                <p>For nearly 20 years we have delivered tailored, cloud-based software solutions to facilitate collaboration and information management in construction and engineering projects. We confront today’s digitalisation challenges in the industry by providing state-of-the-art systems, as well as expert knowledge and consulting services to free up time for project professionals.</p>
            </div>
        </div>
    </div>
</section>

<section class="block--img-text__invert section-pad-md">
    <div class="container">
        <div class="row align-items-center">
            <div class="block--img-text__text col-lg-6 paragraph-margin-md">
                <h2 class="h1">Developing the digital landscape</h2>
                <p>All projects need a single source of truth. Construction projects are complex with masses of data which needs to be collected, managed and disseminated to all parties involved. To solve this, our secure, ISO 27001:2013-certified solutions connect teams, processes, information and systems across company boundaries and support the entire project lifecycle with an open, collaborative approach.</p>
                <p>Founded in 2000, we've since grown from our humble beginnings in Munich into a market leader in construction intelligence.</p>
            </div>
            <div class="block--img-text__img col-lg-6 paragraph-margin-md">
                <div class="img-res">
                    <img src="https://via.placeholder.com/600x400.png/09f/fff">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="block--marquee section-pad-sm paragraph-margin-lg">
    <div class="container ta-c">
        <h2 class="h1">Our customers include:</h2>
    </div>
   
    <div class="block--marquee__wrapper container-fluid">
        <div class="block--marquee__inner">
            <div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
            </div>
            <div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
            </div>
        </div>
        <div class="block--marquee__inner" data-direction="reverse">
            <div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
            </div>
            <div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="block--team-grid section-pad-md bg-white">
    <div class="container paragraph-margin-lg">
        <h2 class="h1">Our Team</h3>
        <div class="row paragraph-margin-sm">
            <div class="col-12 paragraph-margin-sm">
                <h3 class="h2">Group Leadership Team</h3>
            </div>
            <a href="#" class="block--team-grid--item col-lg-4 col-md-6 paragraph-margin-xs">
                <div class="block--team-grid--img img-res">
                    <img src="https://via.placeholder.com/600x400.png/09f/fff">
                </div>
                <div class="paragraph-margin-none">
                    <div class="h4 text-color--pink">Gareth Burton</div>
                    <div class="h3">Chief Executive Officer</div>
                </div>
                <span href="#" class="link link--right">
                    <span>Biography</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </span>
            </a>
            <a href="#" class="block--team-grid--item col-lg-4 col-md-6 paragraph-margin-xs">
                <div class="block--team-grid--img img-res">
                    <img src="https://via.placeholder.com/600x400.png/09f/fff">
                </div>
                <div class="paragraph-margin-none">
                    <div class="h4 text-color--pink">Ralf Grüßhaber</div>
                    <div class="h3">Chief Financial Officer</div>
                </div>
                <span href="#" class="link link--right">
                    <span>Biography</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </span>
            </a>
            <a href="#" class="block--team-grid--item col-lg-4 col-md-6 paragraph-margin-xs">
                <div class="block--team-grid--img img-res">
                    <img src="https://via.placeholder.com/600x400.png/09f/fff">
                </div>
                <div class="paragraph-margin-none">
                    <div class="h4 text-color--pink">Anton Hofmeier</div>
                    <div class="h3">Chief Sales Officer</div>
                </div>
                <span href="#" class="link link--right">
                    <span>Biography</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </span>
            </a>
            <a href="#" class="block--team-grid--item col-lg-4 col-md-6 paragraph-margin-xs">
                <div class="block--team-grid--img img-res">
                    <img src="https://via.placeholder.com/600x400.png/09f/fff">
                </div>
                <div class="paragraph-margin-none">
                    <div class="h4 text-color--pink">Dirk Lutzebäck</div>
                    <div class="h3">Chief Technology Officer</div>
                </div>
                <span href="#" class="link link--right">
                    <span>Biography</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </span>
            </a>
            <a href="#" class="block--team-grid--item col-lg-4 col-md-6 paragraph-margin-xs">
                <div class="block--team-grid--img img-res">
                    <img src="https://via.placeholder.com/600x400.png/09f/fff">
                </div>
                <div class="paragraph-margin-none">
                    <div class="h4 text-color--pink">Alexander Eder</div>
                    <div class="h3">Chief Operating Officer</div>
                </div>
                <span href="#" class="link link--right">
                    <span>Biography</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </span>
            </a>
            <a href="#" class="block--team-grid--item col-lg-4 col-md-6 paragraph-margin-xs">
                <div class="block--team-grid--img img-res">
                    <img src="https://via.placeholder.com/600x400.png/09f/fff">
                </div>
                <div class="paragraph-margin-none">
                    <div class="h4 text-color--pink">Frank Felten</div>
                    <div class="h3">Chief Product Officer</div>
                </div>
                <span href="#" class="link link--right">
                    <span>Biography</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </span>
            </a>
        </div>

        <div class="row paragraph-margin-sm">

            <div class="col-12 paragraph-margin-sm">
                <h3 class="h2">Non-Executive Board Members</h3>
            </div>  
            <a href="#" class="block--team-grid--item col-lg-4 col-md-6 paragraph-margin-xs">
                <div class="block--team-grid--img img-res">
                    <img src="https://via.placeholder.com/600x400.png/09f/fff">
                </div>
                <div class="paragraph-margin-none">
                    <div class="h4 text-color--pink">Tom Bachmaier</div>
                    <div class="h3">Non-Executive Board Member</div>
                </div>
                <span href="#" class="link link--right">
                    <span>Biography</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </span>
            </a>
            <a href="#" class="block--team-grid--item col-lg-4 col-md-6 paragraph-margin-xs">
                <div class="block--team-grid--img img-res">
                    <img src="https://via.placeholder.com/600x400.png/09f/fff">
                </div>
                <div class="paragraph-margin-none">
                    <div class="h4 text-color--pink">Peter Prestele</div>
                    <div class="h3">Non-Executive Board Member</div>
                </div>
                <span href="#" class="link link--right">
                    <span>Biography</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </span>
            </a>
            <a href="#" class="block--team-grid--item col-lg-4 col-md-6 paragraph-margin-xs">
                <div class="block--team-grid--img img-res">
                    <img src="https://via.placeholder.com/600x400.png/09f/fff">
                </div>
                <div class="paragraph-margin-none">
                    <div class="h4 text-color--pink">Ryan MacNamee</div>
                    <div class="h3">Non-Executive Board Member</div>
                </div>
                <span href="#" class="link link--right">
                    <span>Biography</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </span>
            </a>
            
        </div>
    </div>
</section>

<section class="block--stats section-pad-sm bg-pink">
    <div class="container paragraph-margin-md ta-c">
        <h3 class="h1">We’re proud to support:</h3>
        <div class="row align-items-center">
            <div class="block--stats--container col-lg-4">
                <div class="block--stats--number"><span data-val="150" class="animateVal">150</span>k</div>
                <div class="block--stats--text">users</div>
            </div>
            <div class="block--stats--container col-lg-4">
                <div class="block--stats--number"><span data-val="10" class="animateVal">10</span>k</div>
                <div class="block--stats--text">projects</div>
            </div>
            <div class="block--stats--container col-lg-4">
                <div class="block--stats--number"><span data-val="50" class="animateVal">50</span></div>
                <div class="block--stats--text">countries</div>
           </div>
        </div>
    </div>
</section>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/footer.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>