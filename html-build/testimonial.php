<?php 
    $page = [
        'title' => 'Team Member',
        'template' => 'team-member-layout',
    ];
?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>

<section class="v-padding--lg bg-blue">
</section>

<section class="block--testimonial--v1 v-padding--md bg-lgrey">
    <div class="container">
        <div class="row eq-margin ta-c">
            <div class="col-12">
                <blockquote>
                "The decision for consistent BIM implementation was above all characterised by the expectation of decisive progress towards the implementation of construction standards, the improvement of planning quality, public participation and the transfer of digital data over to operations. In addition to the advantages of the construction component library and the visualisation functionality of BIM, it also fosters improved collaboration between project participants through the usage of mutual project communication platform.
                </blockquote>
            </div>
            <div class="col-12 ">
            <strong>Head of Principles/Construction Technology</strong> - DB Station&Service AG
            </div>
        </div>
    </div>
</section>

<section class="v-padding--lg bg-white">
</section>

<section class="block--testimonial--v2 v-padding--md bg-lgrey">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7  offset-lg-1">
                <blockquote>
                "The decision for consistent BIM implementation was above all characterised by the expectation of decisive progress towards the implementation of construction standards, the improvement of planning quality, public participation and the transfer of digital data over to operations. In addition to the advantages of the construction component library and the visualisation functionality of BIM, it also fosters improved collaboration between project participants through the usage of mutual project communication platform."
                </blockquote>
            </div>
            <div class="col-lg-3 offset-lg-1">
            <strong>Head of Principles/Construction Technology</strong>, DB Station&Service AG
            </div>
        </div>
    </div>
</section>

<section class="v-padding--lg bg-white">
</section>

<section class="block--testimonial--v3 v-padding--md bg-lgrey">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-3 offset-lg-1">
            <strong>Head of Principles/Construction Technology</strong>, DB Station&Service AG
            </div>
            <div class="col-lg-7 offset-lg-1">
                <blockquote>
                "The decision for consistent BIM implementation was above all characterised by the expectation of decisive progress towards the implementation of construction standards, the improvement of planning quality, public participation and the transfer of digital data over to operations. In addition to the advantages of the construction component library and the visualisation functionality of BIM, it also fosters improved collaboration between project participants through the usage of mutual project communication platform."
                </blockquote>
            </div>
        </div>
    </div>
</section>

<section class="v-padding--lg bg-white">
</section>

<section class="block--testimonial--v4 v-padding--md bg-lgrey">
    <div class="container">
        <div class="row eq-margin">
            <div class="col-12">
                <h4 class="h2 h2--sm margin--none">Testimonial</h4>
            </div>
            <div class="col-12">
                <blockquote>
                "The decision for consistent BIM implementation was above all characterised by the expectation of decisive progress towards the implementation of construction standards, the improvement of planning quality, public participation and the transfer of digital data over to operations. In addition to the advantages of the construction component library and the visualisation functionality of BIM, it also fosters improved collaboration between project participants through the usage of mutual project communication platform."
                </blockquote>
            </div>
            <div class="col-12">
                Head of Principles/Construction Technology, <strong>DB Station&Service AG</strong>
            </div>
        </div>
    </div>
</section>

<section class="v-padding--lg bg-white">
</section>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/footer.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>