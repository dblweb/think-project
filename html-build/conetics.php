<?php 
    $page = [
        'title' => 'Conetics',
        'template' => 'needs name',
    ];
?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>

<section class="hero--border-col bg-green section-pad-lg">
    <div class="container paragraph-margin-md">
        <div class="row">
         <div class="col-12">
                <h1 class="h3">Conetics</h1>
            </div>
        </div>
        <div class="hero--border-col--2col-wrapper row align-items-top">
            <div class="error-page--code col-xl-5 col-lg-6 paragraph-margin-md">
                <h1 class="h1">Stronger together!</h1>
                <p>Conetics joined forces with think project!, the leading provider of cross-enterprise collaboration and information management in Germany and Europe.</p>
                <p>Together we are more than ever the strong partner for digitalisation in construction and engineering.</p>
            </div>
            <div class="error-page--border"></div>
                <div class="error-page--description col-xl-5 offset-xl-2 col-lg-6 paragraph-margin-md">
                    <div>
                        <img src="https://via.placeholder.com/250x100.png/09f/fff">
                        <div class="h2">Enter the Conetics portal:</div>
                    </div>
                    
                    <div><a href="#" class="btn btn--skew"><span>Conectics Login</span></a></div>
                    <p>
                        Do you need support?<br>
                        Get support from your familiar support team. <a href="#" class="link">Click here</a>!
                    </p>
                    
                </div>
            </div>
        </div>
    </div>
</section>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/footer.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>