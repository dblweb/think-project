<script src="/assets/build/combined.20190718114457.js"></script>


<button class="set">Set cookies</button> | <button class="clear">Clear cookies</button>


<script data-cfasync='false' src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

<script src="/assets/js/plugins/jquery.cookie.js"></script>
<!-- set a load of cookies -->
<script>

function clearListCookies(){
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++) {   
        var spcook =  cookies[i].split("=");
        document.cookie = spcook[0] + "=;expires=Thu, 21 Sep 1979 00:00:01 UTC;";                                
    }
}

$(document).ready( function() {
    $('button.set').on('click', function() {
        $.cookie('cookie1', 'cookie1', { expires: 365, path: '/' });
        $.cookie('cookie2', 'cookie2', { expires: 365, path: '/' });
        $.cookie('cookie3', 'cookie3', { expires: 365, path: '/' });
        $.cookie('cookie4', 'cookie4', { expires: 365, path: '/' });
        $.cookie('cookie5', 'cookie5', { expires: 365, path: '/' });
        $.cookie('cookie6', 'cookie6', { expires: 365, path: '/' });
        $.cookie('cookie7', 'cookie7', { expires: 365, path: '/' });
        location.reload();
    });

    $('button.clear').on('click', function() {
        removeCookies();
        location.reload();
    });
});
</script>
