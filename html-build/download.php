<?php 
    $page = [
        'title' => 'Download item',
        'template' => 'download-child-layout',
    ];
?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>

<section class="section-pad-sm">
    <div class="container paragraph-margin-lg">
        <div class="row">
            <div class="col-12 paragraph-margin-lg">
                <ul class="breadcrumbs h3">
                    <li><a href="">Dowmloads</a></li>
                    <li>BIM COLLABORATION</li>
                </ul>
                <h1 class="h1">BIM Collaboration Pocket Guide</h1>
            </div>
        </div>
        <hr>
    </div>
</section>
<section class="section-pad-sm">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="h3">FREE WHITEPAPER ON ISO 19650 AND MORE</div>
                <h2 class="h2">BIM Collaboration:<br> Guide to standards and practices</h2>

                <div class="bg-pink h2 padding">Download Whitepaper</div>
                <div class="bg-lpink padding"><em>marketo</em></div>
            </div>
            <div class="col-lg-6 offset-lg-2 cms-textarea--content">
                <p>For some time, Building Information Modelling has been the centre of attention in the construction industry. The increased use of BIM within the market has created the need to provide standards and best practices on how to adopt BIM. Several BIM standards now exist, focusing on techniques and requirements as well as organisational questions.</p>
                <p>In term of standards for organising information management in projects applying Building Information Modelling, the PAS 1192 has long been one of the guiding lights in the UK as well as internationally. This is not surprising, as the UK construction industry heads towards BIM maturity level 2. However, a new international BIM standard has been published recently: the ISO 19650. It provides an international view and approach on information management for BIM.</p>
                <p>With all these standards, it can be hard to keep track and know the key concepts of all areas. Our free paper “BIM Collaboration – Guide to standards and practices” has been written by our inhouse BIM experts to help you out!</p>
                <hr>

                <h3>Lost in BIM standards? Here is your overview!</h3>
                <ul>
                    <li>We categorise BIM standards</li>
                    <li>Every level of standards is shortly explained</li>
                    <li>Your bonus resource: a table of BIM standards indicating the online source</li>
                </ul>

                <h3>Special focus: ISO 19650 and BIM collaboration</h3>
                <ul>
                    <li>Core concepts of ISO 19650 and BS 1192 are explained</li>
                    <li>BIM Collaboration in the Common Data Environment</li>
                </ul>

                <h3>What do I get?</h3>
                <ul>
                    <li>A 10–page paper (average reading time: 15 minutes)</li>
                    <li>Free of charge</li>
                    <li>PDF delivered to you as a download link via email</li>
                    <li>The option to receive additional information on BIM and more (just check the box “I want to receive additional information by think project!”)</li>
                </ul>

                <h3>What do I have to do?</h3>
                <p>After filling out the form, you will receive an email with a download link. As a ISO 27001-certified software provider with headquarters in Germany, we take the protection of your data very seriously. To learn more, please take a look at our data protection statement.</p>
            </div>
        </div>
    </div>
</section>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/footer.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>