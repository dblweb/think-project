<?php 
    $page = [
        'title' => 'News and Events',
        'template' => 'news-events-layout',
    ];
?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>

<section class="section-pad-md">
    <div class="container paragraph-margin-lg">
        <div class="row align-items-center">
            <div class="col-lg-6 paragraph-margin-md">
                <h1 class="h3">News & Events</h1>
                <h2 class="h1">News and Events</h1>
            </div>
            <div class="col-lg-6">
                <div class="page-note paragraph-margin-sm">
                    <div class="h2 text-color--pink">Press contact</div>
                    <p>
                        Robyn Cook – Content Marketing Manager<br>
                        +44 (0) 1452 547 140 | robyn.cook@thinkproject.com
                    </p>
                </div>
            </div>
        </div>
        <div id="news-events--container">
            <a href="#" class="paragraph-margin-sm">
                <div class="h3">11/03/2019</div>
                <h3 class="h2 text-color--pink">think project! and ceapoint join forces</h3>
                <p>Munich-based think project!, a leading cloud-based Common Data Environment provider for construction and engineering projects, acquired 100% of the German BIM specialist ceapoint aec technologies GmbH. ceapoint’s product line DESITE will be positioned as an integrated and open solution within the think project! portfolio, and its cloud functionality will be expanded and distributed internationally in future.</p>
                <span class="link link--right">
                    <span>Read more</span>
                </span>
            </a>
            <a href="#" class="paragraph-margin-sm">
                <div class="h3">12/02/2019</div>
                <h3 class="h2 text-color--pink">think project! continues double-digit growth in 2018</h3>
                <p>Munich-based think project!, a leading cloud-based Common Data Environment provider for construction and engineering projects, grew revenue by over 20% during the financial year of 2018. Revenue for think project!, both in Germany and internationally, grew across all target segments (public and private asset owners, general contractors and project managers). This positive trend also led to an expansion of staff in development, sales and customer services, resulting in an overall 15% increase to approximately 300 people across the entire group. The year’s highlights included, amongst others, the acquisition of contract management specialist CEMAR, and numerous wins of strong customers.</p>
                <span class="link link--right">
                    <span>Read more</span>
                </span>
            </a>
            <a href="#" class="paragraph-margin-sm">
                <div class="h3">03/12/2018</div>
                <h3 class="h2 text-color--pink">Enabling mobile construction processes with think project! Quality 2.0</h3>
                <p>think project!, leading Common Data Environment (CDE) provider for construction and engineering projects, releases the latest version of the app think project! Quality. The app for quality management brings digital processes to the construction site in order to improve efficiency in construction projects.</p>
                <span class="link link--right">
                    <span>Read more</span>
                </span>
            </a>
            <a href="#" class="paragraph-margin-sm">
                <div class="h3">04/10/2018</div>
                <h3 class="h2 text-color--pink">think project! announces additional investment and launch of collaboration solution in France</h3>
                <p>Private-Equity-backed think project!, the leading Common Data Environment (CDE) provider for construction and engineering projects, announced it is strengthening its team in France to accelerate growth in the Software-as-a-Service (SaaS) market.</p>
                <span class="link link--right">
                    <span>Read more</span>
                </span>
            </a>
            <a href="#" class="paragraph-margin-sm">
                <div class="h3">05/06/2018</div>
                <h3 class="h2 text-color--pink">think project! announces additional investment and launch of collaboration solution in France</h3>
                <p>Private-Equity-backed think project!, the leading Common Data Environment (CDE) provider for construction and engineering projects, announced it is strengthening its team in France to accelerate growth in the Software-as-a-Service (SaaS) market.</p>
                <span class="link link--right">
                    <span>Read more</span>
                </span>
            </a>
            <a href="#" class="paragraph-margin-sm">
                <div class="h3">24/05/2018</div>
                <h3 class="h2 text-color--pink">CG Group and think project! establish a strategic partnership</h3>
                <p>think project!, the leading Common Data Environment (CDE) provider for construction and engineering projects, has publicly announced their strategic partnership with Germany’s leading project developer CG Group. The CG Group's executive management team initiated the strategic initiative of “Building Intelligence by CG (B-IQ)” in order to improve cost potentials by up to 25%. In particular, this initiative is currently being implemented for the “Vertical Villages” projects that involve the renovation and revitalisation of commercial properties. The focus of the partnership is on cross-enterprise collaboration, information management and BIM process management.</p>
                <span class="link link--right">
                    <span>Read more</span>
                </span>
            </a>
            <a href="#" class="paragraph-margin-sm">
                <div class="h3">07/11/2017</div>
                <h3 class="h2 text-color--pink">think project! appoints software veteran Peter Prestele to board of directors</h3>
                <p>Private-Equity-backed think project!, the leading Common Data Environment (CDE) provider for construction and engineering projects, announced it is strengthening its team in France to accelerate growth in the Software-as-a-Service (SaaS) market.</p>
                <span class="link link--right">
                    <span>Read more</span>
                </span>
            </a>
            <a href="#" class="paragraph-margin-sm">
                <div class="h3">08/02/2018</div>
                <h3 class="h2 text-color--pink">think project! appoints construction industry technology expert Gareth Burton to board of directors</h3>
                <p>think project!, the leading SaaS company for collaboration in construction and engineering, today announced the appointment of one of the construction industry’s leading Chief Information Officers, Gareth Burton, as a non-executive member of the Board of Directors. With the addition of Mr. Burton to the Board, think project! continues its successful growth strategy to target the construction SaaS market.</p>
                <span class="link link--right">
                    <span>Read more</span>
                </span>
            </a>
        </div>
        <div class="row">
            <div class="col-12 ta-c">
                <a href="http://welcome.thinkproject.com/en/portal/" class="menu-item btn btn--skew"><span>Show more</span></a>
            </div>
        </div>
    </div>
</section>


<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/footer.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>