<?php 
    $page = [
        'title' => 'Team Member',
        'template' => 'team-member-layout',
    ];
?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>

<section class="v-padding--sm">
    <div class="container eq-margin--sm">
        <div class="row eq-margin--sm">
            <div class="col-12">
                <ul class="breadcrumbs h3">
                    <li><a href="">About Us</a></li>
                    <li>Anton Hofmeir</li>
                </ul>
            </div>
            <div class="col-12">
                <h1 class="h1 margin--none">Anton Hofmeier</h1>
            </div>
            <div class="col-12 eq-margin--md">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="img-res">
                            <img src="https://via.placeholder.com/600x400.png/09f/fff">
                        </div>
                    </div>
                    <div class="col-xl-4  col-lg-6 eq-margin--sm">
                        <div>
                            <div class="h3">Germany</div>
                            <h3 class="h2 h2--sm margin--none">Anton Hofmeier</h4>
                        </div>
                        <p>In his role at think project!, Anton Hofmeier is responsible for the management of all sales units of the think project! Group.</p>
                        <hr>
                        <table class="h3">
                            <tbody>
                                <tr>
                                    <td>TITLE:</td>
                                    <td>CHIEF SALES OFFICER</td>
                                </tr>
                                <tr>
                                    <td>EDUCATION:</td>
                                    <td>MUNICH UNIVERSITY</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>

                    </div>
                </div>
                <hr>
                <div class="col-12">
                    <p>Over his many years of sales experience gathered at various software firms, he was responsible for the management of sales organisations, the implementation of Change Management processes, as well as the integration of new business units. He has a degree (German Diplom level) in electrical engineering and data technology from the Munich University of Applied Sciences. He has worked for think project! since the beginning of 2018.</p>
                </div>
                <hr>
            </div>
        </div>
    </div>
</section>

<section class="v-padding--sm">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 class="h1">Team</h3>
            </div>
            <div class="col-12">
                <h3 class="h2 h2--sm">Group Leadership Team</h3>
            </div>
        </div>
        <ul class="grid-list row">
            
            <li class="col-lg-4">
                <a href="#" class="eq-margin">
                    <div class="block--team-grid--img img-res">
                        <img src="https://via.placeholder.com/600x400.png/09f/fff">
                    </div>
                    <div>
                        <h4 class="h2 h2--sm text-color--pink margin--none">Gareth Burton</h4>
                        <div class="h3">Job title</div>
                    </div>
                    <div>
                        <span class="link link--right">
                            <span>Biography</span>
                            <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                                <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                            </svg>
                        </span>
                    </div>
                </a>
            </li>

            <li class="col-lg-4">
                <a href="#" class="eq-margin">
                    <div class="block--team-grid--img img-res">
                        <img src="https://via.placeholder.com/600x400.png/09f/fff">
                    </div>
                    <div>
                        <h4 class="h2 h2--sm text-color--pink margin--none">Gareth Burton</h4>
                        <div class="h3">Job title</div>
                    </div>
                    <div>
                        <span class="link link--right">
                            <span>Biography</span>
                            <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                                <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                            </svg>
                        </span>
                    </div>
                </a>
            </li>

            <li class="col-lg-4">
                <a href="#" class="eq-margin">
                    <div class="block--team-grid--img img-res">
                        <img src="https://via.placeholder.com/600x400.png/09f/fff">
                    </div>
                    <div>
                        <h4 class="h2 h2--sm text-color--pink margin--none">Gareth Burton</h4>
                        <div class="h3">Job title</div>
                    </div>
                    <div>
                        <span class="link link--right">
                            <span>Biography</span>
                            <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                                <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                            </svg>
                        </span>
                    </div>
                </a>
            </li>

            <li class="col-lg-4">
                <a href="#" class="eq-margin">
                    <div class="block--team-grid--img img-res">
                        <img src="https://via.placeholder.com/600x400.png/09f/fff">
                    </div>
                    <div>
                        <h4 class="h2 h2--sm text-color--pink margin--none">Gareth Burton</h4>
                        <div class="h3">Job title</div>
                    </div>
                    <div>
                        <span class="link link--right">
                            <span>Biography</span>
                            <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                                <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                            </svg>
                        </span>
                    </div>
                </a>
            </li>
        </ul>
    </div>
</section>

<section class="v-padding--sm">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 class="h2 h2--sm">Group Leadership Team</h3>
            </div>
        </div>
        <ul class="grid-list row">
            
            <li class="col-lg-4">
                <a href="#" class="eq-margin">
                    <div class="block--team-grid--img img-res">
                        <img src="https://via.placeholder.com/600x400.png/09f/fff">
                    </div>
                    <div>
                        <h4 class="h2 h2--sm text-color--pink margin--none">Gareth Burton</h4>
                        <div class="h3">Job title</div>
                    </div>
                    <div>
                        <span class="link link--right">
                            <span>Biography</span>
                            <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                                <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                            </svg>
                        </span>
                    </div>
                </a>
            </li>

            <li class="col-lg-4">
                <a href="#" class="eq-margin">
                    <div class="block--team-grid--img img-res">
                        <img src="https://via.placeholder.com/600x400.png/09f/fff">
                    </div>
                    <div>
                        <h4 class="h2 h2--sm text-color--pink margin--none">Gareth Burton</h4>
                        <div class="h3">Job title</div>
                    </div>
                    <div>
                        <span class="link link--right">
                            <span>Biography</span>
                            <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                                <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                            </svg>
                        </span>
                    </div>
                </a>
            </li>

            <li class="col-lg-4">
                <a href="#" class="eq-margin">
                    <div class="block--team-grid--img img-res">
                        <img src="https://via.placeholder.com/600x400.png/09f/fff">
                    </div>
                    <div>
                        <h4 class="h2 h2--sm text-color--pink margin--none">Gareth Burton</h4>
                        <div class="h3">Job title</div>
                    </div>
                    <div>
                        <span class="link link--right">
                            <span>Biography</span>
                            <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                                <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                            </svg>
                        </span>
                    </div>
                </a>
            </li>
        </ul>

        </div>
    </div>
</section>


<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/footer.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>