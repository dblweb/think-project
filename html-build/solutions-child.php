<?php 
    $page = [
        'title' => 'Documents',
        'template' => 'service-child-layout',
    ];
?>


<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>

<section class="hero hero--icon-text section-pad-md bg-orange">
    <div class="container paragraph-margin-md">
        <div class="row">
            <div class="col-12">
                <ul class="breadcrumbs h3">
                    <li><a href="">Solutions</a></li>
                    <li>Documents</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-4"><img src="https://via.placeholder.com/120x120.png/09f/fff"></div>
                    <div class="col-lg-8 offset-lg-1 col-md-7 offset-md-1">
                        <h1 class="h1">Document Management</h1>
                    </div>
                </div>
                <hr>
            </div>
            <div class="hero--icon-text__col col-lg-6 offset-lg-1 paragraph-margin-md newspaper-col-2">
                <p>Nowadays project teams have to deal with an increasing amount of documents in their daily work.</p>
                <p>Written correspondence alone can fill entire racks of files. Despite meticulous allocation to a set of labelled files, the search for a specific piece of information through all these many pieces of paper is time-consuming and sometimes completely in vain.</p>
                <p>think project! offers you a common data environment, a cloud based platform to store, manage and distribute your project documents efficiently and in a structured manner.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="#" class="link link--down scroll--nxt">
                    <span>Find out more</span>
                    <svg class="icon--arrow-d icon--svg" viewBox="0 0 20 86">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-d"></use>
                    </svg>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="block--list section-pad-md">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-lg-7 paragraph-margin-md">
                <h3 class="h1">Your advantages</h3>
                <ul class="list">
                    <li>Ensure instant availability for all project members according to their access rights</li>
                    <li>Profit from an auditable documentation of all document movements</li>
                    <li>Keep all project members up to date (“Single version of truth”)</li>
                </ul>
            </div>
            <div class="col-lg-4 offset-lg-1 paragraph-margin-md">
               <div class="container">
                    <div class="row">
                        <div class="col-lg-10 offset-lg-2 page-note">
                            Project services on demand are currently only available in German-speaking countries.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-12">
            <hr>
        </div>
    </div>
</div>

<section class="block--advantage-grid section-pad-md">
    <div class="container">
        <div class="row">

            <div class="block--advantage-grid--item col-lg-6">
                <div class="container">
                    <div class="row">
                        <div class="block--advantage-grid--icon col-3">
                            <div class="img-res">
                                <img src="https://via.placeholder.com/200x200.png/09f/fff">
                            </div>
                        </div>
                        <div class="block--advantage-grid--container col-8 paragraph-margin-sm">
                            <h4 class="h2 h2--sm">Classification</h4>
                            <p class="paragraph-lg text-opacity">Structure your digital document management!</p>
                            <ul class="list">
                                <li>Store your documents according to specific project or company requirements, e.g. subsidiaries, sub-projects, trades</li>
                                <li>Assign access rights based on your predefined filing criteria</li>
                           </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="block--advantage-grid--item col-lg-6">
                <div class="container">
                    <div class="row">
                        <div class="block--advantage-grid--icon col-3">
                            <div class="img-res">
                                <img src="https://via.placeholder.com/200x200.png/09f/fff">
                            </div>
                        </div>
                        <div class="block--advantage-grid--container col-8 paragraph-margin-sm">
                            <h4 class="h2 h2--sm">Collaborate</h4>
                            <p class="paragraph-lg text-opacity">Keep your project team up to date!</p>
                            <ul class="list">
                                <li>Automatically distribute documents according to predefined attributes/rules, e.g. trade, construction phase etc.</li>
                                <li>Distribute documents to individuals or defined groups of people</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="sign-post--carousel section-pad-sm bg-blue">
    <div class="container paragraph-margin-lg ta-c">
        <div class="row paragraph-margin-none">
            <h3 class="col-12 h1">Other products:</h3>
        </div> 
        <div class="row">
            <div class="carousel col-12">
                <div class="carousel--wrapper">
                    <div class="carousel--item">
                        <a href="#" class="paragraph-margin-sm">
                            <div class="img-res">
                                <img src="https://via.placeholder.com/200x200.png/09f/fff" />
                            </div>
                            <div class="h2 h2--sm">Expert</div>
                        </a>
                    </div>
                    <div class="carousel--item">
                        <a href="#" class="paragraph-margin-sm">
                            <div class="img-res">
                                <img src="https://via.placeholder.com/200x200.png/09f/fff" />
                            </div>
                            <div class="h2 h2--sm">Expert</div>
                        </a>
                    </div>
                    <div class="carousel--item">
                        <a href="#" class="paragraph-margin-sm">
                            <div class="img-res">
                                <img src="https://via.placeholder.com/200x200.png/09f/fff" />
                            </div>
                            <div class="h2 h2--sm">Expert</div>
                        </a>
                    </div>
                    <div class="carousel--item">
                        <a href="#" class="paragraph-margin-sm">
                            <div class="img-res">
                                <img src="https://via.placeholder.com/200x200.png/09f/fff" />
                            </div>
                            <div class="h2 h2--sm">Expert</div>
                        </a>
                    </div>
                    <div class="carousel--item">
                        <a href="#" class="paragraph-margin-sm">
                            <div class="img-res">
                                <img src="https://via.placeholder.com/200x200.png/09f/fff" />
                            </div>
                            <div class="h2 h2--sm">Expert</div>
                        </a>
                    </div>
                    <div class="carousel--item">
                        <a href="#" class="paragraph-margin-sm">
                            <div class="img-res">
                                <img src="https://via.placeholder.com/200x200.png/09f/fff" />
                            </div>
                            <div class="h2 h2--sm">Expert</div>
                        </a>
                    </div>
                    <div class="carousel--item">
                        <a href="#" class="paragraph-margin-sm">
                            <div class="img-res">
                                <img src="https://via.placeholder.com/200x200.png/09f/fff" />
                            </div>
                            <div class="h2 h2--sm">Expert</div>
                        </a>
                    </div>
                    <div class="carousel--item">
                        <a href="#" class="paragraph-margin-sm">
                            <div class="img-res">
                                <img src="https://via.placeholder.com/200x200.png/09f/fff" />
                            </div>
                            <div class="h2 h2--sm">Expert</div>
                        </a>
                    </div>
               </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-12">
                <a href="#" class="link link--left">
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-l"></use>
                    </svg>
                    <span>
                        back to products
                    </span>
                </a>
            </div>
        </div> 
    </div>
</section>


<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/footer.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>