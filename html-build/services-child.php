<?php 
    $page = [
        'title' => 'Project Service',
        'template' => 'services-child-layout',
    ];
?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>

<section class="hero hero--icon-text section-pad-md bg-pink">
    <div class="container paragraph-margin-md">
        <div class="row">
            <div class="col-12">
                <ul class="breadcrumbs h3">
                    <li><a href="">Services</a></li>
                    <li>Project Service</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-4"><img src="https://via.placeholder.com/120x120.png/09f/fff"></div>
                    <div class="col-lg-8 offset-lg-1 col-md-7 offset-md-1">
                        <h1 class="h1">Project <br>Service</h1>
                    </div>
                </div>
                <hr>
            </div>
            <div class="hero--icon-text__col col-lg-6 offset-lg-1 paragraph-margin-md">
                <p>Collaboration and information management involves a range of organizational tasks that can be outsourced in case dedicated staff are not available for the project.</p>
                <p>think project! – in cooperation with the associated company planConnect – offers highly efficient project services on demand.think project! offers you a common data environment, a cloud based platform to store, manage and distribute your project documents efficiently and in a structured manner.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="#" class="link link--down scroll--nxt">
                    <span>Find out more</span>
                    <svg class="icon--arrow-d icon--svg" viewBox="0 0 20 86">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-d"></use>
                    </svg>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="block--list section-pad-md">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-lg-7 paragraph-margin-md">
                <h3 class="h1">Project services at a glance</h3>
                <ul class="list">
                    <li>Contact your personal project assistant any time you need help</li>
                    <li>Ensure that access to relevant information is just a phone call away</li>
                    <li>Outsource information management and/or document control services depending on project size</li>
                    <li>Scale support services to fit your changing requirements</li>
                    <li>Get help to undergo the transition from paper-based to digital information</li>
                </ul>
            </div>
            <div class="col-lg-4 offset-lg-1 paragraph-margin-md">
               <div class="container">
                    <div class="row">
                        <div class="col-lg-10 offset-lg-2 page-note">
                            Project services on demand are currently only available in German-speaking countries.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="sign-post--col section-pad-sm bg-blue">
    <div class="container paragraph-margin-lg ta-c ">
        <div class="row">
            <div class="sign-post--col--item col-lg-6">
                <a href="#" class=" paragraph-margin-sm">
                    <div class="img-res"><img src="https://via.placeholder.com/200x200.png/09f/fff"></div>
                    <h4 class="h1">Consultancy</h4>
                    <p>Try our consultancy options, offering qualified consulting and services.</p>
                    <div>
                        <span class="link link--right">
                            <span>Find out more</span>
                            <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                                <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                            </svg>
                        </span>    
                    </div>
                </a>
            </div> 
            <div class="sign-post--col--item col-lg-6">
                <a href="#" class="paragraph-margin-sm">
                    <div class="img-res"><img src="https://via.placeholder.com/200x200.png/09f/fff"></div>
                    <h4>Other products</h4>
                    <p>Do you need a single solution? If so, we have off the shelf options for your business.</p>
                    <div>
                        <span class="link link--right">
                            <span>Find out more</span>
                            <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                                <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                            </svg>
                        </span>
                    </div>
                </a>
            </div> 
        </div>
        <div>
            <a href="#" class="link link--left">
                <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                    <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-l"></use>
                </svg>
                <span>
                    back to products
                </span>
            </a>
        </div>
    </div>
</section>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/footer.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>