<?php 
    $page = [
        'title' => 'Contact',
        'template' => 'contact-layout',
    ];
?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>

<section class="bg-lgrey section-pad-sm">
    <div class="container paragraph-margin-lg">
        <div class="row">
            <div class="col-12 paragraph-margin-md">
                <h1 class="h3">Contact</h1>
                <h2 class="h1">Get in touch:</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 paragraph-margin-md">
                <p>Would like to send a message to the think project! team? Please fill out this contact form, including your message and contact information. We will get back to you as soon as possible!</p>
                <p>We are looking forward to receiving your message!</p>
            </div>
            <div class="col-lg-8">
                <!--<form>
                    <div class="row">
                        <div class="col-2">
                            <label class="form-element--radio">
                                <span>Mr</span>
                                <input class="form-element--radio" type="radio" name="prefix" id="mr" value="mr">
                            </label>
                        </div>
                        <div class="col-2">
                            <label class="form-element--radio">
                                <span>Mrs</span>
                                <input class="form-element--radio" type="radio" name="prefix" id="mrs" value="mrs">
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <input class="form-element" type="text" placeholder="First name">
                        </div>
                        <div class="col-lg-6">
                            <input class="form-element" type="text" placeholder="Last name">
                        </div>
                        <div class="col-lg-6">
                            <input class="form-element" type="text" placeholder="Email Address">
                        </div>
                        <div class="col-lg-6">
                            <input class="form-element" type="text" placeholder="Phone Number">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <textarea class="form-element" rows="1" placeholder="Message"></textarea>
                        </div>
                    </div>
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="form--checkbox">
                                <input type="checkbox" id="checkbox1" class="hidden">
                                <label for="checkbox1">
                                    <span></span>
                                    I have read the data protection statement and accept it.
                                </label>
                            </div>
                            <div class="form--checkbox">
                                <input type="checkbox" id="checkbox2" class="hidden">
                                <label for="checkbox2">
                                    <span></span>
                                    I would like to receive more information about think project!
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-4 ta-r">
                            <button class="menu-item btn btn--skew btn--pink"><span>Submit</span></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 form--sm-print">
                            <p>The information you provide will only be used to answer your request, unless you check the box “I want to receive more information by think project!”. In this case we will keep you up to date about upcoming events as well as trends and tips in construction and engineering with regards to digitalisation, collaboration, BIM and much more.</p>
                        </div>
                </form>-->
            </div>
        </div>
    </div>
</section>


<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/footer.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>