<?php 
    $page = [
        'title' => 'Downloads',
        'template' => 'downloads-layout',
    ];
?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>

<section class="section-pad-md">
    <div class="container paragraph-margin-md">
        <div class="row">
            <div class="col-lg-6 paragraph-margin-md">
                <h1 class="h3">Downloads</h1>
                <h2 class="h1">Downloads</h1>
            </div>
        </div>
        <div class="downloads--grid row">
            <div class="col-lg-4">
                <a href="#" class="bg-lgrey">
                    <div class="row">
                        <div class="col-4 ta-c">
                            <img src="https://via.placeholder.com/80x100.png/09f/fff">
                        </div>
                        <div class="col-8 paragraph-margin-xs">
                            <div class="h3">WHITEPAPER</div>
                            <div class="h2 text-color--pink">BIM collaboration pocket guide to standards</div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4">
                <a href="#" class="bg-lgrey">
                    <div class="row">
                        <div class="col-4 ta-c">
                            <img src="https://via.placeholder.com/80x100.png/09f/fff">
                        </div>
                        <div class="col-8 paragraph-margin-xs">
                            <div class="h3">WHITEPAPER</div>
                            <div class="h2 text-color--pink">BIM collaboration pocket guide to standards</div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4">
                <a href="#" class="bg-lgrey">
                    <div class="row">
                        <div class="col-4 ta-c">
                            <img src="https://via.placeholder.com/80x100.png/09f/fff">
                        </div>
                        <div class="col-8 paragraph-margin-xs">
                            <div class="h3">WHITEPAPER</div>
                            <div class="h2 text-color--pink">BIM collaboration pocket guide to standards</div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4">
                <a href="#" class="bg-lgrey">
                    <div class="row">
                        <div class="col-4 ta-c">
                            <img src="https://via.placeholder.com/80x100.png/09f/fff">
                        </div>
                        <div class="col-8 paragraph-margin-xs">
                            <div class="h3">WHITEPAPER</div>
                            <div class="h2 text-color--pink">BIM collaboration pocket guide to standards</div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4">
                <a href="#" class="bg-lgrey">
                    <div class="row">
                        <div class="col-4 ta-c">
                            <img src="https://via.placeholder.com/80x100.png/09f/fff">
                        </div>
                        <div class="col-8 paragraph-margin-xs">
                            <div class="h3">WHITEPAPER</div>
                            <div class="h2 text-color--pink">BIM collaboration pocket guide to standards</div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4">
                <a href="#" class="bg-lgrey">
                    <div class="row">
                        <div class="col-4 ta-c">
                            <img src="https://via.placeholder.com/80x100.png/09f/fff">
                        </div>
                        <div class="col-8 paragraph-margin-xs">
                            <div class="h3">WHITEPAPER</div>
                            <div class="h2 text-color--pink">BIM collaboration pocket guide to standards</div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4">
                <a href="#" class="bg-lgrey">
                    <div class="row">
                        <div class="col-4 ta-c">
                            <img src="https://via.placeholder.com/80x100.png/09f/fff">
                        </div>
                        <div class="col-8 paragraph-margin-xs">
                            <div class="h3">WHITEPAPER</div>
                            <div class="h2 text-color--pink">BIM collaboration pocket guide to standards</div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/footer.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>