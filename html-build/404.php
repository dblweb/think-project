<?php 
    $page = [
        'title' => 'Error',
        'template' => 'error-layout',
    ];
?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>

<section class="hero--border-col bg-green section-pad-lg section-fh">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="breadcrumbs h3">
                    <li>Error</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
            <div class="hero--border-col--2col-wrapper row align-items-center">
                <div class="error-page--code col-xl-5 col-lg-6">
                    <div class="error-number text-color--blue">404</div>
                </div>
                <div class="error-page--border"></div>
                    <div class="error-page--description col-xl-5 offset-xl-2 col-lg-6  paragraph-margin-sm ">
                        <div class="h1 h1--lg">Oops, the page you’re looking for could not be found.</div>
                        <a href="#" class="btn btn--skew"><span>Back home</span></a>
                        <div class="h2">What are you looking for?</div>
                        <div class="form-element--wrapper form-element--icon bg-green">
                            <input class="form-element bg-green" type="text" placeholder="Search">
                            <svg class="icon--search" viewBox="0 0 24 24">
                                <use xlink:href="/assets/svg/svg-defs.svg#icon--search"></use>
                            </svg>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</section>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>