<?php 
    $page = [
        'title' => 'Locations',
        'template' => 'location-layout',
    ];
?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>

<section class="section-pad-sm">
    <div class="container paragraph-margin-md">
        <div class="row">
            <h1 class="col-12 h3">LOCATIONS</h1>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <h2>Our locations and partner networks</h2>
            </div>
            <div class="col-lg-8">
                <div class="img-res">
                    <img src="https://via.placeholder.com/820x400.png/09f/fff">
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <hr>
    </div>
</section>

<section class="section-pad-sm">
    <div class="container paragraph-margin-md">
        <div class="row">
            <h3 class="col-12 h2 text-color--green">Think project! Companies and offices</h3>
        </div>
        <ul class="grid-list row">
            
            <li class="col-lg-3 col-md-4 paragraph-margin-xs">
                <div class="h2 h2--xs">Parent company</div>
                <p>tp Holding GmbH <br>
                Zamdorfer Strasse 100 <br>
                81677 Munich <br>
                Germany</p>
                <p>
                <p>Phone +49 89 930839 300 <br>
                info(at)thinkproject.com <br>
                www.thinkproject.com <br>
                <a href="#" class="link">Google Maps</a>
            </li>

            <li class="col-lg-3 col-md-4 paragraph-margin-xs">
                <div class="h2 h2--xs">Distribution and service Germany</div>
                <p>think project! – Munich office<br>
                Zamdorfer Strasse 100 <br>
                81677 Munich <br>
                Germany</p>
                <p>
                <p>Phone +49 89 930839 300 <br>
                info(at)thinkproject.com <br>
                www.thinkproject.com <br>
                <a href="#" class="link">Google Maps</a>
            </li>

            <li class="col-lg-3 col-md-4 paragraph-margin-xs">
                <div class="h2 h2--xs">Distribution and service Germany</div>
                <p>think project! – Cologne office <br>
                (Conetics)<br>
                Takustrasse 1 <br>
                50825 Cologne <br>
                Germany</p>
                <p>
                <p>Phone +49 221 292 567 0 <br>
                info(at)thinkproject.com <br>
                www.thinkproject.com <br>
                <a href="#" class="link">Google Maps</a>
            </li>

            <li class="col-lg-3 col-md-4 paragraph-margin-xs">
                <div class="h2 h2--xs">Product development</div>
                <p>think project! GmbH Berlin <br>
                Eichenstrasse 3b <br>
                12435 Berlin <br>
                Germany</p>
                <p>
                <p>Phone +49 89 930839 300 <br>
                info(at)thinkproject.com <br>
                www.thinkproject.com <br>
                <a href="#" class="link">Google Maps</a>
            </li>

            <li class="col-lg-3 col-md-4 paragraph-margin-xs">
                <div class="h2 h2--xs">Product development</div>
                <p>think project! R&D Poland <br>
                ul. Cyfrowa 6 <br>
                71–441 Szczecin <br>
                Poland</p>
                <p>
                <p>Phone +49 89 930839 300 <br>
                info(at)thinkproject.com <br>
                www.thinkproject.com <br>
                <a href="#" class="link">Google Maps</a>
            </li>

            <li class="col-lg-3 col-md-4 paragraph-margin-xs">
                <div class="h2 h2--xs">Distribution and service Austria</div>
                <p>think project! Österreich <br>
                Landesstrasse 23 <br>
                5302 Henndorf am Wallersee <br>
                Austria</p>
                <p>
                <p>Phone +43 6214 20060 0 <br>
                austria(at)thinkproject.com <br>
                www.thinkproject.at <br>
                <a href="#" class="link">Google Maps</a>
            </li>

            <li class="col-lg-3 col-md-4 paragraph-margin-xs">
                <div class="h2 h2--xs">Distribution and service Austria</div>
                <p>think project! Österreich <br>
                Landesstrasse 23 <br>
                5302 Henndorf am Wallersee <br>
                Austria</p>
                <p>
                <p>Phone +43 6214 20060 0 <br>
                austria(at)thinkproject.com <br>
                www.thinkproject.at <br>
                <a href="#" class="link">Google Maps</a>
            </li>

            <li class="col-lg-3 col-md-4 paragraph-margin-xs">
                <div class="h2 h2--xs">Distribution and service Austria</div>
                <p>think project! Österreich <br>
                Landesstrasse 23 <br>
                5302 Henndorf am Wallersee <br>
                Austria</p>
                <p>
                <p>Phone +43 6214 20060 0 <br>
                austria(at)thinkproject.com <br>
                www.thinkproject.at <br>
                <a href="#" class="link">Google Maps</a>
            </li>

            <li class="col-lg-3 col-md-4 paragraph-margin-xs">
                <div class="h2 h2--xs">Distribution and service Austria</div>
                <p>think project! Österreich <br>
                Landesstrasse 23 <br>
                5302 Henndorf am Wallersee <br>
                Austria</p>
                <p>
                <p>Phone +43 6214 20060 0 <br>
                austria(at)thinkproject.com <br>
                www.thinkproject.at <br>
                <a href="#" class="link">Google Maps</a>
            </li>

        </ul>
    </div>
</section>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/footer.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>