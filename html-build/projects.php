<?php 
    $page = [
        'title' => 'Project reports',
        'template' => 'project-reports-layout',
    ];
?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>


<section class="section-pad-sm">
    <div class="container paragraph-margin-md">
        <div class="row">
            <div class="col-12"><h1 class="h3">Success Stories</h1></div>
        </div>
        <div class="row align-items-center">
            <div class="col-lg-6 paragraph-margin-md">
                
                <h2 class="h1">Project reports</h2>
            </div>
            <div class="col-lg-6 ta-r">
            <select class="form--select filter-menu">
                <option value="" disabled selected>Country / Region</option>
                <option value="region1">region 1</option>
                <option value="region2">region 2</option>
                <option value="region3">region 3</option>
            </select>
            <select class="form--select filter-menu">
                <option value="" disabled selected>Industry</option>
                <option value="industry1">industry 1</option>
                <option value="industry2">industry 2</option>
                <option value="industry3">industry 3</option>
            </select>
            
            </div>
        </div>
        <div class="project--grid row">

            <a href="#"  class="col-lg-6">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="img-res">
                            <img src="https://via.placeholder.com/290x240.png/09f/fff" />
                        </div>
                    </div>
                    <div class="col-lg-6 paragraph-margin-xs">
                        <div class="h3">Laa an der Thsya - Austria</div>
                        <h3 class="h2">Vitality World Thermal bath Laa</h3>
                        <hr>
                        <div class="h3">
                            <div>VAMED</div>
                            <div>HOTEL & TOURISM</div>
                            <div>BUILDING CONSTRUCTION</div>
                        </div>
                        <div>
                            <span href="#" class="link link--right">
                                <span>View project</span>
                                <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                                    <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                                </svg>
                            </span>
                        </div>
                    </div>
                </div>
            </a>

            <a href="#"  class="col-lg-6">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="img-res">
                            <img src="https://via.placeholder.com/290x240.png/09f/fff" />
                        </div>
                    </div>
                    <div class="col-lg-6 paragraph-margin-xs">
                        <div class="h3">Laa an der Thsya - Austria</div>
                        <h3 class="h2">Vitality World Thermal bath Laa</h3>
                        <hr>
                        <div class="h3">
                            <div>VAMED</div>
                            <div>HOTEL & TOURISM</div>
                            <div>BUILDING CONSTRUCTION</div>
                        </div>
                        <div>
                            <span href="#" class="link link--right">
                                <span>View project</span>
                                <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                                    <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                                </svg>
                            </span>
                        </div>
                    </div>
                </div>
            </a>

            <a href="#"  class="col-lg-6">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="img-res">
                            <img src="https://via.placeholder.com/290x240.png/09f/fff" />
                        </div>
                    </div>
                    <div class="col-lg-6 paragraph-margin-xs">
                        <div class="h3">Laa an der Thsya - Austria</div>
                        <h3 class="h2">Vitality World Thermal bath Laa</h3>
                        <hr>
                        <div class="h3">
                            <div>VAMED</div>
                            <div>HOTEL & TOURISM</div>
                            <div>BUILDING CONSTRUCTION</div>
                        </div>
                        <div>
                            <span href="#" class="link link--right">
                                <span>View project</span>
                                <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                                    <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                                </svg>
                            </span>
                        </div>
                    </div>
                </div>
            </a>

            <a href="#"  class="col-lg-6">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="img-res">
                            <img src="https://via.placeholder.com/290x240.png/09f/fff" />
                        </div>
                    </div>
                    <div class="col-lg-6 paragraph-margin-xs">
                        <div class="h3">Laa an der Thsya - Austria</div>
                        <h3 class="h2">Vitality World Thermal bath Laa</h3>
                        <hr>
                        <div class="h3">
                            <div>VAMED</div>
                            <div>HOTEL & TOURISM</div>
                            <div>BUILDING CONSTRUCTION</div>
                        </div>
                        <div>
                            <span href="#" class="link link--right">
                                <span>View project</span>
                                <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                                    <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                                </svg>
                            </span>
                        </div>
                    </div>
                </div>
            </a>
            <a href="#"  class="col-lg-6">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="img-res">
                            <img src="https://via.placeholder.com/290x240.png/09f/fff" />
                        </div>
                    </div>
                    <div class="col-lg-6 paragraph-margin-xs">
                        <div class="h3">Laa an der Thsya - Austria</div>
                        <h3 class="h2">Vitality World Thermal bath Laa</h3>
                        <hr>
                        <div class="h3">
                            <div>VAMED</div>
                            <div>HOTEL & TOURISM</div>
                            <div>BUILDING CONSTRUCTION</div>
                        </div>
                        <div>
                            <span href="#" class="link link--right">
                                <span>View project</span>
                                <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                                    <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                                </svg>
                            </span>
                        </div>
                    </div>
                </div>
            </a>
            <a href="#"  class="col-lg-6">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="img-res">
                            <img src="https://via.placeholder.com/290x240.png/09f/fff" />
                        </div>
                    </div>
                    <div class="col-lg-6 paragraph-margin-xs">
                        <div class="h3">Laa an der Thsya - Austria</div>
                        <h3 class="h2">Vitality World Thermal bath Laa</h3>
                        <hr>
                        <div class="h3">
                            <div>VAMED</div>
                            <div>HOTEL & TOURISM</div>
                            <div>BUILDING CONSTRUCTION</div>
                        </div>
                        <div>
                            <span href="#" class="link link--right">
                                <span>View project</span>
                                <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                                    <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                                </svg>
                            </span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="row">
            <div class="col-12 ta-c">
                <a href="http://welcome.thinkproject.com/en/portal/" class="menu-item btn btn--skew"><span>Show more</span></a>
            </div>
        </div>
    </div>
</section>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/footer.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>