<?php 
    $page = [
        'title' => 'Search',
        'template' => 'search-layout',
    ];
?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>

<section class="section-pad-sm">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="h3">Search</h1>
            </div>
        </div>
        <div class="row paragraph-margin-sm">
            <div class="col-12 ta-c">
                <div class="form-element--wrapper form-element--icon" style="width: 100%; max-width: 350px;">
                    <input class="form-element" type="text" placeholder="Search">
                    <svg class="icon--search" viewBox="0 0 24 24">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--search"></use>
                    </svg>
                </div>
            </div>
            <div class="col-12 ta-c">
                3 Results<hr>
            </div>
        </div>
        <ul class="grid-list search--grid row t-margin--md">
            
            <li class="search--grid__item col-lg-6">
                <div class="h3">SUCCESS STORIES / PROJECT REPORTS</div>
                <h4 class="h2 text-color--pink">Vitality World Thermal bath Laa</h4>
                <p>In Northern Weinviertel the latest extension of Thermal bath Laa, was opened. Starting December 2016, construction took 17 months including full maintenance operations.</p>
                <a href="#" class="link link--right">
                    <span>Visit page<span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </span></span></a>
            </li>

            <li class="search--grid__item col-lg-6">
                <div class="h3">SUCCESS STORIES / PROJECT REPORTS</div>
                <h4 class="h2 text-color--pink">Vitality World Thermal bath Laa</h4>
                <p>In Northern Weinviertel the latest extension of Thermal bath Laa, was opened. Starting December 2016, construction took 17 months including full maintenance operations.</p>
                <a href="#" class="link link--right">
                    <span>Visit page<span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </span></span></a>
            </li>

            <li class="search--grid__item col-lg-6">
                <div class="h3">SUCCESS STORIES / PROJECT REPORTS</div>
                <h4 class="h2 text-color--pink">Vitality World Thermal bath Laa</h4>
                <p>In Northern Weinviertel the latest extension of Thermal bath Laa, was opened. Starting December 2016, construction took 17 months including full maintenance operations.</p>
                <a href="#" class="link link--right">
                    <span>Visit page<span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </span></span></a>
            </li>

        </ul>
    </div>
</section>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/footer.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>