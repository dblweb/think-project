<?php 
    $page = [
        'title' => 'Solutions',
        'template' => 'solutions-layout',
    ];
?>


<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>

<section class="hero hero--img-text section-pad-md bg-orange">
    <div class="container">
        <div class="row align-items-top">
            <div class="hero--img-text__col col-lg-6 paragraph-margin-md">
                <h1 class="h3">Solutions</h1>
                <h2 class="h1">A range of products, for the way you like to work</h2>
                <p>Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, et at consectetuer adipiscing elit, dolor sit rem ipsum dolor sit amet Lorem ipsum dolor sit amet, Lorem ipsum dolor, consectet.</p>
                <a href="#" class="link link--down scroll--nxt">
                    <span>Find out more</span>
                    <svg class="icon--arrow-d icon--svg" viewBox="0 0 20 86">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-d"></use>
                    </svg>
                </a>
            </div>
            <div class="hero--img-text__col col-lg-5 offset-lg-1">
                <div class="img-res">
                    <img src="https://via.placeholder.com/800x600.png/09f/fff">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="hero-secondary hero-secondary--grid section-pad-md bg-orange">
    <div class="container">
        <div class="row justify-content-center ta-c">
            <a href="#" class="hero-secondary--grid__item col-lg-3 col-md-6 paragraph-margin-xs">
                <img src="https://via.placeholder.com/120x120.png/09f/fff">
                <h3 class="h2 h2--sm">Task Management</h3>
                <p>Lorem ipsum dolor sit amet, Lorem adipiscing elit, dolor sit rem ipsum dolor sit amet.</p>
            </a>
            <a href="#" class="hero-secondary--grid__item col-lg-3 col-md-6 paragraph-margin-xs">
                <img src="https://via.placeholder.com/120x120.png/09f/fff">
                <h3 class="h2 h2--sm">Document Management</h3>
                <p>Lorem ipsum dolor sit amet, Lorem adipiscing elit, dolor sit rem ipsum dolor sit amet.</p>
            </a>
            <a href="#" class="hero-secondary--grid__item col-lg-3 col-md-6 paragraph-margin-xs">
                <img src="https://via.placeholder.com/120x120.png/09f/fff">
                <h3 class="h2 h2--sm">BIM</h3>
                <p>Lorem ipsum dolor sit amet, Lorem adipiscing elit, dolor sit rem ipsum dolor sit amet.</p>
            </a>
            <a href="#" class="hero-secondary--grid__item col-lg-3 col-md-6 paragraph-margin-xs">
                <img src="https://via.placeholder.com/120x120.png/09f/fff">
                <h3 class="h2 h2--sm">Services</h3>
                <p>Lorem ipsum dolor sit amet, Lorem adipiscing elit, dolor sit rem ipsum dolor sit amet.</p>
            </a>
            <a href="#" class="hero-secondary--grid__item col-lg-3 col-md-6 paragraph-margin-xs">
                <img src="https://via.placeholder.com/120x120.png/09f/fff">
                <h3 class="h2 h2--sm">Quality Management</h3>
                <p>Lorem ipsum dolor sit amet, Lorem adipiscing elit, dolor sit rem ipsum dolor sit amet.</p>
            </a>
            <a href="#" class="hero-secondary--grid__item col-lg-3 col-md-6 paragraph-margin-xs">
                <img src="https://via.placeholder.com/120x120.png/09f/fff">
                <h3 class="h2 h2--sm">Contracts</h3>
                <p>Lorem ipsum dolor sit amet, Lorem adipiscing elit, dolor sit rem ipsum dolor sit amet.</p>
            </a>
            <a href="#" class="hero-secondary--grid__item col-lg-3 col-md-6 paragraph-margin-xs">
                <img src="https://via.placeholder.com/120x120.png/09f/fff">
                <h3 class="h2 h2--sm">Tender Management</h3>
                <p>Lorem ipsum dolor sit amet, Lorem adipiscing elit, dolor sit rem ipsum dolor sit amet.</p>
            </a>
            <a href="#" class="hero-secondary--grid__item col-lg-3 col-md-6 paragraph-margin-xs">
                <img src="https://via.placeholder.com/120x120.png/09f/fff">
                <h3 class="h2 h2--sm">Insights and Integration</h3>
                <p>Lorem ipsum dolor sit amet, Lorem adipiscing elit, dolor sit rem ipsum dolor sit amet.</p>
            </a>
        </div>
    </div>
</section>

<section class="block--img-text__invert section-pad-md">
    <div class="container">
        <div class="row align-items-top">
            <div class="block--img-text__text col-lg-6 paragraph-margin-md">
                <h2 class="h1">Looking for something more bespoke?</h2>
                <p>think project! has gained experience from thousands of construction projects and can therefore offer qualified consulting and services including identification of requirements, roll-out planning, software implementation, on-demand and ongoing support, maintenance and user training.</p>
                <a href="#" class="link link--right">
                    <span>Services</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </a>
            </div>
            <div class="block--img-text__img col-lg-6 paragraph-margin-md">
                <div class="img-res">
                    <img src="https://via.placeholder.com/600x400.png/09f/fff">
                </div>
            </div>
        </div>
    </div>
</section>


<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/footer.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>