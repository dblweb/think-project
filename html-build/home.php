<?php 
    $page = [
        'title' => 'Home',
        'template' => 'home-layout',
    ];
?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/head.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/header.php'; ?>

<section class="hero hero--center-text bg-blue section-pad-lg">
    <div class="container">

        <!-- shapes -->

        <!-- shapes -->

        <div class="row ta-c">
            <div class="col-12 paragraph-margin-lg">
                <h2 class="h1 h1--lg">
                    Breaking down the barriers between digital and construction
                </h2>
                <p class="paragraph-lg">
                thinkproject is a collective of industry leading products and professionals with one clear goal: To develop and deliver world-class products to support, connect and advance the industry and the people in it.
                </p>
                <a href="#" class="link link--down scroll--nxt">
                    <span>Find out more<span>
                    <svg class="icon--arrow-d icon--svg" viewBox="0 0 20 86">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-d"></use>
                    </svg>
                </a>
            </div>
        </div>

    </div>
</section>

<section class="block--img-text section-pad-lg bg-blue shape-divider">
    <div class="container">

        <!-- shapes -->
        
        <!-- shapes -->
        
        <div class="row align-items-top">
            <div class="block--img-text__text col-lg-6 paragraph-margin-md">
                <h2 class="h1">Let us make things simple</h2>
                <p>Your world is complex, so let’s make things simple and give you the tools to collate and connect project data in one place, for all to see. We have curated a suite of products that connect you and your colleagues through a common data environment enabling you to take control of your projects and deliver world-class results.</p>
                <a href="#" class="link link--right">
                    <span>Our solutions<span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </a>
            </div>
            <div class="block--img-text__img  col-lg-6 paragraph-margin-md ">
                <div class="img-res">
                    <img src="https://via.placeholder.com/600x600.png/09f/fff">
                </div>
            </div>
        </div>

    </div>
</section>

<section class="block--img-text__invert section-pad-md">
    <div class="container">

        <div class="row align-items-center">
            <div class="block--img-text__text  col-lg-6 paragraph-margin-md">
                <h2 class="h1">Construction intelligence</h2>
                <p>thinkproject is a collective of industry leading products and professionals with one clear goal: To develop and deliver world-class products to support, connect and advance the industry and the people in it.</p>
                <a href="#" class="link link--right">
                    <span>About thinkproject!</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </a>
            </div>
            <div class="block--img-text__img  col-lg-6 paragraph-margin-md ">
                <div class="img-res">
                    <img src="https://via.placeholder.com/600x400.png/09f/fff">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="block--marquee section-pad-sm paragraph-margin-lg">
    
    <div class="container ta-c">
        <h2 class="h1">Our customers include:</h2>
    </div>
   
    <div class="block--marquee__wrapper container-fluid">
        <div class="block--marquee__inner">
            <div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
            </div>
            <div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
            </div>
        </div>
        <div class="block--marquee__inner" data-direction="reverse">
            <div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
            </div>
            <div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
                <div class="block--marquee__item">
                    <div class="img-res"><img src="https://via.placeholder.com/200x120.png/09f/fff"></div>
                </div>
            </div>
        </div>
    </div>

</section>

<section class="block--img-text-offset section-pad-md">
    <div class="container">

        <div class="row align-items-center">
            <div class="block--img-text-offset__text col-lg-4 offset-lg-1 paragraph-margin-md">
                <div class="paragraph-margin-none">
                    <p class="h3">Vamed Vitality World</p>
                    <h4 class="h2 text-color--pink">Thermal bath Laa</h4>
                </div>
                <p>Thermal bath Laa expansion, including the ‘Silent Spa’ completed construction whilst still in full operation. The Think Project Cross-Enterprise Collaboration Platform was used for document storage and distribution throughout the project.</p>
                <a href="#" class="link link--right">
                    <span>About this project</span>
                    <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                        <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                    </svg>
                </a>
            </div>
            <div class="block--img-text-offset__img col-lg-5 offset-lg-2">
                <div class="img-res"><img src="https://via.placeholder.com/400x300.png/09f/fff"></div>
            </div>
        </div>
    </div>
</section>

<section class="block--stats section-pad-sm bg-green">
    <div class="container paragraph-margin-md ta-c">
        <h3 class="h1">We’re proud to support:</h3>
        <div class="row align-items-center">
            <div class="block--stats--container col-lg-4">
                <div class="block--stats--number"><span data-val="150" class="animateVal">150</span>k</div>
                <div class="block--stats--text">users</div>
            </div>
            <div class="block--stats--container col-lg-4">
                <div class="block--stats--number"><span data-val="10" class="animateVal">10</span>k</div>
                <div class="block--stats--text">projects</div>
            </div>
            <div class="block--stats--container col-lg-4">
                <div class="block--stats--number"><span data-val="50" class="animateVal">50</span></div>
                <div class="block--stats--text">countries</div>
           </div>
        </div>
    </div>
</section>

<section class="block--newsfeed section-pad-md bg-white">
    <div class="container">
        <div class="row paragraph-margin-sm">
            
            <div class="col-12 paragraph-margin-sm">
                <h3 class="h1">Lastest news</h3>
            </div>

            <a href="#" class="block--newsfeed--item col-lg-3 col-md-6 paragraph-margin-sm">
                <div class="img-res block--newsfeed--img">
                    <img src="https://via.placeholder.com/400x250.png/09f/fff" alt="">
                </div>
                <p>Think Project continues double-digit growth in 2018.</p>
                <div>
                    <span href="#" class="link link--right">
                        <span>Read more</span>
                        <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                            <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                        </svg>
                    </span>
                </div>
            </a>

            <a href="#" class="block--newsfeed--item col-lg-3 col-md-6 paragraph-margin-sm">
                <div class="img-res block--newsfeed--img">
                    <img src="https://via.placeholder.com/400x250.png/09f/fff" alt="">
                </div>
                <p>Think Project continues double-digit growth in 2018.</p>
                <div>
                    <span href="#" class="link link--right">
                        <span>Read more</span>
                        <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                            <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                        </svg>
                    </span>
                </div>
            </a>
            
            <a href="#" class="block--newsfeed--item col-lg-3 col-md-6 paragraph-margin-sm">
                <div class="img-res block--newsfeed--img">
                    <img src="https://via.placeholder.com/400x250.png/09f/fff" alt="">
                </div>
                <p>Think Project continues double-digit growth in 2018.</p>
                <div>
                    <span href="#" class="link link--right">
                        <span>Read more</span>
                        <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                            <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                        </svg>
                    </span>
                </div>
            </a>

            <a href="#" class="block--newsfeed--item col-lg-3 col-md-6 paragraph-margin-sm">
                <div class="img-res block--newsfeed--img">
                    <img src="https://via.placeholder.com/400x250.png/09f/fff" alt="">
                </div>
                <p>Think Project continues double-digit growth in 2018.</p>
                <div>
                    <span href="#" class="link link--right">
                        <span>Read more</span>
                        <svg class="icon--arrow-r icon--svg" viewBox="0 0 30 18">
                            <use xlink:href="/assets/svg/svg-defs.svg#icon--arrow-r"></use>
                        </svg>
                    </span>
                </div>
            </a>
        </div>
    </div>
</section>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/footer.php'; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/html-build/inc/foot.php'; ?>