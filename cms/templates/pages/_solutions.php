<?php
    include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php');
    include($vars['inc'].'/top.php');
?>

<?php
    PerchSystem::set_var('navTitle', $meta['nav']);
    perch_content_create('Hero - Solutions', ['template' => '/hero/img-text.html',]);
    perch_content_create('Sub Hero - Solutions', ['template' => '/hero/page-grid.html',]);
    perch_content_create('Content', ['template' => 'content.html',]);
?>

<?php
    perch_content_custom('Hero - Solutions');
    perch_content_custom('Sub Hero - Solutions');
    perch_content_custom('Content');
?>

<?php
  include($vars['inc'].'/bottom.php');
?>