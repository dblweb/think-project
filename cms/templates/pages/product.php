<?php
    include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php'); 
    include($vars['inc'].'/top.php');
?>

<?php
    // template vars
    $bg_color = 'bg-orange';
    $crumbs = perch_pages_breadcrumbs(array(), true);
    
    PerchSystem::set_vars([
        'bgClass' => $bg_color,
        'crumbs'=> $crumbs,
        'navTitle' => $meta['nav'],
    ]);

    perch_content_create('Hero', ['template' => '/hero/text-col.html',]);
    perch_content_create('Content', ['template' => 'content.html',]);
    perch_content_create('Related Page Carousel', ['template' => 'page-carousel.html',]);
?>

<?php
    perch_content_custom('Hero');
    perch_content_custom('Content');
    perch_content_custom('Related Page Carousel');
?>

<?php
    include($vars['inc'].'/components/footer-form.php');
    include($vars['inc'].'/bottom.php');
?>