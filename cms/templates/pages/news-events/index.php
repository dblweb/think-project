<?php
  include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php');
  
  include($vars['inc'].'/top.php');

  perch_content_create('Heading', ['template' => 'heading.html',]);
  perch_content_create('Contact Card', ['template' => '/contact-card.html',]);
  perch_content_create('Additional Form', ['template' => 'marketo/form.html',]);

?>



<section class="v-padding--md">
  <div class="container eq-margin--md">
      <div class="row">
        <div class="col-12 h3 breadcrubs--header">
            <?php echo $meta['nav']; ?>
        </div>
      </div>
      <div class="row align-items-center">
        <div class="col-lg-6">
        <?php perch_content('Heading'); ?>
        </div>
        <div class="col-xl-5 offset-xl-1 col-lg-6">
          <?php perch_content('Contact Card'); ?> 
        </div>
      </div>
      <div class="row subscribe-filter--row">
      <?php 
        // if cookie isset
        if(isset($_COOKIE['cookieConsent']) && $_COOKIE['cookieConsent']== "accepted" ) {
      ?>
        <div class="col-lg-4">
          <div class="expanding-wrapper">
            <input id="signup" type="checkbox" class="hidden" />
            <label for="signup">
              <div class="bg-pink padding h2 h2--sm"><?php get_translation('lang.news-events.subscribe-newsletter', []); ?></div>
              <div class="bg-pink padding ta-c">
                <svg class="icon--expand icon--svg" viewBox="0 0 12 12">
                  <use xlink:href="/assets/svg/svg-defs.svg#icon--drop"></use>
                </svg>
              </div>
            </label>
            <div class="bg-lpink">
              <div class="padding--sm"><?php perch_content('Additional Form'); ?></div>
            </div>
          </div>
        </div>
      <?php } ?>
        
        <div class="col-lg-8">
          <!-- blog filters will go here -->
          <?php
            $catSet = 'blog';
            $slug = perch_get($catSet);

            perch_categories([
              'data' => [
                'catParent' => 'blog',
                'hasSlug' => $slug,
                'setPath' =>  $catSet. '/' . $slug . '/',
              ],
              'template' => 'list.html',
              'set' => $catSet,
            ]);
            ?>
        </div>
      </div>
      <div class="row">

        <div class="col-12">

          <?php
            if ($slug) {
              perch_blog_custom([
                'data' => [
                  // pass vars to make paging work
                  'customCategory' => 'blog',
                  'customSlug' => $slug,
                ],
                'template' => 'blog-grid.html',
                'count' => 9,
                'sort' => 'postDateTime',
                'sort-order' => 'DESC',
                'blog' => $vars['lang'],
                'category' => 'blog/' . $slug . '/',
              ]);
            } else {
              perch_blog_custom([
                'template' => 'blog-grid.html',
                'count' => 9,
                'sort' => 'postDateTime',
                'sort-order' => 'DESC',
                'blog' => $vars['lang'],
              ]);
            }
          ?>
        </div>
      </div>
  </div>  
</section>

<?php
  include($vars['inc'].'/components/footer-form.php');
  include($vars['inc'].'/bottom.php');
?>