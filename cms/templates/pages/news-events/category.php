<?php
  include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php');
  
  if (perch_get('cat')) {
  	$slug = perch_get('cat');
  	$collection = utility::title(perch_get('cat'));
  } else {
  	// Redirect if no slug
  	utility::redirect('/journal/');
  }
  
  $data = perch_collection($collection, [
  	'data' => [
	    'cat'=>$slug,
  	],
	  'skip-template'=>true,
	  'return-html'=>true,
    'template'=>'collections/_list.html',
    // 'sort'=>'_date',
    'sort'=>'date',
    'sort-order'=>'DESC',
    'paginate'=>true,
    'count'=>12
  ], true);
  
  if (!isset($data[0])) {
    // Redirect if no results
    utility::redirect('/journal/');
  } else {
  	$html = [];
  }
  
  if (utility::count($data) && isset($data[0])) {
  
  	$html['collection'] = $data['html'];
  	$html['heading'] = $collection;
  	
  	// Prepare meta data
  	$title = 'Journal: ' . $collection;
  	
  	perch_page_attributes_extend([
  		'pageTitle'=>$title
  	]);
  	
  }
  
  include($vars['inc'].'/top.php');
?>

<main class="page" role="main">
	<section class="hero background-purple">
		<div class="wrapper wrapper--fixed">
			<header class="hero-header">
				<p class="typography-eyebrow">Journal</p>
				<h1 class="typography-headline"><?php echo $html['heading']; ?></h1>
			</header>
		</div>
	</section>
	
  <?php
    perch_layout('filter', $perch);

	  echo $html['collection'];

    perch_layout('subscribe', $perch);
    perch_layout('twitter');
    perch_layout('social');
  ?>
</main>

<?php
  include($vars['inc'].'/bottom.php');
?>