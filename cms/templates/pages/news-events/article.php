<?php
    include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php');
    
    // set vars for template
    $parent_page = perch_pages_parent_page(array(), true);
    $parent_page_raw = perch_pages_parent_page(['skip-template' => 'true']);
    $parent_path = $parent_page_raw['pagePath'];
    $parent_name = $parent_page_raw['pageNavText'];
    PerchSystem::set_vars([
        'parent_page'=> $parent_page,
        'parent_path' => $parent_path,
        'parent_name' => $parent_name,
    ]);

    if (perch_get('slug')) {
        $slug = perch_get('slug');
    }

    $article = perch_blog_custom([
        'skip-template' => 'true',
        'return-html' => true,
        'filter' => 'postSlug',
        'match' => 'eq',
        'value' => $slug,
    ], true);

    if (!isset($article[0])) {
        // Redirect if no results
        utility::redirect('/' . $vars['lang'] . '/');
    } else {
        $html = [];
    }

    if (utility::count($article) && isset($article[0])) {
        
        $html['article'] = $article['html'];

        // Prepare meta data
        $title = $article[0]['postTitle'];

        if (utility::valid($article[0]['excerpt'])) {
            $description = utility::characters($article[0]['excerpt'], 200, true, false, '...');
        } elseif (utility::valid($article[0]['text'])) {
            $description = utility::characters($article[0]['text'], 200, true, false, '...');
        } else {
            $description = '';
        }

        perch_page_attributes_extend([
            'pageTitle' => $title,
            'description' => $description,
        ]);
    }

    include($vars['inc'].'/top.php');
?>

<?php
    echo $html['article'];
?>

<?php
    include($vars['inc'].'/components/footer-form.php');
    include($vars['inc'].'/bottom.php');
?>