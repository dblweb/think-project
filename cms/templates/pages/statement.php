<?php
  include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php');
  include($vars['inc'].'/top.php');
  perch_content_create('Heading', ['template' => 'heading.html',]);
?>

<section class="v-padding--md">
   <div class="container">
      <div class="row eq-margin">
         <div class="col-12"><h2 class="h3"><?php echo $meta['nav'] ; ?></h2></div>
         <div class="col-12"><?php perch_content('Heading'); ?></div>
      </div>
   </div>
</section>

<?php 
   perch_content_create('Statement section', ['template' => 'text-section.html',]);
?>

<?php
  perch_content('Statement section');
?>

<?php
  include($vars['inc'].'/components/footer-form.php');
  include($vars['inc'].'/bottom.php');
?>