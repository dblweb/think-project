<?php
    include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php');
    include($vars['inc'].'/top.php');
?>

<?php
    // template vars
    $bg_color = 'bg-blue';
    $crumbs = perch_pages_breadcrumbs(array(), true);

    PerchSystem::set_vars([
        'bgClass' => $bg_color,
        'crumbs'=> $crumbs,
        'navTitle' => $meta['nav'],
    ]);
    perch_content_create('Hero', ['template' => '/hero/page-grid.html',]);
    perch_content_custom('Hero');
?>

<?php
    PerchSystem::set_vars([
        'statBG' => 'bg-green',
    ]);
    include($vars['inc'].'/components/stats.php');
    include($vars['inc'].'/components/footer-form.php');
    include($vars['inc'].'/bottom.php');
?>