<?php
    include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php');
    $legacy = true;
    include($vars['inc'].'/top.php');
    perch_content_create('Hero', ['template' => '/hero/border-col.html',]);

    // template vars
    $bg_color = 'bg-green';

    PerchSystem::set_vars([
        'bgClass' => $bg_color,
    ]);
    perch_content_custom('Hero');
?>

<?php
   include($vars['inc'].'/bottom.php');
?>