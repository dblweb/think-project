<?php
  include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php');
  
  // set vars for template
  $parent_page = perch_pages_parent_page(array(), true);
  $parent_path = perch_pages_parent_page(['skip-template' => 'true'], true);
  PerchSystem::set_vars([
    'parent_page'=> $parent_page,
    'parent_path' => $parent_path['pagePath'],
  ]);
  
  if (perch_get('slug')) {
  	$slug = perch_get('slug');
  }
  
  $project = perch_collection('Projects', [
    'skip-template'=>true,
    'return-html'=>true,
    // update here
    'template'=>'collections/project.html',
    'filter'=>'slug_' . $vars['lang'],
    'match'=>'eq',
    'value'=>$slug,
  ], true);
  
  if (!isset($project[0])) {
    // Redirect if no results
    utility::redirect('/' . $vars['lang'] . '/');
  } else {
  	$html = [];
  }
  
  if (utility::count($project) && isset($project[0])) {
    
    $html['project'] = $project['html'];

    // Prepare meta data
    
    $title = $project[0]['name_' . $vars['lang']];
    
    if (utility::valid($project[0]['excerpt_' . $vars['lang']])) {
      $description = utility::characters($project[0]['excerpt_' . $vars['lang']], 200, true, false, '...');
    } elseif (utility::valid($project[0]['text_' . $vars['lang']])) {
      $description = utility::characters($project[0]['text_' . $vars['lang']], 200, true, false, '...');
    } else {
      $description = '';
    }

    perch_page_attributes_extend([
      'pageTitle' => $title,
    	'description' => $description,
    ]);
  }
  
  include($vars['inc'].'/top.php');
?>

<?php
  echo $html['project'];
?>

<?php
  include($vars['inc'].'/components/footer-form.php');
  include($vars['inc'].'/bottom.php');
?>