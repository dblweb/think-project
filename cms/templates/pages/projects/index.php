<?php
  include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php');
  
  $filtered = false;
  $catPaths = [];

  if (perch_get('country-region')) {
    $filtered = true;
    $countryPath = 'country-region/' . perch_get('country-region');
    $countryCats = perch_categories(['set' => 'country-region','skip-template' => true,]);
    
    // -- set not cat paths for $catPaths array
    foreach ($countryCats as $cat) {
      $path = (strpos($cat['catPath'], $countryPath) === false ? '!' . $cat['catPath'] : $cat['catPath']);
      array_push($catPaths, $path);
    }
    // -- set not cat paths for $catPaths array
  }
  
  if (perch_get('industry')) {
    $filtered = true;
    $industryPath = 'industry/' . perch_get('industry');
    $industryCats = perch_categories(['set' => 'industry','skip-template' => true,]);
    
    // -- set not cat paths for $catPaths array
    foreach ($industryCats as $cat) {
      $path = (strpos($cat['catPath'], $industryPath) === false ? '!' . $cat['catPath'] : $cat['catPath']);
      array_push($catPaths, $path);
    }
    // -- set not cat paths for $catPaths array
  }
  
  if ($filtered == true) {
    $projects = perch_collection('Projects', [
      'skip-template'=> true,
      'return-html'=> true,
      'template' => 'collections/_project-list.html',
      'count' => 8,
      'paginate' => true,
      'category' => $catPaths,
    ], true);
    
    $html = '';
    
    // if (utility::count($projects) < 1) {
    if (utility::count($projects) && utility::valid($projects['html'])) {
      $html = $projects['html'];
    } else {
      $html = '<p>' . get_translation('lang.projects.no-projects', [], true) . '</p>';
    }
    
  } else {
  
    $html = perch_collection('Projects', [
      'template' => 'collections/_project-list.html',
      'count' => 8,
      'paginate' => true,
    ], true);
    
  }
    
  include($vars['inc'].'/top.php');

  perch_content_create('Heading', ['template' => 'heading.html',]);
?>

<section class="v-padding--md">
  <div class="container eq-margin--md">
    <div class="row">
      <div class="col-12 h3 breadcrubs--header">
       
        <?php perch_pages_breadcrumbs(); ?>
      </div>
    </div>
    <div class="row align-items-center">
      <div class="col-lg-6">
        <?php perch_content('Heading'); ?>
      </div>
      <div class="col-lg-6 ta-r">
        <?php
          // Output country
          perch_categories([
            'data' => [
              'filterTitle' => 'Country / Region',
              'filterPath' => 'country-region',
              'filterVal' => 'country-region/' . perch_get('country-region'). '/',
            ],
            'template' => 'select.html',
            'set' => 'country-region',
            // show top level - remove once more projects have been added
            'filter' => 'catParentID',
            'match' => 'eq',
            'value' => 0,
          ]);

          // Output country
          perch_categories([
            'data' => [
              'filterTitle' => 'Industry',
              'filterPath' => 'industry',
              'filterVal' => 'industry/' . perch_get('industry') . '/',
            ],
            'template' => 'select.html',
            'set' => 'industry',
          ]);
        ?>
      </div>
    </div>
      <?php
        perch_content_custom('Projects', [
          'page'=>'/news/index.php',
          'template' => 'collections/_project-list.html',
        ]);
      ?>
      <?php
        echo $html;
      ?>
  </div>
</section>

<?php
  include($vars['inc'].'/components/footer-form.php');
  include($vars['inc'].'/bottom.php');
?>