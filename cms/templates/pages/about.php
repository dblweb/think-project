<?php
    include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php'); 
    include($vars['inc'].'/top.php');
?>

<?php
    // template vars
    $bg_color = 'bg-blue';
    $crumbs = perch_pages_breadcrumbs(array(), true);
    
    PerchSystem::set_vars([
        'bgClass' => $bg_color,
        'crumbs'=> $crumbs,
        'navTitle' => $meta['nav'],
    ]);
    perch_content_create('Hero - About', ['template' => '/hero/img-text.html',]);
    perch_content_create('Content', ['template' => 'content.html',]);
?>

<?php
    perch_content_custom('Hero - About');
    perch_content_custom('Content');
?>

<section class="v-padding--md eq-margin--sm">
    <div class="container">
        <div class="row">
            <h3 class="h1 col-12"><?php get_translation('lang.about.our-team', []); ?></h3>
        </div>
    </div>
</section>

<?php
    function teamSection($slug) {
        $teamTitle = perch_category($slug, ['template' => 'title.html',], true); 
        perch_collection('Team', [
            'data' => [
                'teamTitle' => $teamTitle,
            ],
            'template' => '/collections/team-grid.html',
            'category' => $slug,
        ]);
    }

    teamSection('team-groups/cxo-team/');
    teamSection('team-groups/non-executive-board-members/');
?>

<?php
    PerchSystem::set_vars([
        'statBG' => 'bg-pink',
    ]);
    include($vars['inc'].'/components/stats.php');
    include($vars['inc'].'/components/footer-form.php');
    include($vars['inc'].'/bottom.php');
?>