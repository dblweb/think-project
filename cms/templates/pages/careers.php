<?php
    include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php'); 
    include($vars['inc'].'/top.php');
?>

<?php
    // template vars
    $bg_color = 'bg-green';
    $crumbs = perch_pages_breadcrumbs(array(), true);

    PerchSystem::set_vars([
        'bgClass' => $bg_color,
        'crumbs'=> $crumbs,
        'navTitle' => $meta['nav'],
    ]);
    perch_content_create('Hero', ['template' => '/hero/img-text.html',]);
    perch_content_create('Hero Career', ['template' => '/hero/img-text-alt.html',]);
    perch_content_create('Content', ['template' => 'content.html',]);
?>

<?php
    perch_content_custom('Hero');
    perch_content_custom('Hero Career');
    perch_content_custom('Content');
?>

<?php
  include($vars['inc'].'/bottom.php');
?>