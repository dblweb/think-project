<?php
    include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php');
    include($vars['inc'].'/top.php');
?>

<?php
    perch_content_create('Contact Intro', ['template' => '/heading-para.html',]);
    perch_content_create('Form scripts', ['template' => '/marketo/form.html',]);
?>

<section class="bg-lgrey v-padding--lg">
    <div class="container eq-margin--md">
        <div class="row">
            <div class="col-12 margin--none">
                <h1 class="h3"><?php echo $vars['page']; ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 eq-margin">
                <?php perch_content_custom('Contact Intro'); ?>
            </div>
            <div class="col-lg-8">
                <?php 
                    if(isset($_COOKIE['cookieConsent']) && $_COOKIE['cookieConsent']== "accepted" ) {
                        perch_content('Form scripts');
                    } else {
                        echo get_translation('lang.forms.update-cookie-setting', []);
                    }
                ?> 
            </div>
        </div>
    </div>        
</section>


       

<?php
  include($vars['inc'].'/bottom.php');
?>