<?php
  include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php');
    
  include($vars['inc'].'/top.php');
?>

<section class="hero hero--center-text section-pad-lg bg-blue">
   <div class="container">
      <div class="row ta-c">
         <div class="col-12 paragraph-margin-lg">
            <h1>Title goes here</h1>
            <p class="paragraph-lg">
               Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, et at consectetuer adipiscing elit, dolor sit rem ipsum dolor sit amet Lorem ipsum dolor sit amet, Lorem ipsum dolor, consectet.
            </p>
            <a href="#content" class="cta cta-arrow--down ta-c">Find out more<br /><i class="fa fa-long-arrow-down"></i></a>
         </div>
      </div>
   </div>
</section>

<?php
  include($vars['inc'].'/bottom.php');
?>