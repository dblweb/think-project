<?php
    include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php'); 
    include($vars['inc'].'/top.php');
?>

<section class="hero--border-col bg-green section-pad-lg section-fh">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="breadcrumbs h3">
                    <li>Error</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
            <div class="hero--border-col--2col-wrapper row align-items-center">
                <div class="error-page--code col-xl-5 col-lg-6">
                    <div class="error-number text-color--blue">404</div>
                </div>
                <div class="error-page--border"></div>
                    <div class="error-page--description col-xl-5 offset-xl-2 col-lg-6  paragraph-margin-sm ">
                        <div class="h1 h1--lg"><?php perch_content('Error description'); ?> Sorry, that page could not be found.</div>
                        <a href="<?php echo '/' . $vars['lang'] . '/'?>" class="btn btn--skew"><span>Back home</span></a>
                        <div class="h2">
            
                        </div>
                    </div>
                </div>
        </div>
    </div>
</section>
<?php
    include($vars['inc'].'/bottom.php');
?>