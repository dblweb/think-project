<?php
    include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php');
    include($vars['inc'].'/top.php');
    
    perch_content_create('Support Section', ['template' => 'support.html',]);

?>

<section class="v-padding--md">
    <div class="container eq-margin--md">
        <div class="row">
            <div class="col-12 h3 breadcrubs--header"><?php perch_pages_breadcrumbs(); ?></div>
        </div>
        <div class="row">
            <div class="col-12"><h1 class="h1 margin--none"><?php echo $meta['title']; ?></h1></div>
        </div>
        <hr />
    </div>
</section>

<?php perch_content_custom('Support Section'); ?>
           



<?php
    include($vars['inc'].'/components/footer-form.php');
    include($vars['inc'].'/bottom.php');
?>