<?php
  include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php'); 
  include($vars['inc'].'/top.php');
?>

<?php
  // template vars
  $bg_color = 'bg-blue';
  $crumbs = perch_pages_breadcrumbs(array(), true);

  PerchSystem::set_vars([
    'bgClass' => $bg_color,
    'crumbs'=> $crumbs,
    'navTitle' => $meta['nav'],
  ]);
  perch_content_create('Hero - Home', ['template' => '/hero/home.html',]);
  perch_content_create('Sub Hero - Shape', ['template' => '/hero/sub-shape.html',]);
  perch_content_create('Content', ['template' => 'content.html',]);
  perch_content_create('Content Secondary', ['template' => 'content.html',]);
?>

<?php 
  perch_content_custom('Hero - Home');
  perch_content_custom('Sub Hero - Shape');
  perch_content_custom('Content');
  include($vars['inc'].'/components/latest-blog.php');

  perch_collection('Projects', [
    'count' => 1,
    'sort'  => '_date', 
    'sort-order' => 'DESC',
    'template' => '/collections/project-latest.html',
  ]);
?>

<?php
  PerchSystem::set_vars([
    'statBG' => 'bg-green',
  ]);
  include($vars['inc'].'/components/stats.php');
  perch_content_custom('Content Secondary');
  include($vars['inc'].'/components/footer-form.php');
  
?>

<?php
  include($vars['inc'].'/bottom.php');
?>