<?php
    include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php');
    include($vars['inc'].'/top.php');
    perch_content_create('Heading', ['template' => 'heading.html',]);
?>

<section class="v-padding--md">
    <div class="container eq-margin--sm">
        <div class="row">
            <div class="col-12 h3 breadcrubs--header">
                <?php echo $meta['nav']; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12 ">
                <?php perch_content('Heading'); ?>
            </div>
        </div>
        <?php 
            perch_collection('Downloads', [
                'template' => 'collections/_downloads-list.html',
            ]);
        ?>
</section>

<?php
    include($vars['inc'].'/components/footer-form.php');
    include($vars['inc'].'/bottom.php');
?>