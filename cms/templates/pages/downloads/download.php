<?php
  include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php');


  // set vars for template
  $parent_page = perch_pages_parent_page(array(), true);
  $parent_path = perch_pages_parent_page(['skip-template' => 'true'], true);
  $crumbs = perch_pages_breadcrumbs(array(), true);
  $cookieAccepted = (isset($_COOKIE['cookieConsent']) && $_COOKIE['cookieConsent']== 'accepted' ? 'true' : 'false');

  PerchSystem::set_vars([
    'parent_page'=> $parent_page,
    'parent_path' => $parent_path['pagePath'],
    'crumbs'=> $crumbs,
    'cookieAccepted' => $cookieAccepted,
  ]);
 

  
  if (perch_get('slug')) {
  	$slug = perch_get('slug');
  }
  


  $download = perch_collection('Downloads', [
    'skip-template'=>true,
    'return-html'=>true,
    'template'=>'collections/download.html',
    'filter'=>'slug_' . $vars['lang'],
    'match'=>'eq',
    'value'=>$slug,
  ], true);
  
  if (!isset($download[0])) {
    // Redirect if no results
    utility::redirect('/' . $vars['lang'] . '/');
  } else {
  	$html = [];
  }
  
  if (utility::count($download) && isset($download[0])) {
    
    $html['download'] = $download['html'];

    // Prepare meta data
    
    $title = $download[0]['name_' . $vars['lang']];
    
    if (utility::valid($download[0]['text_' . $vars['lang']])) {
      $description = utility::characters($download[0]['text_' . $vars['lang']], 200, true, false, '...');
    } else {
      $description = '';
    }
    
    perch_page_attributes_extend([
    	'pageTitle'=>$title,
    	'description'=>$description,
    ]);
  }
  include($vars['inc'].'/top.php');
?>

<?php
  echo $html['download'];

?>

<?php
    include($vars['inc'].'/components/footer-form.php');
    include($vars['inc'].'/bottom.php');
?>