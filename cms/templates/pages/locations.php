<?php
   include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php');
   include($vars['inc'].'/top.php');

   $crumbs = perch_pages_breadcrumbs([], true);
   $location_items = perch_collection('Locations', [], true);

   PerchSystem::set_vars([
      'crumbs'=> $crumbs,
      'locations' => $location_items,
   ]);

   perch_content_create('Locations', ['template' => '/locations.html',]);
   perch_content_custom('Locations');
?>

<!----------------->
<!-- Google maps -->
<!----------------->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwKzM361wFiRQ8x-Qent34wdig6blC9QE&callback=initMap" async defer></script>
<!----------------->
<!-- Google maps -->
<!----------------->

<?php   
   $locationItems = perch_collection('Locations', ['skip-template' => true, ]);
   $locationArr = [];

   foreach($locationItems as $item) {
      if (array_key_exists('officeLocation', $item)) {
         array_push($locationArr, $item['officeLocation'][0]);
      }
   }

   $locationArr = array_unique($locationArr);
?>
<section class="location-grid v-padding">
   <div class="container">
      <ul class="grid-list row">
         <?php
            // output address with titles for each country
            foreach($locationArr as $path) {
               perch_collection('Locations', [
                  'category' => $path,
                  'template' => '/collections/location-group.html', 
               ]);
            }
         ?>
      </ul>
   </div>
</section>



<?php
   include($vars['inc'].'/components/footer-form.php');
   include($vars['inc'].'/bottom.php');
?>