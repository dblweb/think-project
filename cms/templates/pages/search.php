<?php
    include($_SERVER['DOCUMENT_ROOT'].'/framework/init.php');
    include($vars['inc'].'/top.php');
?>

<section class="v-padding--md">
    <div class="container eq-margin--md">
        <div class="row">
            <div class="col-12 h3 breadcrubs--header">
                <?php perch_pages_breadcrumbs(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12 ta-c">
                <?php perch_search_form(['template'=>'site-search-form.html']); ?>
            </div>
        </div>
       
            <?php 
                $query = perch_get('q');
                perch_content_search($query, array(
                    'data' => [
                        'lang_uppercase' => strtoupper($vars['lang']),
                    ],
                    'count' => 6,
                    'from-path' => '/' . $vars['lang'],
                    'excerpt-chars' => 300,
                    'template' => 'site-search-result.html',
                ));
            ?>
        
    </div>
</section>

<?php
  include($vars['inc'].'/components/footer-form.php');
?>

<?php
    include($vars['inc'].'/bottom.php');
?>