<?php
      
  switch($_SERVER['SERVER_NAME']) {
  
    case 'think-project':
    case 'localhost':
      include(__DIR__.'/config.localhost.php');
      break;
    
    case 'thinkproject.thirdfloordesign.net':
    	include('config.staging.php');
    	break;
  
    default:
      include('config.production.php');
  }
  
  
  // License
  define('PERCH_LICENSE_KEY', 'R31905-MCS743-EBV482-YWA335-XNJ017');    
    
    
  // Email
  define('PERCH_EMAIL_FROM', 'hello@thirdfloordesign.co.uk');
  define('PERCH_EMAIL_FROM_NAME', 'Third Floor');
  define('PERCH_EMAIL_METHOD', 'smtp');
  define('PERCH_EMAIL_HOST', 'smtp.postmarkapp.com');
  define('PERCH_EMAIL_AUTH', true);
  define('PERCH_EMAIL_SECURE', 'tsl');
  define('PERCH_EMAIL_PORT', 2525);
  define('PERCH_EMAIL_USERNAME', 'ad0643ad-86a5-488d-b55a-afdc7b2b76a1');
  define('PERCH_EMAIL_PASSWORD', 'ad0643ad-86a5-488d-b55a-afdc7b2b76a1');
    
    
  // Paths
  define('PERCH_LOGINPATH', '/cms');
  define('PERCH_PATH', str_replace(DIRECTORY_SEPARATOR.'config', '', __DIR__));
  define('PERCH_CORE', PERCH_PATH.DIRECTORY_SEPARATOR.'core');
  define('PERCH_RESFILEPATH', PERCH_PATH . DIRECTORY_SEPARATOR . 'assets');
  define('PERCH_RESPATH', PERCH_LOGINPATH . '/assets');
  // define('PERCH_DEFAULT_BUCKET', 'images');
    
    
  // Config
  define('PERCH_SCHEDULE_SECRET', '150ad1438ddf92c288387f34e28ba4d6671f5858');
  define('PERCH_TZ', 'Europe/London');
  define('PERCH_CUSTOM_EDITOR_CONFIGS', true);
  define('PERCH_TEMPLATE_FILTERS', true);
  define('PERCH_UNDO_BUFFER', 5);
  define('PERCH_SESSION_TIMEOUT_MINS', 30);
  define('PERCH_SITE_BEHIND_LOGIN', false);
  define('PERCH_TRANSLATION_LANG', 'en');
  
  // Custom
  define('PIPIT_UI_LOADER_FILEPATH', PERCH_PATH . '/addons/plugins/ui/config.php');