<?php
	
	// Database
	define('PERCH_DB_USERNAME', 'root');
	define('PERCH_DB_PASSWORD', 'root');
	define('PERCH_DB_SERVER', 	'localhost');
	define('PERCH_DB_DATABASE', 'thinkproject_dev');
	define('PERCH_DB_PREFIX', 	'perch_');
	
	
	// Path
	// define('PERCH_SITEPATH', '/Users/Marc/Dropbox (Boldr)/Work/Third Floor Design/Think Project/Build');
	
	
	// Config
	define('PERCH_PRODUCTION_MODE', PERCH_DEVELOPMENT);
	define('PERCH_SSL', false);
	define('PERCH_DEBUG', true);