Perch.UserConfig.redactor = (function() {
    var get = function(profile, config, field) {
        if (profile == 'basic') {
            return {
                buttons: ['html', 'bold', 'italic', 'underline', 'link'],
                plugins: ['properties'],
                animation: false
            };
        } else {
            return {
                buttons: [
                    'html',
                    'format',
                    'bold',
                    'italic',
                    'underline',
                    'lists',
                    'link'
                ],
                formatting: [
                    'p',
                    'blockquote',
                    'h1',
                    'h2',
                    'h3',
                    'h4',
                    'h5',
                    'h6'
                ],
                plugins: [
                    'alignment',
                    'table',
                    'perchassets',
                    'video',
                    'widget',
                    'properties',
                    'fullscreen'
                ],
                animation: false
            };
        }

        return config;
    };

    var load = function(cb) {
        if (typeof $R.plugins.properties == 'undefined') {
            $.when(
                $.getScript(
                    Perch.path +
                        '/addons/plugins/editors/redactor/alignment/alignment.min.js'
                ),
                $.getScript(
                    Perch.path +
                        '/addons/plugins/editors/redactor/fullscreen/fullscreen.min.js'
                ),
                $.getScript(
                    Perch.path +
                        '/addons/plugins/editors/redactor/properties/properties.min.js'
                ),
                $.getScript(
                    Perch.path +
                        '/addons/plugins/editors/redactor/table/table.min.js'
                ),
                $.getScript(
                    Perch.path +
                        '/addons/plugins/editors/redactor/video/video.min.js'
                ),
                $.getScript(
                    Perch.path +
                        '/addons/plugins/editors/redactor/widget/widget.min.js'
                )
                /*
                $.Deferred(function(deferred){
                    $(deferred.resolve);
                })
                */
            ).done(function() {
                cb();
            });
        } else {
            cb();
        }
    };

    return {
        'get': get,
        'load': load
    };
})();
