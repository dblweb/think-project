<?php

  return [
    'css' => [
      // '/cms/addons/plugins/ui/app.css'
    ],

    'js' => [
      // '/cms/addons/plugins/ui/app.js'
    ],

    'favicon' => '/assets/ico/favicon.ico',
  ];