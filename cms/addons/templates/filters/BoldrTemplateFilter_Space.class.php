<?php
  // Template filter to return value without space
	class BoldrTemplateFilter_space extends PerchTemplateFilter {
    public function filterAfterProcessing($value, $valueIsMarkup = false) {
    	if ($this->Tag->type == 'text') {
        return str_replace(' ', '', $value);
      }
    }
	}
	
	PerchSystem::register_template_filter('space', 'BoldrTemplateFilter_space');