<?php
  // Template filter to return reading length in minutes
  class BoldrTemplateFilter_timeSince extends PerchTemplateFilter {
    public function filterAfterProcessing($value, $valueIsMarkup = false) {
      $postDate = $value;
      $today = date('Y-m-d');
      $date1 = date_create($today);
      $date2 = date_create($postDate);
      $diff = date_diff($date1,$date2);
      $days = $diff->format("%a");
      $months = $diff->format("%m");
      if ($months > 0) {
          $posted = 'Posted ' . $months . ' months ago';
      } elseif ($days > 0) {
          $posted = 'Posted ' . $days . ' days ago';
      } else {
          $posted = 'Posted today';
      }
      return $posted;
    }
  }
  
  PerchSystem::register_template_filter('timesince', 'BoldrTemplateFilter_timeSince');