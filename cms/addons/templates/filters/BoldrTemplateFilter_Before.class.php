<?php
  // Template filter to return value before '.'
	class BoldrTemplateFilter_before extends PerchTemplateFilter {
    public function filterAfterProcessing($value, $valueIsMarkup = false) {
    	if ($this->Tag->type == 'text') {
    		$number = explode('.', $value);
        return $number[0];
      }
    }
	}
	
	PerchSystem::register_template_filter('before', 'BoldrTemplateFilter_before');