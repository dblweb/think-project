<?php
  // Template filter to return value after '.'
	class BoldrTemplateFilter_after extends PerchTemplateFilter {
    public function filterAfterProcessing($value, $valueIsMarkup = false) {
    	if ($this->Tag->type == 'text') {
        $number = explode('.', $value);
        return $number[1];
      }
    }
	}
	
	PerchSystem::register_template_filter('after', 'BoldrTemplateFilter_after');