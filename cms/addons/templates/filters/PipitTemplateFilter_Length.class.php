<?php
  // Template filter to return reading length in minutes
  class PipitTemplateFilter_length extends PerchTemplateFilter {
      public function filterBeforeProcessing($value, $valueIsMarkup = false)
      {
        $word = str_word_count(strip_tags($value));
        $m = floor($word / 200);
        $s = floor($word % 200 / (200 / 60));
  
        if ($m > 0) {
          $est = $m . ' minute' . ($m == 1 ? '' : 's');
        } else {
          $est = 1 . ' minute';
        }
  
        return $est;
      }
  }
  
  PerchSystem::register_template_filter('length', 'PipitTemplateFilter_length');