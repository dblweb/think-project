<?php
    
  spl_autoload_register(function($class) {
    if (strpos($class, 'BoldrShortcode')===0) {
      include(PERCH_PATH.'/addons/apps/boldr_shortcode/lib/'.$class.'.class.php');
      return true;
    }
    return false;
  });
  
  // register shortcodes
  PerchSystem::register_shortcode_provider('BoldrShortcode_Codepen');
  PerchSystem::register_shortcode_provider('BoldrShortcode_Instagram');
  PerchSystem::register_shortcode_provider('BoldrShortcode_Soundcloud');
  PerchSystem::register_shortcode_provider('BoldrShortcode_Speakerdeck');
  PerchSystem::register_shortcode_provider('BoldrShortcode_Twitter');
  PerchSystem::register_shortcode_provider('BoldrShortcode_Vimeo');
  PerchSystem::register_shortcode_provider('BoldrShortcode_Youtube');