<?php

class BoldrShortcode_Instagram extends PerchShortcode_Provider {
    public $shortcodes = ['instagram'];

    public function get_shortcode_replacement($Sortcode, $Tag) {
        $id = $Sortcode->arg(0);

        $API = new PerchAPI(1.0, 'boldr_instagram');
        $HTTP = $API->get('HTTPClient');

        $response = $HTTP->get('https://api.instagram.com/oembed/?url='.urlencode('https://instagr.am/p/'.$id));

        if ($response) {            
            $data = json_decode($response, true);
            if (isset($data['html'])) return $data['html'];
        }

        return '';
    }
}

/*
    Emample: https://api.instagram.com/oembed/?url=http://instagr.am/p/BGS8uwxFR-n
*/