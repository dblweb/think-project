<?php

class BoldrShortcode_Vimeo extends PerchShortcode_Provider {
    public $shortcodes = ['vimeo'];

    public function get_shortcode_replacement($Sortcode, $Tag) {
        $id = $Sortcode->arg(0);
        
        $args = $Sortcode->get_args();
        
        array_shift($args);
        
        $query = '';
        
        if (!empty($args)) {
            $query = '?'.http_build_query($args);
        }

        $API = new PerchAPI(1.0, 'boldr_vimeo');
        $HTTP = $API->get('HTTPClient');
        
        $response = $HTTP->get('https://vimeo.com/api/oembed.json?url='.urlencode('https://vimeo.com/'.$id.$query));

        if ($response) {            
            $data = json_decode($response, true);
            if (isset($data['html'])) return $data['html'];
        }

        return '';
    }
}

/*
    Example: https://vimeo.com/api/oembed.json?url=https://vimeo.com/191465923?width=1920
    
    Arguments:
    
    url             The Vimeo URL for a video. (required)
    api             Enable the JavaScript API. Defaults to false.
    autopause	    Pause this video automatically when another one plays. Defaults to true.
    autoplay        	Automatically start playback of the video. Defaults to false.
    byline	        Show the byline on the video. Defaults to true.
    callback	        When returning JSON, wrap in this function.
    color	        Specify the color of the video controls.
    height	        The exact height of the video. Defaults to original size.
    loop	            Play the video again automatically when it reaches the end. Defaults to false.
    maxheight	    Same as height, but video will not exceed original size.
    maxwidth	    Same as width, but video will not exceed original size.
    player_id	    A unique id for the player that will be passed back with all JavaScript API responses.
    portrait	    Show the user’s portrait on the video. Defaults to true.
    title	        Show the title on the video. Defaults to true.
    width	        The exact width of the video. Defaults to original size.
    xhtml	        Make the embed code XHTML compliant. Defaults to false.
*/