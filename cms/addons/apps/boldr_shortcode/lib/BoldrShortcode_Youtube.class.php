<?php

class BoldrShortcode_Youtube extends PerchShortcode_Provider {
    public $shortcodes = ['youtube'];

    public function get_shortcode_replacement($Sortcode, $Tag) {
        $id = $Sortcode->arg(0);

        $API = new PerchAPI(1.0, 'boldr_youtube');
        $HTTP = $API->get('HTTPClient');

        $response = $HTTP->get('https://www.youtube.com/oembed?url='.urlencode('https://youtu.be/'.$id));

        if ($response) {            
            $data = json_decode($response, true);
            if (isset($data['html'])) return $data['html'];
        }

        return '';
    }
}

/*
    Example: https://www.youtube.com/oembed?url=https://youtu.be/uYrjWrodhpY?maxwidth=1920
    
    Arguments:

    url             the URL of the resource
    maxwidth        the maximum width. The thumbnail size is not affected. This value, if bigger than the usual video return, will increase the size of the video.
    maxheight       the maximum height. The thumbnail size is not affected. This value will only decrease the size of the video.
    format          json or xml
*/