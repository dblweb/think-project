<?php
  return [
    // Global
    'global' => [
      'phone' => 'Phone',
      'google-maps' => 'Google Maps',
    ],

    // Stats
    'stats' => [
      'heading' => 'We’re proud to support:',
      'users' => 'Users',
      'projects' => 'Projects',
      'countries' => 'Countries',
    ],
    
    // Header
    'header' => [
      'login' => 'Login',
      'select-language' => 'Select language',
      'search-our-website' => 'Search our website',
      // menu titles
      'solutions' => 'Solutions',
      'customer-service' => 'Customer service',
    ],

    // Home
    'home' => [
      'latest-news' => 'Latest News',
      'read-more' => 'Read more',
      'view-project' => 'View project',
      'find-out-more' => 'Find out more',
    ],

    // About 
    'about' => [
        'our-team' => 'Our Team',
    ],

    // Projects
    'projects' => [
      'url' => '/projects/',
      'view-project' => 'View project',
      'client' => 'Client',
      'related-projects' => 'Related projects',
      'country-region' => 'Country / Region',
      'industry' => 'Industry',
      'no-projects' => 'Sorry no projects under this category can be found, please select another.',
      'all' => 'All',
    ],

    // Downloads
    'downloads' => [
      'url' => '/downloads/',
      'brochure' => 'Brochure',
      'whitepaper' => 'Whitepaper',
      'webinar' => 'Webinar',
      'download' => 'Download',
      'manual' => 'Manual',
      'update-cookie-setting' => 'To access this item, please <a class="cookie-setting">enable cookies</a>.',
    ],

    // News & Events
    'news-events' => [
      'url' => '/news-events/',
      'subscribe-newsletter' => 'Subscribe to our newsletter',
      'all' => 'all',
      'slug' => 'category',
    ],

    // Search
    'search' => [
      'url' => '/search/',
      'home' => 'Home',
      'prev' => 'Prev',
      'next' => 'Next',
      'result' => [
        'none' => 'Sorry, no results for',
        'empty' => 'Sorry, please enter something to search for.',
      ],
      'paging' => [
        'page' => 'Page',
        'of' => 'of',
      ],
      'cta' => 'visit page'
    ], 

    // Footer (includes the footer form)
    'footer' => [
      'signup' => 'Contact us / Sign up', 
    ], 
    
    // Cookie consent
    'cookie-consent' => [
      'cookie-setting' => 'Cookie settings',
      'cookie-policy' => 'Cookie policy',
      'text' => 'Our website uses cookies and other technologies to make your website experience better and to help us understand how you use our website. Please click "accept" to continue or "decline" to deactivate non-functional Cookies. The full website experience cannot be ensured, when Cookies are deactivated due to technical reasons.',
      'data-protection-link' => 'Learn more about Cookies in our <a href="/en/data-protection-statement/">data protection statement</a>',
      'current-setting' => 'Your current cookie setting:',
      'decline' => 'Decline',
      'accept' => 'Accept',
    ],



    // Marketo Forms 
    'forms' => [
      'update-cookie-setting' => 'This form functionality is disabled please update your <a class="cookie-setting link">cookie settings</a> in order to enable the functionality.',
    ],
  ];

?>