<?php
    $statUser = get_translation('lang.stats.users', [], true);
    $statProject = get_translation('lang.stats.projects', [], true);
    $statCountries = get_translation('lang.stats.countries', [], true);

    perch_content_custom('Statistics', [
        'data' => [
            'statUser' => $statUser,
            'statProject' => $statProject,
            'statCountries' => $statCountries ,
        ],
    ]); 
?>