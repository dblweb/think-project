<footer class="bg-dgrey section-pad-sm">
  <div class="container paragraph-margin-md">
    <div class="row">

      <div class="footer--menu-col col-xl-3 col-md-3 d-none d-lg-block paragraph-margin-sm">
        <?php
         $navTitle = get_translation('lang.header.solutions', [], true);
          perch_pages_navigation([
            'data' => [
              'customNavHeading' => false,
            ],
            'navgroup' => $vars['lang'] . '-footer-1',
            'add-trailing-slash' => true,
            'template' => ['header/level-1.v2.html', 'header/level-2.v2.html'],
          ]);
        ?>
      </div>
      
      <div class="footer--menu-col col-xl-3 col-md-3 d-none d-lg-block paragraph-margin-sm">
        <?php
          perch_pages_navigation([
            'data' => [
              'customNavHeading' => false,
            ],
            'navgroup' => $vars['lang'] . '-footer-2',
            'add-trailing-slash' => true,
            'template' => ['header/level-1.v2.html', 'header/level-2.v2.html'],
          ]);
        ?>
      </div>
      
      <div class="footer--menu-col col-xl-3 col-md-3 d-none d-lg-block paragraph-margin-sm">
        <?php
          perch_pages_navigation([
            'data' => [
              'customNavHeading' => false,
            ],
            'navgroup' => $vars['lang'] . '-footer-3',
            'add-trailing-slash' => true,
            'template' => ['header/level-1.v2.html', 'header/level-2.v2.html'],
          ]);
        ?>        
      </div>

      <div class="footer--contact-col col-xl-3 col-md-3 d-none d-lg-block paragraph-margin-sm">
         <?php if (isset($lang_location)) echo $lang_location; ?>
      </div>

      <div class="footer--links-col col-6 col-lg-12">
        <?php
          PerchSystem::set_var('cookieText', get_translation('lang.cookie-consent.cookie-setting', [], true));
          perch_pages_navigation([
            'levels' => 1,
            'navgroup' => $vars['lang'] . '-footer-misc',
            'add-trailing-slash' => true,
            'template' => 'footer/misc.html',
          ]);
        ?>
      </div>
      
      <div class="footer-copyright-col col-12">
        <hr>
        <strong class="h4">&copy; <?php echo date('Y') . ' / ' . $meta['site']; ?></strong>
      </div>
   </div>
 </div>
</footer>

<!-- close overflow wrapper -->
</div>
<!-- close overflow wrapper -->