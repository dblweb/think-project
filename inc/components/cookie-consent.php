
<div id="cookieConsent" class="container bg-white padding--md eq-margin--sm mfp-hide">
  <h4 class="h1"><?php get_translation('lang.cookie-consent.cookie-setting', []); ?></h4>
  <?php if (!isset($_COOKIE['cookieConsent'])) { ?>
    <hr />
  <?php } ?>
  <p><?php get_translation('lang.cookie-consent.text', []); ?></p>
  <p><?php get_translation('lang.cookie-consent.data-protection-link', []); ?></p>
  <?php if (isset($_COOKIE['cookieConsent'])) { ?>
    <hr /><div>
    <strong class="d-block"><?php get_translation('lang.cookie-consent.current-setting', []); ?></strong>
    <?php if ($_COOKIE['cookieConsent'] == 'accepted') { ?>
      
        <strong class="d-block text-color--green">Cookies are active</strong>
    <?php } else { ?>
        <strong class="d-block text-color--pink">Non-functional cookies are disabled</strong>
    <?php } ?>
    </div><hr />
  <?php } ?>
  <div class="cookieConsent--cta ta-c">
    <button class="btn btn--skew" data-cookie="declined"><span><?php get_translation('lang.cookie-consent.decline', []); ?></span></button>
    <button class="btn btn--skew btn--green" data-cookie="accepted"><span><?php get_translation('lang.cookie-consent.accept', []); ?></span></button>
  </div>
</div>
