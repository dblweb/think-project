<!-- open overflow wrapper -->
<div class="overflow-wrapper">
<!-- open overflow wrapper -->

<header>
  <div class="container">
    <div class="row">
      <div class="col-4 logo--container">
        <a href="<?php echo '/' . utility::html($vars['lang']) . '/'; ?>" class="logo">
          <svg class="logo" viewBox="0 0 180 40">
            <use xlink:href="/assets/svg/svg-logo.svg#logo"></use>
          </svg>
        </a>
      </div>
    </div>
</header>