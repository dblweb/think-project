<footer class="<?php echo $bg_color ?> section-pad-sm">
    <div class="container paragraph-margin-md">
        <div class="row">
            <div class="footer--links-col col-6 col-lg-12">
                <?php
                PerchSystem::set_var('cookieText', get_translation('lang.cookie-consent.cookie-setting', [], true));
                perch_pages_navigation([
                    'levels' => 1,
                    'navgroup' => $vars['lang'] . '-footer-misc',
                    'add-trailing-slash' => true,
                    'template' => 'footer/misc.html',
                ]);
                ?>
            </div>

            <div class="footer-copyright-col col-12">
                <hr>
                <strong class="h4">&copy; <?php echo date('Y') . ' / ' . $meta['site']; ?></strong>
            </div>
        </div>
    </div>
</footer>