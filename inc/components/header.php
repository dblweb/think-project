<!-- open overflow wrapper -->
<div class="overflow-wrapper">
<!-- open overflow wrapper -->


<header>
  <div class="container">
    <div class="row align-items-center">
      <div class="col-5 logo--container">
        <a href="<?php echo '/' . utility::html($vars['lang']) . '/'; ?>" class="logo">
        <svg role="img" class="logo" viewBox="0 0 180 40">
	        <use xlink:href="/assets/svg/svg-logo.svg#logo"/>
        </svg>
        </a>
      </div>
      
      <div class="col-7 menu--container">
        <button class="btn--language menu-item menu--label d-none d-lg-inline-block d-md-inline-block" data-menu="language">
          <?php echo strtoupper($vars['lang']); ?>
          <svg role="img" class="icon--drop" viewBox="0 0 12 12">
	          <use xlink:href="/assets/svg/svg-defs.svg#icon--drop"/>
          </svg>
        </button>
        <a href="<?php echo 'https://welcome.thinkproject.com/' . $vars['lang'] . '/portal/'; ?>" class="menu-item btn btn--skew"><span>Login</span></a>
        <button class="btn--search menu-item menu--label" data-menu="search" >
          <svg role="img" class="icon--search" viewBox="0 0 24 24">
	          <use xlink:href="/assets/svg/svg-defs.svg#icon--search" />
          </svg>
        </button>
        
        <button class="btn--menu menu-item menu--label" data-menu="menu">
          <div class="menu--hamburger">
            <span></span>
            <span></span>
          </div>
        </button>

        <!-- drop down menus -->
        <div class="drop-down bg-white" id="language">
          <div class="container eq-margin--sm ta-c ">
            <h4 class="h2"><?php get_translation('lang.header.select-language', []); ?>:</h4>
            
            <div class="row flag-wrapper">

              <!-- print for all lang in cms -->
              <?php
                perch_pages_navigation([
                  'levels' => 1,
                  'navgroup' => 'lang',
                  'add-trailing-slash' => true,
                  'template' => 'header/lang.html',
                ]);
              ?>
              <!-- print for all lang in cms -->
            </div>
          </div>
        </div>
        
        <div class="drop-down bg-white" id="search">
          <div class="container ta-c eq-margin--sm">
            
            <h4 class="ta-c h2"><?php get_translation('lang.header.search-our-website', []); ?>:</h4>
            <?php perch_search_form(['template'=>'site-search-form.html']); ?>
          </div>
        </div>
        
        <div class="drop-down bg-white" id="menu">
          <div class="container">
            
            <div class="row ta-l">
              <div class="menu-col col-lg-3 col-md-4 paragraph-margin-md">
                <?php
                  $navTitle = get_translation('lang.header.solutions', [], true);
                  perch_pages_navigation([
                    'data' => [
                      'customNavHeading' => false,
                    ],
                    'navgroup' => $vars['lang'] . '-header-1',
                    'add-trailing-slash' => true,
                    'template' => ['header/level-1.v2.html', 'header/level-2.v2.html'],
                  ]);
                ?>
              </div>
              
              <div class="menu-col col-lg-3 col-md-4 paragraph-margin-md">
                <?php
                  perch_pages_navigation([
                    'data' => [
                      'customNavHeading' => false,
                    ],
                    'navgroup' => $vars['lang'] . '-header-2',
                    'add-trailing-slash' => true,
                    'template' => ['header/level-1.v2.html', 'header/level-2.v2.html'],
                  ]);
                ?>
              </div>
              
              <div class="menu-col col-lg-3 col-md-4 paragraph-margin-md">
                <?php
                  $navTitle = get_translation('lang.header.customer-service', [], true);
                  perch_pages_navigation([
                    'data' => [
                      'customNavHeading' => false,
                    ],
                    'navgroup' => $vars['lang'] . '-header-3',
                    'add-trailing-slash' => true,
                    'template' => ['header/level-1.v2.html', 'header/level-2.v2.html'],
                  ]);
                ?>
              </div>
              
              <div class="menu-col col-lg-3 d-none d-lg-block">
                <?php if (isset($lang_location)) echo $lang_location; ?>
              </div>
              
              <div class="menu--lang-sm d-md-none d-lg-none container">
                <hr>
                <div class="menu--lang-sm--wrapper d-md-none d-lg-none row">
                    <?php
                      perch_pages_navigation([
                        'levels' => 1,
                        'navgroup' => 'lang',
                        'add-trailing-slash' => true,
                        'template' => 'header/lang-basic.html',
                      ]);
                    ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 menu--hr"><hr /></div>
    </div>
  </div>
</header>