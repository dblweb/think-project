<?php

  $blogSecTitle = get_translation('lang.home.latest-news', [], true);
  $moreCta = get_translation('lang.home.read-more', [], true);

  perch_blog_custom([
    'template' => 'latest.html',
    'count'      => 4,
    'sort'       => 'postDateTime',
    'sort-order' => 'DESC',
    'blog'       => $vars['lang'],
    'data' => [
      'blogSecTitle' => $blogSecTitle,
      'moreCta' => $moreCta,
    ],
  ]);
?>