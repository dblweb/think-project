<section class="bg-grey section-pad-md">
  <div class="container">
    <div class="row paragraph-margin-sm">
      <div class="col-12 ta-c paragraph-margin-sm">
        <h2 class="h1">Contact us / Sign up:</h2>
        <p>Stay up-to-date with our news and events and receive insights into industry trends by filling in the form below to subscribe to our newsletter. Once completed, you will receive a confirmation email to activate your newsletter subscription. You can unsubscribe at any time.</p>
      </div>
      <form class="col-12">
        

          <div class="row">
            <div class="col-lg-4">
              <input class="form-element" type="text" placeholder="First name">
            </div>
            <div class="col-lg-4">
              <input class="form-element" type="text" placeholder="Last name">
            </div>
            <div class="col-lg-4">
              <input class="form-element" type="text" placeholder="Country">
            </div>
          </div>

          <div class="row">
            <div class="col-lg-4">
              <input class="form-element" type="text" placeholder="Email address">
            </div>
            <div class="col-lg-4">
              <input class="form-element" type="text" placeholder="Phone Number">
            </div>
            <div class="col-lg-4">
              <input class="form-element" type="text" placeholder="Company">
            </div>
          </div>

          <div class="row">
            <div class="col-12">
              <textarea class="form-element" rows="1" placeholder="Message"></textarea>
            </div>
          </div>

          <div class="row align-items-center">
            <div class="col-lg-5">   
              <div class="form--checkbox">
                <input type="checkbox" id="checkbox1" class="hidden">
                <label for="checkbox1">
                <span></span>
                  I have read the data protection statement and accept it.
                </label>
              </div>
            </div>
            <div class="col-lg-5">
              <div class="form--checkbox">
                <input type="checkbox" id="checkbox2" class="hidden">
                <label for="checkbox2">
                <span></span>
                I would like to receive more information about think project!
                </label>
              </div>  
            </div>
            <div class="col-lg-2 ta-r"><button class="menu-item btn btn--skew btn--pink"><span>Submit</span></button></div>
          </div>        
      </form>
  </div> 
</div>
  </div>
</section>