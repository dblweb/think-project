<?php
  // something in this function outputs <?php
  // Retrieve contact details
  $lang_attributes = perch_pages_navigation([
    'from-path' => '/' . $vars['lang'] . '/',
    'levels' => 1,
    'navgroup' => 'lang',
    'include-parent' => true,
    'add-trailing-slash' => true,
    'skip-template' => true,
  ], true);
  
  $lang_attributes = $lang_attributes[0];
      
  if (isset($lang_attributes['location']) || array_key_exists('location', $lang_attributes)) {
    $lang_location_id = $lang_attributes['location'][0];
    
    /* -- if commenting the below out this removes the rouge tag */
    $lang_location = perch_collection('Locations', [
      'template' => 'collections/_location.html',
      'filter' => '_id',
      'match' => 'eq',
      'value' => $lang_location_id,
    ], true);
    /* -- if commenting the above out this removes the rouge tag */
  }