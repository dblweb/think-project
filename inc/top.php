<?php
  include($vars['inc'].'/pre.php');
  include($vars['inc'].'/functions.php');
  include($vars['inc'].'/head.php');
?>
<body class="<?php echo $vars['class']; ?>">
  
<?php
  // used for legacy pages
  // remove header if $legacy var is set
  if (!isset($legacy))  {  
    include($vars['inc'].'/components/header.php');
  } else {
    include($vars['inc'].'/components/header-legacy.php');
  }
?>