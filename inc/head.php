<!doctype html>
<html lang="<?php echo $vars['lang']; ?>" class="no-js">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <title><?php echo $meta['title']; ?></title>
<?php if (utility::valid($meta['description'])) { ?>
  <meta name="description" content="<?php echo utility::characters($meta['description'], 150); ?>">
<?php } ?>
<?php if (utility::valid($meta['keywords'])) { ?>
  <meta name="keywords" content="<?php echo $meta['keywords']; ?>">
<?php } ?>
<?php if (utility::valid($meta['robots'])) { ?>
  <meta name="robots" content="noindex">
<?php } ?>
  <meta name="author" content="<?php echo $vars['author']; ?>">
  
  <link rel="dns-prefetch" href="//code.jquery.com">
  <link rel="dns-prefetch" href="//www.google.com">
  
  <!-- / alternate langs -->
  <link rel="alternate" hreflang="x-default" href="<?php echo $vars['domain']; ?>/en/">
  <?php
    foreach ($vars['langs'] as $lang) {
      // remove condition when all langs have been added to cms //
      // ** Don't forget to check for duplicate <link hreflang> tags ** // 
      if (!empty(utility::html($lang))) {
  ?>
  <link 
    rel="alternate" 
    hreflang="<?php echo utility::html($lang); ?>" 
    href="<?php echo $vars['domain'] . '/' . utility::html($lang) . '/'; ?>
  ">
  <?php
      }
    }
  ?>

    <!-- legacy site alternate langs -->
    <?php
      $i = 0;
      foreach ($vars['legacy']['paths'] as $key => $path) {
    ?>
      <link rel="alternate" hreflang="<?php echo $key; ?>" href="<?php echo $path; ?>"> 
    <?php
        $i++;
      }
    ?>
    <!-- legacy site alternate langs -->
  <!-- alternate langs / -->


  <link rel="canonical" href="<?php echo $vars['url']; ?>">
  
  <!-- css -->
  <link rel="stylesheet" href="<?php utility::asset('/build/combined.css', true); ?>">
    
  <!-- icons -->
  <link rel="icon" href="<?php utility::asset('/ico/favicon.png'); ?>">
  <link rel="shortcut icon" href="<?php utility::asset('/ico/favicon.ico'); ?>">
  <link rel="apple-touch-icon" href="<?php utility::asset('/ico/apple-touch-icon.png'); ?>">
  
  <!-- google tags -->
  <meta itemprop="name" content="<?php echo $meta['title']; ?>">
<?php if (utility::valid($meta['description'])) { ?>
  <meta itemprop="description" content="<?php echo utility::characters($meta['description'], 150); ?>">
<?php } ?>
<?php if (utility::valid($meta['image'])) { ?>
  <meta itemprop="image" content="<?php echo $vars['domain'] . $meta['image']; ?>">
<?php } ?>
  
  <!-- twitter tags -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:title" content="<?php echo $meta['title']; ?>">
<?php if (utility::valid($meta['description'])) { ?>
  <meta name="twitter:description" content="<?php echo $meta['description']; ?>">
<?php } ?>
<?php if (utility::valid($meta['image'])) { ?>
  <meta name="twitter:image:src" content="<?php echo $vars['domain'] . $meta['image']; ?>">
<?php } ?>
  
  <!-- open graph tags -->
  <meta property="og:title" content="<?php echo $meta['title']; ?>">
  <meta property="og:type" content="website">
  <meta property="og:url" content="<?php echo $vars['url']; ?>">
<?php if (utility::valid($meta['image'])) { ?>
  <meta property="og:image" content="<?php echo $vars['domain'] . $meta['image']; ?>">
<?php } ?>
<?php if (utility::valid($meta['description'])) { ?>
  <meta property="og:description" content="<?php echo $meta['description']; ?>">
<?php } ?>
  <meta property="og:site_name" content="<?php echo $meta['site']; ?>">
  
  <!-- <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script> -->
  <?php utility::head(); ?>
  <!-- Website by Third Floor Design <hello [at] thirdfloordesign.co.uk> -->
</head>
