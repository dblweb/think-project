<?php
	// Perch
	if (PERCH) {
	
		global $settings;
	
	  // Create Perch settings
	  perch_content_create('Settings', [
		  'template' => 'settings.html',
		  'multiple' => false,
		  'edit-mode' => 'singlepage',
		  'shared' => true
	  ]);
	  
	  
	  
	  // Retrieve global settings and set meta vars
	  $settings = perch_content_custom('Settings', [
	    'skip-template' => true,
	  ], true);


	  
	  if (isset($settings[0])) {
	  
	    $settings = $settings[0];
	      
	    if (utility::valid($settings['site'])) $meta['site'] = utility::html($settings['site']);
	    if (utility::valid($settings['image'])) $meta['image'] = $settings['image'];
	  }
	  
	  // Retrieve page attributes and set vars
	  $page_attributes = perch_page_attributes([
	    'skip-template' => true,
	  ], true);
	  
	  if (isset($page_attributes)) {

	  	if (utility::valid($page_attributes['pageTitle'])) $meta['title'] = $page_attributes['pageTitle'];
	  	if (utility::valid($page_attributes['pageNavText'])) $meta['nav'] = $page_attributes['pageNavText'];
	  	
	  	// if (isset($page_attributes['description']) || array_key_exists('description', $page_attributes)) $meta['description'] = $page_attributes['description']['processed'];
	  	$meta['description'] = perch_page_attribute('description', [], true);
	  	
	  	if (isset($page_attributes['keywords']) || array_key_exists('keywords', $page_attributes)) $meta['keywords'] = $page_attributes['keywords'];
	  	// if (array_key_exists('image', $attributes)) $meta['image'] = $attributes['image'];
	  	if (isset($page_attributes['robots']) || array_key_exists('robots', $page_attributes)) $meta['robots'] = $page_attributes['robots'];
	  	
	  	if (PERCH_RUNWAY) {
	  	  if (utility::valid($page_attributes['pagePath'])) $vars['path'] = $page_attributes['pagePath'] . '/';
	  	}
	  	
	  	// Add page attributes to class
	  	if (utility::valid($page_attributes['pageTemplate'])) {
	  	  
	  	  // Page ID
	  	  $page_id = $page_attributes['pageID'];
	  	  
	  	  $vars['class'] .= ' page-id-' . $page_id;
	  	  
	  	  // Parent page ID
	  	  $parent_page_id = $page_attributes['pageParentID'];
	  	  
	  	  $vars['class'] .= ' parent-page-id-' . $parent_page_id;
	  	  
	  	  // Page template
	  	  $page_template = $page_attributes['pageTemplate'];
	  	  $page_template = utility::strip_file_extension($page_template);
	  	  $page_template = str_replace('/', '-', $page_template);
	  	  
		  $vars['class'] .= ' template-' . $page_template;
	  	}	  	
	  }
	  
	  
	  // Set Perch vars
	  PerchSystem::set_vars([
	    'class' => $vars['class'],
	    'site' => $meta['site'],
	    'title' => $meta['title'],
	    'nav' => $meta['nav'],
	    'description' => $meta['description'],
		'keywords' => $meta['keywords'],
	  ]);
	  
	  $perch = PerchSystem::get_vars();
	}


	
