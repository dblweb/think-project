  <?php
    // used for legacy pages
    // remove footer if $legacy var is set
    if (!isset($legacy))  {  
      include($vars['inc'].'/components/footer.php');
    } else {
      include($vars['inc'].'/components/footer-legacy.php');
    }
    
    include($vars['inc'].'/components/cookie-consent.php');
  ?>

  <script src="<?php utility::asset('/js/libs/modernizr-3.5.0.min.js'); ?>"></script>
  <script data-cfasync='false' src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="<?php utility::asset('/js/libs/jquery-3.4.1.min.js'); ?>"><\/script>')</script>

  <script src="<?php utility::asset('/build/combined.js', true); ?>"></script>
	
  <?php perch_get_javascript(); ?>
  
  <?php
    if (isset($_COOKIE['cookieConsent']) && $_COOKIE['cookieConsent'] == 'accepted') {
      utility::setting('cookie');
    }
    
    utility::footer();
  ?>

<?php if (isset($_COOKIE['cookieConsent']) && $_COOKIE['cookieConsent'] == 'accepted') { ?>
  <!-- marketo forms -->
  <link rel="stylesheet" href="https://lp.thinkproject.com/rs/907-THX-267/images/thinkproject-forms.min.css" />
  <script src="https://lp.thinkproject.com/js/forms2/js/forms2.min.js"></script>
  <script src="https://lp.thinkproject.com/rs/907-THX-267/images/thinkproject-forms.min.js"></script>
  <!-- marketo forms -->
<?php } ?>

</body>
</html>