<?php
	if (isset($_SERVER['HTTP_HOST'])) {
		$host = $_SERVER['HTTP_HOST'];
	} elseif (isset($_SERVER['SERVER_NAME'])) {
	  $host = $_SERVER['SERVER_NAME'];
	} else {
		$host = 'www.thinkproject.com';
	}
	  
	if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
	  $scheme = 'https://';
	} else {
	  $scheme = 'http://';
	}
	
	// Paths
  define('HOST', $host);
  define('SCHEME', $scheme);
  define('DOMAIN', SCHEME.HOST);
	define('INC', ROOT.DIRECTORY_SEPARATOR.'inc');
	define('ASSET', '/assets');
	
	
	// Settings
	define('SITE', 'Think Project');
	define('DESCRIPTION', 'Multi-lingual website');
	define('AUTHOR', 'Third Floor Design <hello [at] thirdfloordesign.co.uk>');
	define('VERSION', '1.0');
	
	
	// Config
	define('SALT', '1b029ad5a69f6813cc66c4668cc84fdcecc0aa4e28eea695f602b25804a92fa8');
	define('SECRET', 'c9a675849118bb38c415e16cef75cb83c2dba28acb1a0eb242155bf92580f614');
	define('TIMEZONE', 'Europe/London');
	define('LANGUAGE', 'en');
	
	
	switch($_SERVER['SERVER_NAME']) {
		
    case 'think-project':
    case 'localhost':
      include(__DIR__.'/config.localhost.php');
      break;
      
    case 'thinkproject.thirdfloordesign.net':
    	include('config.staging.php');
    	break;

    default:
      include('config.production.php');
  }