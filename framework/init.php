<?php
    
  define('ROOT', realpath(dirname(__FILE__).'/../'));
  define('PATH', ROOT.DIRECTORY_SEPARATOR.'framework');
  define('CORE', PATH.DIRECTORY_SEPARATOR.'core');
  
  define('PERCH', true);
  
  // Perch
  if (PERCH) {
    if (!PERCH_RUNWAY) include(__DIR__.'/../cms/runtime.php');

    // global $Perch;
  }
  
  
  // Loader
  include(PATH.'/config/config.php');
  include(CORE.'/utility.php');
  include(CORE.'/pre.php');
  
  // Language
  include(CORE.'/lang.php');
  
  
  // Set error reporting
  if (MODE === 'development') {
  	ini_set('display_errors', true);
  	error_reporting(-1);
  } else {
  	error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
  }