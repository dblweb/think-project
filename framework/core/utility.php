<?php
    
  class utility {
  
    // Get current scheme
    
    public static function get_scheme() {
      
      $scheme = 'http://';
      
      if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
        $scheme = 'https://';
      }
      
      /*
      // Additional ssl check
      elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
          $scheme = 'https://';
      }
      */
      
      return $scheme;
    }
        
    // Get current url
    public static function get_url() {
        
      $url = utility::get_scheme();
        
      if ($_SERVER['SERVER_PORT'] != 80) {
        $url .= $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].strtok($_SERVER['REQUEST_URI'], '?');
      } else {
        $url .= $_SERVER['SERVER_NAME'].strtok($_SERVER['REQUEST_URI'], '?');
      }
        
      return $url;
    }
        
    // Get path of current path
    public static function get_path($query = false) {
      $path = $_SERVER['REQUEST_URI'];
      $url = parse_url($path);
        
      if ($query) {
        return $path;
      }
      
      return $url['path'];
    }
    
    // Get page of current url
    public static function get_page() {
      $page = basename(utility::get_path());
        
      if (utility::valid($page)) {
        return $page;
      } else {
        return 'home';
      }
    }
    
    // Create class for current url
    /*
    public static function get_class() {
      if (PERCH) {
        $path = PerchSystem::get_page();
      } else {
        $path = utility::get_path();
      }
      
      $parts = preg_split('#/#', $path, -1, PREG_SPLIT_NO_EMPTY);
      
      if (count($parts) == 0) {
        $class = 'page-home';
      } else {
        $class = utility::strip_file_extension(implode(' ', $tokens));
      }
      
      return $class;
    }
    */
    
    public static function get_class() {
      if (PERCH) {
        $path = PerchSystem::get_page();
        // $path = perch_page_attribute('pagePath', [], true);
      } else {
        $path = utility::get_path();
      }
      
      // $parts = preg_split('#/#', $path, -1, PREG_SPLIT_NO_EMPTY);
      $parts = array_slice(explode('/', $path), 1);
      
      if (utility::count($parts)) {
      	foreach ($parts as $part) {
      		if (!empty($part)) {
      	  	$class[] = 'page-' . utility::strip_file_extension($part);
      		} else {
      			$class[] = 'page-home';
      		}
      	}
      	
      	$class = implode(' ', $class);
      }

      return $class;
    }
        
    // Get current ip
    public static function get_ip() {
			if (array_key_exists('HTTP_CLIENT_IP', $_SERVER)) {
				$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
			} else if (array_key_exists('HTTP_X_REAL_IP', $_SERVER)) {
				$ipaddress = $_SERVER['HTTP_X_REAL_IP'];
			} else if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
				$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else if (array_key_exists('HTTP_X_FORWARDED', $_SERVER)) {
				$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
			} else if (array_key_exists('HTTP_FORWARDED_FOR', $_SERVER)) {
				$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
			} else if (array_key_exists('HTTP_FORWARDED', $_SERVER)) {
				$ipaddress = $_SERVER['HTTP_FORWARDED'];
			} else if (array_key_exists('REMOTE_ADDR', $_SERVER)) {
				$ipaddress = $_SERVER['REMOTE_ADDR'];
			} else {
				$ipaddress = 'Unknown';
			}

			return $ipaddress;
		}
		
		// Get mime type
    public static function get_mime_type($file) {
			if (!file_exists($file) || !is_readable($file)) {
				return false;
			}

			$mimetype = false;

			$use_finfo_class       = true;
			$use_finfo_function    = true;
			$use_getimagesize      = true;
			$use_mime_content_type = true;

			if ($use_finfo_class && class_exists('finfo')) {
				$finfo  = new finfo(FILEINFO_MIME, null);
				$result = $finfo->file($file);

				if ($result && strpos($result, ';')) {
					$parts    = explode(';', $result);
					$mimetype = $parts[0];
				}
			}
			if ($use_finfo_function && function_exists('finfo_open')) {
				try {
					$finfo  = finfo_open(FILEINFO_MIME, null);
					$result = finfo_file($finfo, $file);
					finfo_close($finfo);
				} catch (Exception $e) {
					// erm...
					$result = false;
				}

				if ($result && strpos($result, ';')) {
					$parts    = explode(';', $result);
					$mimetype = $parts[0];
				}
			}

			if ($mimetype == false && $use_getimagesize && function_exists('getimagesize')) {
				try {
					$result = @getimagesize($file);
					if (is_array($result)) {
						$mimetype = $result['mime'];
					}
				} catch (Exception $e) {
					$mimetype = false;
				}
			}

			if ($mimetype == false && $use_mime_content_type && function_exists('mime_content_type')) {
				$mimetype = mime_content_type($file);
			}

			if ($mimetype == false && !stristr(ini_get("disable_functions"), "shell_exec")) {
				$mimetype = shell_exec("file -bi " . escapeshellarg($file));
			}

			// SVG special case
			if ($mimetype == 'text/html' && utility::file_extension($file)=='svg') {
				$SVGdoc = simplexml_load_file($file);
				if ($SVGdoc !== false) {
					if (strtolower($SVGdoc->getName())=='svg') {
						$mimetype = 'image/svg+xml';
					}	
				}
				
			}

			return $mimetype;
		}
		
    // Return ssl/non-ssl url depending on flag
    public static function url_to_ssl($path) {
    	if (SSL) {
    		return utility::url_ssl($path);
    	} else {
    		return utility::url_non_ssl($path);
    	}
    }

    // Return ssl url
    public static function url_ssl($path) {
			if (strpos($path, 'https:') === 0) {
				return $path;
			}

			if (strpos($path, 'http:') === 0) {
				return str_replace('http:', 'https:', $path);
			}

			return 'https://' . $_SERVER['HTTP_HOST'] . $path;
		}

    // Return non-ssl url
		public static function url_non_ssl($path) {
			if (strpos($path, 'http:') === 0) {
				return $path;
			}

			if (strpos($path, 'https:') === 0) {
				return str_replace('https:', 'http:', $path);
			}

			return 'http://' . $_SERVER['HTTP_HOST'] . $path;
		}
		
		// Create a safely encoded url
		public static function urlify($str) {
		  $str = htmlspecialchars_decode($str);
		  return preg_replace(array('/[^a-zA-Z0-9 \t-]/', '/[ \t]/', '/-+/', '/^-/', '/-$/'), array('', '-', '-', '', ''), strtolower($str));                                                                                                                    
		}
		
		// Get browser info
		// browscap.ini must be available for this to work
		public static function browser_info() {
		    $info = get_browser(null, true);
		    $browser = $info['browser'] . ' ' . $info['version'];
		    $os = $info['platform'];
		    $ip = $_SERVER['REMOTE_ADDR'];
		    return array('ip' => $ip, 'browser' => $browser, 'os' => $os);
		}
        
    // Limit string to number of sentences
    public static function sentences($str, $count) {
      if (trim($str) == '') {
      	return $str;
      }
      
      preg_match('/^([^.!?]*[\.!?]+){0,'. $count .'}/', strip_tags($str), $excerpt);
      return $excerpt[0];
    }
        
    // Limit string to number of words
    public static function words($str, $words, $strip_tags = true, $balance_tags = false, $append = false) {
			$limit = $words;
			$str   = trim($str);
			if ($strip_tags) {
				$str = strip_tags($str);
			}
			$aStr   = explode(" ", $str);
			$newstr = '';

			if (utility::count($aStr) <= $limit) {
				return $str;
			}

			for ($i = 0; $i < $limit; $i++) {
				$newstr .= $aStr[$i] . " ";
			}

			$newstr = trim($newstr);

			if ($append != false) {
				$newstr .= $append;
			}

			if ($balance_tags) {
				return utility::balance_tags($newstr);
			}

			return $newstr;
		}

        
    // Limit string to number of characters
    public static function characters($str, $chars, $strip_tags = true, $balance_tags = false, $append = false) {
			utility::mb_fallback();

			$limit = $chars;

			$str = trim($str);
			if ($strip_tags) {
				$str = strip_tags($str);
			}

			if (mb_strlen($str) <= $limit) {
				return $str;
			}

			$str = mb_substr($str, 0, intval($limit));
			$last_space = mb_strrpos($str, ' ');
			if ($last_space > 0) {
				$str = mb_substr($str, 0, $last_space);
			}

			if ($append != false) {
				$str .= $append;
			}

			if ($balance_tags) {
				return utility::balance_tags($str);
			}

			return $str;
		}
        
    // Balance tags
    public static function balance_tags($str) {
			// Find broken tags
			$regexp = '/<[^>]*$/';
			preg_match($regexp, $str, $matches);
			if (utility::count($matches)) {
				// We have a broken tag
				$last_lt = strrpos($str, '<');
				if ($last_lt > 0) {
					$str = substr($str, 0, $last_lt);
				}
			}

			// Find opening tags
			$regexp = '/<([^\/]([a-zA-z]*))[^>]*>/';
			preg_match_all($regexp, $str, $matches);
			if (utility::count($matches)) {
				$opening_tags = $matches[1];
				$closing_tags = [];

				$regexp = '/<\/([a-zA-z]*)>/';
				preg_match_all($regexp, $str, $matches);
				if (utility::count($matches)) {
					$closing_tags = $matches[1];
				}

				// Find closing tags for openers
				$opening_tags = array_reverse($opening_tags);
				foreach ($opening_tags as $opening_tag) {
					if (isset($closing_tags[0])) {
						if ($closing_tags[0] != $opening_tag) {
							$str .= '</' . $opening_tag . '>';
						} else {
							array_shift($closing_tags);
						}
					} else {
						$str .= '</' . $opening_tag . '>';
					}
				}
			}

			return $str;
		}
        
    // Redirect user to url
    public static function redirect($url = null, $status = 302) {
      if (is_null($url)) {
        $url = $_SERVER['PHP_SELF'];
      }
      
      http_response_code($status);
      header('Location: ' . $url);
      exit;
    }
        
    // Set cookie
    public static function setcookie($name, $value = '', $expires = 0, $path = '', $domain = '', $secure = null, $http_only = true) {
            
			if ($secure === null) {
				if (SSL) {
					$secure = true;
				} else {
					$secure = false;
				}
			}

			header('Set-Cookie: ' . rawurlencode($name) . '=' . rawurlencode($value)
				   . (empty($expires) ? '' : '; expires=' . gmdate('D, d-M-Y H:i:s \\G\\M\\T', $expires))
				   . (empty($path) ? '' : '; path=' . $path)
				   . (empty($domain) ? '' : '; domain=' . $domain)
				   . (!$secure ? '' : '; secure')
				   . (!$http_only ? '' : '; HttpOnly'), false);
		}
        
    // Find needle in a haystack
    public static function find($haystack, $needle) {
      $needle = strtolower($needle);
    	$needles = array_map('trim', explode(",", $needle));
    	
    	foreach ($needles as $needle) {
    		if (strpos($haystack, $needle) !== false) {
    		  return true;
    		}
    	}
    	
    	return false;
    }
        
    // Hash string
    public static function hash($str = false) {
      if ($str == false) {
        if (function_exists('random_bytes')) {
          $hash = bin2hex(random_bytes(32));
        } else {
          $hash = bin2hex(openssl_random_pseudo_bytes(32));
        }
      } else {
        $hash = hash('sha256', $str);
      }
        
      return $hash;
    }
        
    // Create password
    public static function generate_password($count = 8) {
      $vals = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      $pwd = '';
        
      for ($i=0; $i<$count; $i++) {
        $pwd .= $vals[rand(0, strlen($vals)-1)];
      }
        
      return $pwd;
    }
        
    // Make password
    public static function make_password($value, $rounds = 12) {
      return password_hash($value, PASSWORD_BCRYPT, array('cost' => $rounds));
    }
    
    // Verify password
    public static function verify_password($value, $hash) {
      return password_verify($value, $hash);
    }
    
    // Change string to title case
    public static function title($str) {
      $str = ucfirst(str_replace('-', ' ', $str));
        
      return utility::html($str);
    }
        
    // Strip tags
  	public static function tagless($str, $removebreaks = false) {
      $str = preg_replace('@<(script|style)[^>]*?>.*?</\\1>@si', '', $str);
      $str = strip_tags($str);

      if ($removebreaks) {
        $str = preg_replace('/[\r\n\t ]+/', ' ', $str);
      }

      return trim($str);
    }
        
    // Output string as safe html
    public static function html($s, $quotes = false, $double_encode = false) {
			if ($quotes) {
				$q = ENT_QUOTES;
			} else {
				$q = ENT_NOQUOTES;
			}

			if ($quotes === -1) {
				$q = null;
			}

			if ((is_string($s) && strlen($s)) || is_numeric($s)) {
				return htmlspecialchars($s, $q, 'UTF-8', $double_encode);
			}

			return '';
		}
        
    // Fix eol
    public static function eol($str) {
      $str = str_replace("\r\n", "\n", $str);
      $str = str_replace("\r", "\n", $str);
      $str = str_replace("\n", "\n", $str);
      
      return $str;
    }
        
    // Find file extension
    public static function file_extension($file) {
    	if (strpos($file, '.') !== false) {
    	  return substr($file, strrpos($file, '.') + 1);
    	}
    	
    	return false;
    }
    
    // Strip file extension
    public static function strip_file_extension($file) {
      if (strpos($file, '.') === false) {
        return $file;
      }
      
      return substr($file, 0, strrpos($file, '.'));
    }
    
    // Strip file name
    public static function strip_file_name($path) {
      $parts = explode(DIRECTORY_SEPARATOR, $path);
      array_pop($parts);
        
      return utility::file_path(implode('/', $parts));
    }
    
    // Safe file path
    public static function file_path($path) {
      if (DIRECTORY_SEPARATOR != '/') {
        $path = str_replace('/', DIRECTORY_SEPARATOR, $path);
      }
        
      return $path;
    }
        
    // Filename ?
    public static function filename($filename, $include_crumb = true, $for_sorting = false) {
			$extensions = ['.html', '.htm', '.php'];
			$filename   = str_replace($extensions, '', $filename);

			$filename = ltrim($filename, '/');
			$filename = str_replace(['_', '-'], ' ', $filename);

			$parts = explode('/', $filename);
			foreach ($parts as &$part) {
				$part = ucfirst($part);
			}

			$filename = array_pop($parts);

			if (strtolower($filename) == 'index') {
				if (count($parts) == 0) {
					if ($for_sorting) {
						$filename = '/';
					} else {
						$filename = 'Home';
					}

				} else {
					$filename = array_pop($parts);
				}

			}

			if ($include_crumb) {
				$parts[]  = $filename;
				$filename = implode(' → ', $parts);
			}

			return $filename;
		}
		
		// In section
		public static function in_section($section_path, $page_path) {
			$parts = explode('/', $section_path);
			array_pop($parts);
			$section = implode('/', $parts);

			if ($section == '') {
				return false;
			}

			$section_parts = explode('/', $section_path);
			$page_parts    = explode('/', $page_path);


			for ($i = 0; $i < utility::count($section_parts); $i++) {
				if ($section_parts[$i] != $page_parts[$i]) {
					return $i - 1;
				}
			}

			return false;
		}
		
		// Decode json
		public static function json_decode($json, $assoc = false) {
			return json_decode($json, $assoc);
		}
        
    // Encode json
		public static function json_encode($arr, $tidy = false) {
			if ($tidy && defined('JSON_PRETTY_PRINT')) {
				return json_encode($arr, JSON_PRETTY_PRINT);
			}

			return json_encode($arr);
		}
        
    // Tidy json
		public static function tidy_json($json) {
			$json = str_replace('{', "{\n\t", $json);
			$json = str_replace('",', '",' . "\n\t", $json);
			$json = str_replace('}', "\n}", $json);

			return $json;
		}
        
    // Valid string
    public static function valid($str) {
      // if (isset($str) && trim($str) != '') {
      if (isset($str) && !empty($str)) {
        return true;
      }
        
      return false;
    }
        
    // Valid email
    public static function valid_email($email) {
			if (function_exists('filter_var')) {
				return filter_var($email, FILTER_VALIDATE_EMAIL);
			} else {

				if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
					// Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
					return false;
				}

				// Split it into sections to make life easier
				$email_array = explode("@", $email);
				$local_array = explode(".", $email_array[0]);
				for ($i = 0; $i < sizeof($local_array); $i++) {
					if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
						return false;
					}
				}
				if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
					$domain_array = explode(".", $email_array[1]);
					if (sizeof($domain_array) < 2) {
						return false; // Not enough parts to domain
					}
					for ($i = 0; $i < sizeof($domain_array); $i++) {
						if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
							return false;
						}
					}
				}

				return true;
			}
		}
        
    // Valid numeric
    public static function valid_numeric($str) {
      return is_numeric($str);
    }
    
    public static function bad_string($str) {
      $bad_strings = [
  			"content-type:"
  			, "mime-version:"
  			, "multipart/mixed"
  			, "Content-Transfer-Encoding:"
  			, "bcc:"
  			, "cc:"
  			, "to:",
  		];
  
  		foreach ($bad_strings as $bad_string) {
  			if (stripos(strtolower($str), $bad_string) !== false) {
  				return true;
  			}
  		}
    }
    
    // Bool
    public static function bool($str) {
              
      $str = strtolower($str);
      
			if ($str === 'false') {
				return false;
			}
			if ($str === '0') {
				return false;
			}
			if ($str === 0) {
				return false;
			}
			if ($str === 'no') {
				return false;
			}
			if ($str === 'n') {
				return false;
			}
			if ($str === false) {
				return false;
			}

			if ($str === 'true') {
				return true;
			}
			if ($str === '1') {
				return true;
			}
			if ($str === 1) {
				return true;
			}
			if ($str === 'y') {
				return true;
			}
			if ($str === 'yes') {
				return true;
			}
			if ($str === true) {
				return true;
			}

			return false;
    }
        
    // Count
    public static function count($str) {
    	if (is_array($str)) {
    		return count($str);
    	}
    	
    	return 0;
    }
        
    // Pad
    public static function pad($n) {
      $n = (int)$n;
      
      if ($n<10) {
        return '0'.$n;
      } else {
        return ''.$n;
      }
    }
    	
    // Extend
    public static function extend($existing, $new) {
			if (is_array($new)) {
				$new = array_merge($existing, $new);
			} else {
				$new = $existing;
			}

			return $new;
		}
        
    // Get
    public static function get($var, $default = false) {
        
      if (PERCH) {
        return perch_get($var);
      } else {
        if (isset($_GET[$var]) && $_GET[$var] != '') {
            return rawurldecode($_GET[$var]);
        }
      }
      
      return $default;
    }
        
    // Post
    public static function post($var, $default = false) {
      if (isset($_POST[$var]) && $_POST[$var] != '') {
        return $_POST[$var];
      }
        
      return $default;
    }
        
    // Include snippet
    public static function snippet($file) {
      if (is_file($file)) {
        ob_start();
        include $file;
        return ob_get_clean();
      }
        
      return false;
    }
        
    // Create versioned file
    public static function version($file, $return = false) {
        
      $path = ROOT.$file;
        
      if (strpos($file, '/') !== 0 || !file_exists($path)) {
        return $file;
      }
      
      $path_info = pathinfo($path);
      $filename = substr($file, 0, strrpos($file, '.'));
      
      // $mtime = filemtime($path);
      $mtime = date("YmdHis", filemtime($path));
      // $mtime = date('Ymd', filemtime($path));
                    
      $versioned = $filename . '.' . $mtime . '.' . $path_info['extension'];
      
      if ($return) {
        return $versioned;
      }
      
      echo $versioned;
    }
    
    /*
    public static function version($file) {
        
      $path = ROOT.$file;
        
      if (strpos($file, '/') !== 0 || !file_exists($path)) {
        return $file;
      } 
        
      // $mtime = filemtime($path);
      $mtime = date("YmdHis", filemtime($path));
      // $mtime = date('Ymd', filemtime($path));
                    
      $versioned = preg_replace('{\\.([^./]+)$}', ".$mtime.\$1", $file);
      
      return $versioned;
    }
    */
        
    // Retrieve asset
    public static function asset($file, $version = false, $return = false) {
        
    	$path = ASSET.$file;
    	
    	if ($version) {
    	    $path = utility::version($path, true);
    	}
    	
    	if ($return) {
    	  return $path;
    	}
    	
    	echo $path;
    }
        
    // assets
    public static function assets($url) {
      $path = pathinfo($url);
      $ver = '.'.filemtime(ROOT.$url).'.';
      
      return $path['dirname'].'/'.str_replace('.', $ver, $path['basename']);
    }
        		
		// Download file
		public static function download($file_path, $file_name, $force_download = true) {
      if (file_exists($file_path)) {
        // find file type
        if (function_exists('finfo_file')) {
          $finfo = finfo_open(FILEINFO_MIME_TYPE);
          $mimetype = finfo_file($finfo, $file_path);
          finfo_close($finfo);
        }else{
          $mimetype = mime_content_type($file_path);
        }
          
        if (!$mimetype) {
          $mimetype = 'application/octet-stream';
        }
      
        header("Content-Type: $mimetype", true);
          
        if ($force_download) {
          header("Content-Disposition: attachment; filename=\"".$file_name."\"", true);
          header("Content-Length: ".filesize($file_path), true);
          header("Content-Transfer-Encoding: binary\n", true);
        }
    
        if ($stream = fopen($file_path, 'rb')){
          while (!feof($stream) && connection_status() == 0) {
            print(fread($stream,filesize($file_path)));
            ob_flush();
          }
          
          fclose($stream);
        }
      
        exit;
      }
		}
        
    // Retrive file download
    public static function file_download($file, $force_download = true) {    
      if ($file) {
        $file_path = realpath(utility::file_path($file));
        $file_name = ltrim($file, '/');

        utility::download($file_path, $file_name, $force_download);
      }
    }
        
    // Retrive secure file download
    public static function secure_download($file, $bucket = 'default', $force_download = true) {
      if (PERCH) {
        // global $Perch;
        $Perch = Perch::fetch();
        $bucket = $Perch->get_resource_bucket($bucket);
      }

      if ($bucket) {
            
        $file_path = realpath(utility::file_path($bucket['file_path'].'/'.ltrim($file, '/')));
        $file_name = ltrim($file, '/');

        // check we're still within the bucket's folder, to secure against bad file paths
        if (substr($file_path, 0, strlen($bucket['file_path'])) == $bucket['file_path']) {

          utility::download($file_path, $file_name, $force_download);
        }
      }
    }
        
    // Send email
    public static function send_mail($name, $email, $subject, $body, $reply = false) {
    
    	global $vars;
        
      $recipients = explode(",", $vars['recipient']);
    
    	// email Headers
    	$header = "From: ".$vars['site']."<".$vars['sender'].">\r\n"; // from
    	
    	if ($reply) {
    	  $header .= "Reply-To: ".$name." <".$email.">\r\n"; // reply
    	}
    	$header .= "Return-Path: ".$vars['site']."<".$vars['sender'].">\r\n"; // return path
    	$header .= "Message-ID: ".md5(date('r', time()))."@".str_replace("www.", "", $_SERVER['SERVER_NAME'])."\r\n"; // unique id
    	$header .= "MIME-Version: 1.0\r\n"; // mime version
    	$header .= "Date: ".date('r', time())."\r\n"; // date
    	// $header .= "Content-Type: text/html; charset=utf-8\r\n"; // content type
    	$header .= "Content-Type: text/plain; charset=utf-8\r\n"; // content type
    	$header .= "X-Mailer: PHP/".phpversion()."\r\n"; // additional header
    	
    	$return = "-f".$vars['sender']; // return path
    	
    	// send
    	if (is_array($recipients)) {
    	  foreach($recipients as $recipient) {
    	    mail($recipient, $subject, $body, $header, $return);
    	  }
    	} else {
    	  $recipient = $recipients;
    	  mail($recipient, $subject, $body, $header, $return);
    	}
    
    }
        
    // MB fallback
    public static function mb_fallback() {	    	
  	// If one's not there, the others won't be
      if (!extension_loaded('mbstring')) {

      	if (!function_exists('mb_strlen')) {
          function mb_strlen($a) {
            return strlen($a);
          }
      	}

        if (!function_exists('mb_strpos')) {
          function mb_strpos($a, $b) {
            return stripos($a, $b);
          }
        }

        if (!function_exists('mb_strrpos')) {
          function mb_strrpos($a, $b) {
            return strripos($a, $b);
          }
        }

        if (!function_exists('mb_substr')) {
          function mb_substr($a, $b, $c) {
            return substr($a, $b, $c);
          }
        }
      }
    }
        
    // Output debug
    public static function debug($return = false) {
        
      global $vars, $meta;
      
      if (!DEBUG) {
        return false;
      }
      
      if ($vars['debug']) {
        $out = '<div class="debug">';
        $out .= '<h1 class="debug-heading">Debug</h1>';
        
        /*
        foreach($vars as $key => $value) {
          $key = ucfirst($key);
          $out .= '<p><b>'.$key.':</b> '.$value."</p>\n";
        }
        */
        /*
        if (isset($vars) && (is_array($vars) || is_object($vars))) {
        	$out .= '<pre>' . print_r($vars, 1) . '</pre>';
        }
        */
        
        /*
        foreach($vars as $key => $value) {
          $key = ucfirst($key);
          $out .= '<p><b>'.$key.':</b> '.$value."</p>\n";
        }
        */
        
        if (isset($vars) && is_array($vars)) {
          $vars = print_r($vars, 1);
          
          $out .= '<h3 class="debug-sub-heading">Global</h3>';
          $out .= '<pre>'.$vars.'</pre>';
        }
        
        if (isset($meta) && is_array($meta)) {
          $meta = print_r($meta, 1);
          
          $out .= '<h3 class="debug-sub-heading">Meta</h3>';
          $out .= '<pre>'.$meta.'</pre>';
        }
        
        if (PERCH) {
          $out .= '<h3 class="debug-sub-heading">Perch</h3>';
          $out .= '<pre>'.print_r(PerchSystem::get_vars(), 1).'</pre>';
        }
        
        $out .= '<h3 class="debug-sub-heading">Php</h3>';
        
        $error = error_get_last();
        
        if (isset($error) && (is_array($error) || is_object($error))) {
          $error = print_r($error, 1);
        }
        
        $out .= '<pre>'.((isset($error)) ? $error : 'No errors').'</pre>';
        $out .= '</div>';
        
        if ($return) {
          return $out;
        } else {
          echo $out;
        }
      }
    }
    
    // Output setting
    
    public static function setting($var, $return = false) {
      
      global $settings;
      
      if (isset($settings[$var])) {
          if ($return) {
            return $settings[$var];
          } else {
            // echo utility::html($settings[$var]);
            echo $settings[$var];
          }
      }
      
      return false;
      
    }
    
    // Output head
    public static function head() {
    
      utility::setting('head');
    }
    
    // Output footer
    public static function footer() {
    
      utility::setting('footer');
      
      if (MODE === 'development') {
      	utility::debug();
      	
      	if (PERCH && !PERCH_RUNWAY) {
      	  PerchUtil::output_debug();
      	}
      }
    }
  }