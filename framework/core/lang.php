<?php

  // Retrieve and check allowed languages. Set lang var or revert to default
   $allowed_langs = perch_pages_navigation([
    'from-path' => '/',
    'levels' => 1,
    'navgroup' => 'lang',
    'add-trailing-slash' => true,
    // 'use-attributes' => false,
    'skip-template' => true,
  ], true);
  
  $vars['langs'] = [];
  
  if (isset($allowed_langs[0])) {
    $vars['legacy'] = [];
    $vars['legacy']['domain'] = 'https://www.thinkproject.com/';
    $vars['legacy']['paths'] = [];

    $vars['langs'] = array_map( function($allowed_lang) {
      $legacy = $GLOBALS['vars']['legacy']; // remove this once all langs are added to cms
      $str = $allowed_lang['pagePath'];
      
      if (strpos($str, $legacy['domain']) !== false) { // remove condition once all langs are added to cms
        $str = str_replace($legacy['domain'], '', $str);
        $arr = explode('/',$str);
        $key = $arr[0];

        $GLOBALS['vars']['legacy']['paths'][$key] = $legacy['domain'] . $str;
      } else {
        return str_replace('/', '', $str); // keep this
      }
    }, $allowed_langs);

    
    
    $url = perch_page_url([
      'add-trailing-slash' => true,
      'include-domain' => false,
    ], true);
    
    $path = explode('/', $url);
    // $path = explode('/', $vars['path']);
    
    if (isset($path[1]) && in_array($path[1], $vars['langs'])) {
      $vars['lang'] = $path[1];
    }
    
  }
  
  // Set Perch vars
  PerchSystem::set_var('lang', $vars['lang']);