<?php

  // Set global vars
  global $vars, $meta;
  
  $vars = [
    'site' => SITE,
    'description' => DESCRIPTION,
    'author' => AUTHOR,
    'version' => VERSION,
    'salt' => SALT,
    'secret' => SECRET,
    'timezone' => TIMEZONE,
    'lang' => LANGUAGE,
    'ssl' => SSL,
    'host' => HOST,
    'scheme' => SCHEME,
    'domain' => DOMAIN,
    'root' => ROOT,
    'inc' => INC,
    'asset' => ASSET,
    'url' => utility::get_url(),
    'path' => utility::get_path(),
    'page' => utility::get_page(),
    'class' => utility::get_class(),
    'ip' => utility::get_ip(),
    'mode' => MODE,
    'debug' => DEBUG
  ];
  
  
  // Default meta vars
  $meta = [
    'site' => SITE,
    'title' => utility::title($vars['page']),
    'description' => '',
    'keywords' => '',
    'image' => '',
    'robots' => '',
  ];
  
  
  // Timezone
  if (defined('TZ')) {
    date_default_timezone_set(TIMEZONE);
  } else {
    date_default_timezone_set('UTC');
  }
  
  
  // Set Perch vars
  if (PERCH) {
    PerchSystem::set_vars([
      'host' => $vars['host'],
      'domain' => $vars['domain'],
      'root' => $vars['root'],
      'url' => $vars['url'],
      'path' => $vars['path'],
      'page' => $vars['page'],
    ]);
  }